FROM node:16-alpine

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app

COPY package.json ./
COPY yarn.lock ./

USER node
RUN yarn install --production

COPY --chown=node:node . .

ENV SERVERPORT 80
ENV SENTRY_KEY https://f0690bc6750747e5b0788196c911ee71@o646239.ingest.sentry.io/5758996

CMD yarn run start

EXPOSE 80