import fs from 'fs';
import path from 'path';
import { ObjectId } from 'mongodb';
import * as bson from 'bson';
import { IErrorData, IErrorDataSchool, IRoosterData, IRoosterDataSchool, IRoosterHourData } from '../typings/data';
import { logWarn } from './logger';
import { parse } from 'json2csv';
import { getRootFolder } from '..';
import { IWeekdagen } from '../typings/generic';

declare type TError = IErrorData | IErrorDataSchool | string[];

// Info
export const dagen: IWeekdagen[] = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'];

// Password generator
export const generatePassword = (length = 10): string => {
	const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=%!@&';
	let retVal = '';
	for (let i = 0, n = charset.length; i < length; ++i) {
		retVal += charset.charAt(Math.floor(Math.random() * n));
	}
	return retVal;
};

// Error functions
export const LoadErrors = (school?: string, type?: string): Promise<TError> => {
	return new Promise<TError>((res, _rej) => {
		fs.readFile('./assets/errors.json', 'utf8', (err, orerrors: string) => {
			if (err) throw err;
			let errors: IErrorData = {};
			if (orerrors.length > 0) {
				errors = JSON.parse(orerrors);
			} else {
				errors = {};
			}
			res(school ? (type ? errors[school][type] : errors[school]) : errors);
		});
	});
};

export const SaveErrors = async (pErrors: TError, school?: string, type?: string): Promise<void> => {
	if ((Array.isArray(pErrors) && pErrors.length < 0) || (Object.entries(pErrors) && Object.entries(pErrors).length < 0))
		return;
	let errors: IErrorData = (await LoadErrors()) as IErrorData;

	if (!school && !type) errors = pErrors as IErrorData;

	school
		? type
			? (errors[school][type] = pErrors as string[])
			: (errors[school] = pErrors as IErrorDataSchool)
		: errors;

	fs.writeFile('./assets/errors.json', JSON.stringify(errors, null, 2), 'utf-8', err => {
		if (err) throw err;
	});
};

// Easy load rooste
export const LoadRooster = (school: string): Promise<IRoosterDataSchool | Record<string, never> | null> => {
	return new Promise((res, _rej) => {
		fs.readFile('./assets/roosters.json', 'utf8', (err, ordata) => {
			if (err) throw err;
			let data: IRoosterData = {};
			if (ordata.length > 0) {
				data = JSON.parse(ordata);
			}
			res((Object.entries(data[school]).length > 0 && data[school]) || null);
		});
	});
};

// Validator
export const IsValid = (type: 'les', info: any): boolean => {
	const lesObject = ['klas', 'leerkracht', 'vak', 'lokaal', 'dag', 'lesuur'];
	switch (type) {
		case 'les':
			lesObject.map(objchild => {
				if (info[objchild] === undefined) {
					return false;
				}
				return true;
			});
			return true;
		default:
			return false;
	}
};

// Util
export const DoesValueExist = (value: any, array: any[]): boolean => {
	if (!array) {
		return false;
	}
	let retval = false;
	array.map(val => {
		if (val === value) {
			retval = true;
			return;
		}
	});
	return retval;
};

export const getLesAmount = (
	klas: string,
	vak: string,
	lkr: string,
	lok: string,
	data: IRoosterDataSchool
): number | null => {
	let amount = 0;
	// Check on missing or invalid values
	if (!klas || !vak || !data || !lkr || !lok) {
		logWarn('getLesAmount missed arguments');
		return null;
	}
	if (!data?.classsorted?.[klas]) {
		logWarn(`Klas ${klas} not found`);
		return null;
	}

	// Sorting trough the classes
	data.classsorted[klas].map(day => {
		if (!day || !Array.isArray(day)) return;
		day.map((lesuur: IRoosterHourData | null) => {
			if (!lesuur) return;
			if (lesuur.vak == vak && lesuur.leerkracht == lkr && lesuur.lokaal == lok) amount++;
		});
	});
	return amount;
};

export const checkIfKlasHasErrors = async (
	school: string,
	klas: string,
	errors: string[] | IErrorDataSchool
): Promise<boolean> => {
	if (!errors) {
		errors = (await LoadErrors(school)) as IErrorDataSchool;
	}

	const ordata = ((await LoadRooster(school)) as IRoosterDataSchool)?.classsorted?.[klas];
	let data: string[] = [];
	let retval = false;

	// Change data objectsinto array's
	ordata.map(dataday => {
		if (!dataday) return;
		dataday.map((hour: IRoosterHourData | null) => {
			if (!hour || Object.entries(hour).length == 0) return;
			data = [...data, ...Object.values(hour)];
		});
	});

	if (Array.isArray(errors)) {
		errors.map(err => {
			// Cycle trough data and check if err is in array
			if (data.some(info => info == err)) retval = true;
		});
	}
	if (Object.entries(errors).length > 0) {
		errors = errors as IErrorDataSchool;
		for (const errtype in errors) {
			if (Object.hasOwnProperty.call(errors, errtype)) {
				const errdata = errors[errtype];
				errdata.map(err => {
					if (data.some(info => info == err)) retval = true;
				});
			}
		}
	}

	return retval;
};

// Files
export const convertToCSV = (data: any[], headers?: string[]): Promise<string> => {
	return new Promise((res, rej) => {
		try {
			const csv = parse(data, headers ? { fields: headers } : {});
			res(csv);
		} catch (error) {
			rej(error);
		}
	});
};

export const writeTotmp = (filename: string, data: any): Promise<void> => {
	return new Promise((res, rej) => {
		try {
			const folder = path.resolve(getRootFolder(), './tmp');
			if (!fs.existsSync(folder)) {
				fs.mkdirSync(folder);
			}
			fs.writeFileSync(path.resolve(folder, filename), data);
			res();
		} catch (error) {
			rej(error);
		}
	});
};

export const Wait = (ms: number): Promise<void> => new Promise(res => setTimeout(res, ms));

export const isObjectIdValid = (id: bson.ObjectId | string | number): boolean =>
	ObjectId.isValid(id) ? (String(new ObjectId(id) === id) ? true : false) : false;
