import mongoose from 'mongoose';
import { Card } from '../model/Creator';
import { Class, Schema, Teacher } from '../model/Data';
import { ICard, IClass, ISchema, ITeacher } from '../typings/model';
import { logError } from './logger';
import { dagen } from './util';

declare type IReplaceLes = (
	schemaId: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId | string,
	origin: {
		day: number;
		hour: number;
		id: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId;
		info: ICard;
		teacher?: ITeacher;
	},
	target: {
		day: number;
		hour: number;
		id: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId | 0;
		info: ICard;
		teacher?: ITeacher;
	},
	times?: number // to know how many times the func was called in recursion
) => Promise<boolean>;

export const isTeacherFree = async (
	teacherid: mongoose.Schema.Types.ObjectId | string,
	day: number,
	hour: number
): Promise<boolean> => {
	/*
		teacher info ophalen
		Schema's 1 per 1 ophalen en doorzoeken welke lkr lesgeeft op welke dag
			--> is zelfde lkr return false & break loop
	*/
	try {
		const teacher = await Teacher.findById(teacherid);
		let retval = true;
		if (!teacher) {
			throw new Error(`Couldn't find teacher for ${teacherid}`);
		}
		for (const klas of teacher.classes) {
			const schema = await Schema.findOne({ user: `standard-${klas}` });
			if (!schema) {
				throw new Error(`Couldn't find schema for ${klas}`);
			}
			// check for card in DB
			if (schema.data?.[dagen[day]]?.[hour]) {
				// er bestaat een card haal deze op
				const card = await Card.findById(schema.data[dagen[day]][hour]);
				if (!card) {
					throw new Error(`Couldn't find card for ${schema.data[dagen[day]][hour]}`);
				}
				if (card.teacher == teacherid) {
					retval = false;
					break;
				}
			}
		}
		return retval;
	} catch (error) {
		logError(error, 'parser.util-isTeacherFree');
		new Error(error);
		return false;
	}
};

export const getLessonForTeacher = async (
	teacherid: mongoose.Schema.Types.ObjectId | string,
	day: number,
	hour: number
): Promise<mongoose.Schema.Types.ObjectId | string | null> => {
	if (await isTeacherFree(teacherid, day, hour)) return null;
	try {
		const teacher = await Teacher.findById(teacherid);
		let retval: null | string = null;
		if (!teacher) {
			new Error(`Couldn't find teacher for ${teacherid}`);
		}
		for (const klas of teacher.classes) {
			const schema = await Schema.findOne({ user: `standard-${klas}` });
			if (!schema) {
				new Error(`Couldn't find schema for ${klas}`);
			}
			// Bekijk of schema bestaat
			if (schema.data[dagen[day]]?.[hour]) {
				const card = await Card.findById(schema.data[dagen[day]][hour]);
				if (card.teacher == teacherid) {
					retval = card._id;
					break;
				}
			}
		}
		return retval;
	} catch (error) {
		logError(error, 'parser.util-isTeacherFree');
		new Error(error);
		return null;
	}
};

/**
 * @param schemaId
 * @param lesId the cardid
 * @param day
 * @param hour
 */
export const moveLes = async (
	schemaId: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId | string,
	lesId: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId | string,
	day: number,
	hour: number
): Promise<void> => {
	try {
		const schema = await Schema.findById(schemaId);
		const newData = { ...schema.toObject().data };
		if (!newData[dagen[day]]) newData[dagen[day]] = {};
		newData[dagen[day]][hour] = lesId as mongoose.Schema.Types.ObjectId;
		await schema.updateOne({ data: newData });
	} catch (error) {
		logError(error, 'parser.util-moveLes');
		throw new Error(error);
	}
};

const clearLes = async (
	schemaId: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId | string,
	day: number,
	hour: number
) => {
	try {
		const schema = await Schema.findById(schemaId);
		const newData = { ...schema.toObject().data };
		if (!newData[dagen[day]]) return;
		delete newData[dagen[day]][hour];
		await schema.updateOne({ data: newData });
	} catch (error) {
		logError(error, 'parser.util-moveLes');
		throw new Error(error);
	}
};

// Origin wordt vervangen door target
// Als origin nie kart sws abort
// Als target niet kan zal er gekeken worden of het lesuur waar de target naar toe moet ergens in de differences staat bij de origins
// Ja: Voer deze difference eerst uit
// Neen: Zoek door schema's naar vrij uur voor lkr op dagen dat hij werkt
export const replaceLes: IReplaceLes = async (schemaId, origin, target, calltime = 0) => {
	// Block to cancel infinite loop
	if (calltime >= 3) return false;

	if (target.id === 0) {
		clearLes(schemaId, origin.day, origin.hour);
		await moveLes(schemaId, origin.id, target.day, target.hour);
		return true;
	}

	target.teacher = await Teacher.findById(target.info.teacher);
	origin.teacher = await Teacher.findById(origin.info.teacher);

	if (!(await isTeacherFree(origin.info.teacher, target.day, target.hour))) {
		return false;
	}

	if (await isTeacherFree(target.info.teacher, origin.day, origin.hour)) {
		await moveLes(schemaId, target.id, origin.day, origin.hour);
		await moveLes(schemaId, origin.id, target.day, target.hour);
		return true;
	}

	// target.card.teacher IS NIET beschikbaar en zal verplaatsen
	try {
		const targetClass: {
			lesson: ICard;
			class?: IClass;
			schema?: ISchema;
		} = {
			lesson: await Card.findById(await getLessonForTeacher(target.info.teacher, origin.day, origin.hour)),
		};
		targetClass.class = await Class.findOne({ name: targetClass.lesson.klas });
		targetClass.schema = await Schema.findOne({
			user: `standard-${targetClass.class.name}`,
		});

		const shuffledDays = target.teacher.days.sort(() => Math.random() - 0.5);

		const assignToRandom = async (): Promise<boolean> => {
			const randomTimings = {
				day: dagen.indexOf(shuffledDays[Math.floor(Math.random() * dagen.length)]),
				hour: Math.floor(Math.random() * 8 + 1), // number between 1 and 8
			};

			if (targetClass.schema.data[dagen[randomTimings.day]]?.[randomTimings.hour]) {
				try {
					const info = await Card.findById(targetClass.schema.data[dagen[randomTimings.day]][randomTimings.hour]);
					return await replaceLes(
						targetClass.schema._id,
						{
							id: target.info.teacher,
							day: origin.day,
							hour: origin.hour,
							info: target.info,
						},
						{
							id: info.teacher,
							day: randomTimings.day,
							hour: randomTimings.hour,
							info,
						},
						calltime++
					);
				} catch (e) {
					logError(e, 'replaceLes.assignRandoms');
					throw new Error(e);
				}
			}

			// Niks gedefinïeerd op dit uur (is dus vrij)
			if (randomTimings.hour >= 5) {
				const checkPreviousHour = async (): Promise<boolean> => {
					if (randomTimings.hour <= 0) {
						return await assignToRandom();
					}
					// Check of class uur hiervoor bezet is
					if (targetClass.schema.data[dagen[randomTimings.day]]?.[randomTimings.hour - 1]) {
						try {
							await moveLes(targetClass.schema._id, targetClass.lesson._id, randomTimings.day, randomTimings.hour);
							await moveLes(schemaId, target.id as string | mongoose.Schema.Types.ObjectId, origin.day, origin.hour);
							await moveLes(schemaId, origin.id, target.day, target.hour);
							return true;
						} catch (e) {
							logError(e, 'replaceLes.assignToRandom.checkPreviousHour');
							throw new Error(e);
						}
					} else {
						randomTimings.hour--;
						return await checkPreviousHour();
					}
				};
				return await checkPreviousHour();
			} else {
				const checkNextHour = async (): Promise<boolean> => {
					if (randomTimings.hour > 8) {
						return await assignToRandom();
					}
					if (targetClass.schema.data[dagen[randomTimings.day]]?.[randomTimings.hour + 1]) {
						try {
							await moveLes(targetClass.schema._id, targetClass.lesson._id, randomTimings.day, randomTimings.hour);
							await moveLes(schemaId, target.id as string | mongoose.Schema.Types.ObjectId, origin.day, origin.hour);
							await moveLes(schemaId, origin.id, target.day, target.hour);
							return true;
						} catch (e) {
							logError(e, 'replaceLes.assignToRandom.checkPreviousHour');
							throw new Error(e);
						}
					} else {
						randomTimings.hour++;
						return await checkNextHour();
					}
				};
				return await checkNextHour();
			}
		};

		return await assignToRandom();
	} catch (error) {
		logError(error, 'parser.util-replaceLes');
		new Error(error);
		return false;
	}
};
