import Joi, { ValidationResult } from 'joi';

//register validation
export const registerValidation = (data: {
	name?: string;
	firstname?: string;
	lastname?: string;
	email?: string;
	password?: string;
}): ValidationResult => {
	const schema = Joi.object({
		name: Joi.string().min(6).required().max(255),
		firstname: Joi.string().required().max(255),
		lastname: Joi.string().required().max(255),
		email: Joi.string().min(6).required().max(255).email(),
		password: Joi.string().min(6).required().max(1024),
	});
	return schema.validate(data);
};

//login validation
export const loginValidation = (data: { email?: string; password?: string }): ValidationResult => {
	const schema = Joi.object({
		email: Joi.string().min(6).required().max(255).email(),
		password: Joi.string().min(6).required().max(1024),
	});
	return schema.validate(data);
};
