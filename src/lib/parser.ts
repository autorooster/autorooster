// import { spawn, Thread, Worker } from 'threads';
import { Schema } from '../model/Data';
import Users from '../model/User';
import { ISchema, ISchemaBase, IUser } from '../typings/model';
import { ObjectId } from 'mongoose';
import { logInfo } from './logger';
// import { IDiffWorker } from '../typings/parser';
import { DiffWorker } from './parser.worker';

export const createWorker = (klas: string): Promise<any> => {
	// eslint-disable-next-line no-async-promise-executor
	return new Promise(async (res, rej) => {
		// Get Leschschema's from DB
		const schemas: ({
			_id?: ObjectId;
		} & ISchemaBase)[] = [];
		const users: IUser[] = await Users.find({ klas });
		await Promise.all(
			users.map(async user => {
				const uschema: ISchema = await Schema.findOne({ user: user._id });
				if (!uschema) return;
				const uschemaObj: { _id?: ObjectId } & ISchemaBase = uschema.toObject();
				uschemaObj._id = uschema._id;
				schemas.push(uschemaObj);
			})
		);
		// const worker = await spawn<IDiffWorker>(new Worker('./parser.worker'));
		try {
			const worker = await DiffWorker({
				userschemas: schemas,
				klas,
			});
			// const workeroutput = await worker({
			// 	userschemas: schemas,
			// 	klas,
			// });
			logInfo('Worker finished');
			res(worker);
		} catch (error) {
			rej(error);
		} finally {
			// await Thread.terminate(worker);
		}
	});
};
