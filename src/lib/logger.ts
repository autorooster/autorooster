import chalk from 'chalk';
import { inspect } from 'util';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const logInfo = (msg: any, nametag: string | null = null): void => {
	console.log(chalk.blue(`${!nametag ? '[+]' : `[${nametag}]`} [INFO]`) + ' ' + inspect(msg));
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const logWarn = (msg: any, nametag: string | null = null): void => {
	console.log(chalk.yellow(`${!nametag ? '[+]' : `[${nametag}]`} [WARN]`) + ' ' + inspect(msg));
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const logError = (msg: any, nametag: string | null = null): void => {
	console.log(chalk.red(`${!nametag ? '[+]' : `[${nametag}]`} [ERROR]`) + ' ' + inspect(msg));
};
