import { ObjectId } from 'mongoose';
import { Card } from '../model/Creator';
import { Teacher, Lokaal } from '../model/Data';
import { IWeekdagen } from '../typings/generic';
import { ICard, ICardBase, ICardBaseFilled, ILokaalBase, ISchemaBase, ITeacherBase } from '../typings/model';
import { isObjectIdValid } from './util';

export const getLesCard = async (
	card: ObjectId | ICardBase,
	pTeacher: boolean,
	pLokaal: boolean
): Promise<ICardBaseFilled | null> => {
	// If p's are true --> preserve id
	pTeacher = pTeacher ?? true;
	pLokaal = pLokaal ?? true;
	if (typeof card === 'string' && isObjectIdValid(card)) {
		card = (await Card.findById(card)).toObject() as ICardBase;
	}
	card = card as ICard;
	if (!card || !card.teacher || !card.klas) return null;

	const teacherdata = await Teacher.findById(card.teacher);
	const lokaaldata = await Lokaal.findById(card.lokaal);
	const newcard: ICardBaseFilled = {
		...card,
		teacher: pTeacher
			? { ...(teacherdata.toObject() as ITeacherBase), _id: card.teacher }
			: (teacherdata.toObject() as ITeacherBase),
		lokaal: pLokaal
			? { ...(lokaaldata.toObject() as ILokaalBase), _id: card.lokaal }
			: (lokaaldata.toObject() as ILokaalBase),
	};
	return newcard;
};

export const validSchema = async (schema: ISchemaBase): Promise<boolean> => {
	if (!schema || !schema.data) return false;
	let retval = true;
	let day: IWeekdagen;
	for (day in schema.data) {
		if (Object.hasOwnProperty.call(schema.data, day)) {
			for (const hour in schema.data[day]) {
				if (Object.hasOwnProperty.call(schema.data[day], hour)) {
					const info = await getLesCard(schema.data[day][hour] as ObjectId, true, true);
					if (!info) retval = false;
				}
			}
		}
	}
	return retval;
};
