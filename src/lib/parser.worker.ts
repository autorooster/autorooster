// import { expose } from 'threads/worker';
import { logError, logInfo } from './logger';
import { Card } from '../model/Creator';
import { Teacher, Schema } from '../model/Data';
import { replaceLes } from './parser.util';
import { IDiff, IDiffWorker } from '../typings/parser';
import { ICard } from '../typings/model';
import { IWeekdagen } from '../typings/generic';

// Mongoose connection
// import { connect } from '../config/db.config';
// connect('parser-worker');

const dagen: IWeekdagen[] = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'];

let differences: IDiff[] = [];
const confirmedDiffs: {
	schema: IDiff['schema'];
	diffindex: number;
}[] = [];

export const DiffWorker: IDiffWorker = async workerData => {
	logInfo('Worker started');
	let klasSchema;
	try {
		klasSchema = await Schema.findOne({ user: `standard-${workerData.klas}` });
		if (!klasSchema) {
			logError(`Couldn't find schema for ${workerData.klas}`, 'parser-worker');
			throw new Error(`Couldn't find schema for ${workerData.klas}`);
		}
	} catch (err) {
		logError(err, 'parser-worker');
		throw new Error(err);
	}

	for (let i = 0; i < workerData.userschemas.length; i++) {
		const uschema = workerData.userschemas[i];
		if (!uschema.differences) {
			logError(`No differences for ${uschema._id}`);
			continue;
		}
		for (const key of Object.keys(uschema.differences)) {
			const diff = uschema.differences[key] as IDiff;
			if (!diff.origin.id) {
				logError(`No origin ID ${key} in ${uschema._id}`);
				continue;
			}
			try {
				diff.origin.info = await Card.findById(diff.origin.id);
				if (diff.target.id != 0) {
					diff.target.info = await Card.findById(diff.target.id);
				}
				// Add in differences Array
				differences.push({
					schema: klasSchema._id,
					origin: diff.origin,
					target: diff.target,
				});
			} catch (err) {
				logError(err.message, 'parser-worker');
				throw new Error(err.message);
			}
		}
	}

	// Check if teacher actually gives lesson on this day
	differences = await Promise.all(
		differences.filter(async diff => {
			try {
				const orteacher = await Teacher.findById(diff.origin.info.teacher);
				if (!orteacher.days.includes(dagen[diff.origin.day])) return false;
				if (diff.target.info) {
					const trgteacher = await Teacher.findById(diff.target.info.teacher);
					if (!trgteacher.days.includes(dagen[diff.target.day])) return false;
				}
				return true;
			} catch (error) {
				logError(error, 'parser-worker');
				throw new Error(error);
			}
		})
	);

	for (const diff of differences) {
		const i = differences.indexOf(diff);
		if (
			await replaceLes(
				diff.schema,
				diff.origin as IDiff['origin'] & {
					info: ICard;
				},
				diff.target as IDiff['target'] & {
					info: ICard;
				}
			)
		) {
			confirmedDiffs.push({
				schema: diff.schema,
				diffindex: i,
			});
		}
	}

	return confirmedDiffs;
};

// expose(DiffWorker);
