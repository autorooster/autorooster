import CSVToJSON from 'csvtojson';
import { Teacher, Class, Lokaal, Schema } from '../model/Data';
import { Card } from '../model/Creator';
import fs from 'fs';
import { setTimeout } from 'timers';
import { logInfo, logWarn, logError } from './logger';
import * as Util from './util';
import { ObjectId } from 'mongoose';
import {
	IErrorData,
	IHeadersData,
	IRoosterClassSorted,
	IRoosterData,
	IRoosterHourData,
	IRoosterDataSchool,
	IRoosterRawData,
} from '../typings/data';
import { ICard } from '../typings/model';
import path from 'path/posix';

const actions: {
	[k: string]: boolean;
} = {};
let Busy = false;

declare type IQueueData = {
	klas: string;
	school: string;
} & IRoosterHourData;
declare interface IConversionQueue {
	func: Function;
	info: {
		dag: number;
		hour: number;
		data: IQueueData;
	};
}

export const importer = async (school: string, headers?: string[]): Promise<void> => {
	actions.importing = true;
	let CSVoptions = {};
	// Stored headers
	let stheaders: IHeadersData = {};
	logInfo('started importing:' + school, 'Importer');

	// Check if school is already imported
	await new Promise<void>((res, _rej) => {
		fs.readFile('./assets/roosters.json', 'utf-8', function (err, ordata) {
			if (err) throw err;
			const data: IRoosterData = JSON.parse(ordata);
			if (data?.[school]?.raw) {
				delete actions.importing;
				return;
			}
			res();
		});
	});

	if (!actions.importing) {
		logWarn('school already exists?');
		return;
	}

	// Read stored headers
	stheaders = await new Promise<IHeadersData>((res, _rej) => {
		fs.readFile('./assets/headers.json', 'utf-8', function (err, ordata) {
			if (err) throw err;
			if (ordata.length <= 0) return;
			res(JSON.parse(ordata));
		});
	});

	// First try accessing stored headers
	if (stheaders && stheaders[school]) {
		CSVoptions = {
			noheader: true,
			headers: stheaders[school],
		};
	}

	// Check if new headers are given
	if (headers !== undefined) {
		CSVoptions = {
			noheader: true,
			headers: headers,
		};
		stheaders[school] = headers;
		fs.writeFile('./assets/headers.json', JSON.stringify(stheaders, null, 2), 'utf-8', err => {
			if (err) {
				logError(`headers.json write error: ${err}`, 'schemaImporter');
				throw new Error(err.message);
			}
		});
	}

	fs.stat(`./uploads/${school}-rooster.txt`, async function (err, _stat) {
		if (err == null) {
			try {
				const data: IRoosterRawData[] = await CSVToJSON(CSVoptions).fromFile(`./uploads/${school}-rooster.txt`);
				delete actions.importing;
				await new Promise<void>((res, _rej) => {
					fs.readFile('./assets/roosters.json', 'utf-8', (err, ordata) => {
						if (err) throw err;
						let savedData: IRoosterData = {};
						if (ordata.length > 0) {
							savedData = JSON.parse(ordata);
						}

						if (savedData[school] === undefined) savedData[school] = {};
						savedData[school].raw = data;

						fs.writeFile('./assets/roosters.json', JSON.stringify(savedData, null, 2), 'utf-8', function (err) {
							if (err) {
								logError(`roosters.json write error: ${err}`, 'schemaImporter');
								throw new Error(err.message);
							}
							res();
						});
					});
				});

				logInfo('imported ' + school, 'Importer');
				Sorter(school);
				return data;
			} catch (err) {
				logError(err, 'SchemaImporter');
				new Error(err);
				delete actions.importing;
				return err;
			}
		} else if (err.code === 'ENOENT') {
			delete actions.importing;
			logWarn(`${school}-rooster.txt not found!`, 'schemaImporter');
			return;
		} else {
			logError(`${school}-rooster.txt reading error: ${err}`, 'schemaImporter');
			throw new Error(err.message);
		}
	});
};

export const Sorter = (school: string): Promise<void> => {
	return new Promise((res, rej) => {
		if (actions.sorting) res();
		actions.sorting = true;
		logInfo('started sorting for: ' + school, 'Sorter');
		fs.readFile('./assets/roosters.json', 'utf-8', async (err, ordata): Promise<void> => {
			if (err) throw err;
			if (ordata.length <= 0) {
				delete actions.sorting;
				return;
			}

			const data: IRoosterData = await JSON.parse(ordata);
			const errors = (await Util.LoadErrors()) as IErrorData;

			if (data[school] === undefined || data[school].raw === undefined) {
				delete actions.sorting;
				return;
			}

			if (errors[school] === undefined) {
				errors[school] = {
					// Use naming scheme same as used for Mongoose.model
					Teacher: [],
					Class: [],
					Lokaal: [],
				};
			}

			const sortedhours: IRoosterClassSorted = {};
			try {
				let i = 0;
				while (data[school].raw === undefined) {
					if (i > 100) break;
					await new Promise<void>((res, _rej) => {
						setTimeout(() => {
							i++;
							res();
						}, 100);
					});
				}
				if (i >= 100) {
					delete actions.sorting;
					return;
				}
				await Promise.all(
					data[school].raw.map(async les => {
						Util.IsValid('les', les);
						if (les.klas.trim() !== '') {
							les.klas = les.klas.replace(/\s/g, '').toUpperCase();

							const klas = await Class.find({
								name: { $regex: '.*' + les.klas + '.*' },
							});
							if (!klas?.[0]) {
								if (!Util.DoesValueExist(les.klas, errors[school].Class)) {
									errors[school].Class.push(les.klas);
								}
							}

							if (sortedhours[les.klas] === undefined) {
								sortedhours[les.klas] = [];
							}
							if (sortedhours[les.klas][les.dag - 1] === undefined) {
								sortedhours[les.klas][les.dag - 1] = [];
							}

							les.lokaal = les.lokaal
								.replace(/(?<!LOK.{1,5})~LOK|LOK|~LOK.{1,7}$/gm, ``)
								.replace(`.`, ``)
								.replace(/(?<=[A-Z])0/gm, ``);
							const lkr = await Teacher.find({
								$or: [
									{ firstname: { $regex: '.*' + les.leerkracht + '.*' } },
									{ lastname: { $regex: '.*' + les.leerkracht + '.*' } },
									{ abbreviation: { $regex: '.*' + les.leerkracht + '.*' } },
								],
							});
							const lok = await Lokaal.find({
								name: Number(les.lokaal.replace(/\D/gm, ``)),
								blok: les.lokaal.replace(/\d/gm, ``),
							});
							if (!lkr?.[0]) {
								// Check of lokaal empty is
								if (les.lokaal.trim() === '') {
									return;
								}
								sortedhours[les.klas][les.dag - 1][les.lesuur - 1] = {
									leerkracht: les.leerkracht,
									lokaal: les.lokaal,
									vak: les.vak,
								};
								if (!lok?.[0]) {
									if (!Util.DoesValueExist(les.lokaal, errors[school].Lokaal)) {
										errors[school].Lokaal.push(les.lokaal);
									}
								}
								if (!Util.DoesValueExist(les.leerkracht, errors[school].Teacher)) {
									errors[school].Teacher.push(les.leerkracht);
								}
							} else {
								sortedhours[les.klas][les.dag - 1][les.lesuur - 1] = {
									leerkracht: lkr[0]._id,
									lokaal: les.lokaal,
									vak: les.vak,
								};
							}
						}
					})
				);
				data[school].classsorted = sortedhours;
				fs.writeFile('./assets/roosters.json', JSON.stringify(data, null, 2), 'utf-8', async err => {
					if (err) throw err;
					await Util.SaveErrors(errors);
					logInfo('Sorting ' + school + ' finished', `Sorter`);
					res();
					delete actions.sorting;
				});
			} catch (err) {
				logError('bigerror: ' + err, `Sorter`);
				new Error(err);
				rej();
			}
		});
	});
};

// Start Conversion functions
export class Converter {
	public school: string;
	public klas: string;
	private convinfo: {
		errors: {
			[k: string]: {
				[k: string]: any;
			};
		};
		data: IRoosterDataSchool;
		queue: IConversionQueue[];
		resolving: boolean;
	} = {
		errors: {},
		data: {},
		queue: [],
		resolving: false,
	};
	constructor(school: string, klas: string) {
		this.school = school;
		this.klas = klas;
		this.start();
	}

	private FinishConvertion = async (school: string, klas: string, ordata: IRoosterData) => {
		while (this.convinfo.resolving) {
			await new Promise<void>(res => {
				setTimeout(() => {
					res();
				}, 200);
			});
		}
		ordata[school] = this.convinfo.data;
		fs.writeFile('./assets/roosters.json', JSON.stringify(ordata, null, 2), 'utf-8', async err => {
			if (err) throw err;
			try {
				await new Schema({
					user: `standard-${klas}`,
					data: this.convinfo.data.schema[klas],
				}).save();
			} catch (e) {
				logError(e);
				new Error(e);
			}
			delete actions.converting;
			logInfo(`Convertion done for ${klas}`, 'Converter');
		});
	};
	// dag & hour zijn de indexes voor in de schema table
	private ConvertRawLesToSaved = async (dag: number, hour: number, info: IQueueData): Promise<void> => {
		// logInfo( `started conversion for ${ dag } ${ hour }` )
		try {
			const lokinfo = {
				name: Number(info.lokaal.replace(/\D/gm, ``)),
				blok: info.lokaal.replace(/\d/gm, ``),
			};
			const lkr = await Teacher.findById(info.leerkracht);
			const lok = await Lokaal.find({
				name: isNaN(lokinfo.name) || lokinfo.name === 0 ? null : lokinfo.name,
				blok: lokinfo.blok.trim().length > 0 ? lokinfo.blok : null,
			});
			// Didn't found a piece of data so log in schema object
			if (!lkr || lok.length <= 0) {
				this.convinfo.errors[Util.dagen[dag]][hour] = { lkr, lok };
				logError(`Kon geen lkr|lok vinden voor data: ${info.leerkracht} ${info.lokaal}`, `Converter-${this.klas}`);
				return;
			}
			let card: ICard = (await Card.findOne({
				klas: info.klas,
				name: info.vak,
				teacher: lkr._id,
				lokaal: lok[0]._id,
			})) as ICard;
			if (!card) {
				card = new Card({
					name: info.vak,
					klas: info.klas,
					teacher: lkr._id,
					hours: Util.getLesAmount(info.klas, info.vak, info.leerkracht, info.lokaal, this.convinfo.data) ?? 1,
					lokaal: lok[0]._id,
				});
				try {
					await card.save();
				} catch (err) {
					logError(`Error while converting: ${err}`, `Converter-${this.klas}`);
					throw new Error(err);
				}
				if (Array.isArray(card) ? !card[0]._id : !card._id) {
					logError(`Error while converting: Couldn't get a card for ${JSON.stringify(info)}`, `Converter-${this.klas}`);
					throw new Error(`Error while converting: Couldn't get a card for ${JSON.stringify(info)}`);
				}
			}
			if (!this.convinfo.data.schema[info.klas][Util.dagen[dag]])
				this.convinfo.data.schema[info.klas][Util.dagen[dag]] = {} as {
					[k: number]: ObjectId;
				};
			this.convinfo.data.schema[info.klas][Util.dagen[dag]][hour] = Array.isArray(card) ? card[0]._id : card._id;
			// logInfo( `resolved this conversion ${ dag } ${ hour }` )
			return;
		} catch (err) {
			logError(err, `Converter-${this.klas}-ConvertRawLesToSaved`);
			throw new Error(err);
		}
	};

	// func is Array of functions
	private resolvedQueueFunc = async (func?: IConversionQueue[]) => {
		this.convinfo.resolving = true;
		if (Object.entries(func).length <= 0) {
			this.convinfo.resolving = false;
			return;
		}
		if (typeof func[0].func !== 'function') {
			this.convinfo.resolving = false;
			return;
		}
		await func[0].func(func[0].info.dag, func[0].info.hour, func[0].info.data);
		func.shift();
		if (Object.entries(func).length > 0) {
			this.resolvedQueueFunc(this.convinfo.queue);
		} else {
			this.convinfo.resolving = false;
		}
	};
	public start = async (): Promise<void> => {
		if (!actions.sorting) {
			await Sorter(this.school);
		}
		while (actions.sorting) {
			Util.Wait(100);
		}
		logInfo(`started conversion for ${this.klas} in ${this.school}`, 'Converter');
		actions.converting = true;
		fs.readFile('./assets/roosters.json', 'utf-8', (err, ordata): void => {
			if (err) throw err;
			if (ordata.length <= 0) {
				delete actions.converting;
				return;
			}

			const data: IRoosterData = JSON.parse(ordata);
			this.convinfo.data = data[this.school];

			if (!this.convinfo.data?.classsorted?.[this.klas]) {
				delete actions.converting;
				return;
			}
			if (!this.convinfo.data.schema) this.convinfo.data.schema = {};
			this.convinfo.data.schema[this.klas] = this.convinfo.data.schema[this.klas] ?? {};
			this.convinfo.data.classsorted[this.klas].map((day, i) => {
				if (!day) return;
				if (!this.convinfo.errors[Util.dagen[i]]) this.convinfo.errors[Util.dagen[i]] = {};
				day.map(async (les: IRoosterHourData, i2: number) => {
					if (!this.convinfo.errors[Util.dagen[i]][i2]) this.convinfo.errors[Util.dagen[i]][i2] = {};
					if (!les) return;
					this.convinfo.queue.push({
						func: this.ConvertRawLesToSaved,
						info: {
							dag: i,
							hour: i2,
							data: {
								...les,
								klas: this.klas,
								school: this.school,
							},
						},
					});
				});
			});
			this.resolvedQueueFunc(this.convinfo.queue);
			this.FinishConvertion(this.school, this.klas, data);
		});
	};
}

export const AutoImporter = async (): Promise<void> => {
	StartBusyChecker();
	const folder = path.resolve('./uploads');
	if (!fs.existsSync(folder)) {
		fs.mkdirSync(folder);
	}
	fs.readdir('./uploads', (err, files) => {
		if (err) {
			return logWarn('Unable to scan directory: ' + err);
		}
		files.forEach(file => {
			fs.readFile('./assets/roosters.json', 'utf-8', function (err, roostersString) {
				if (err) throw err;
				let roosters: IRoosterData = {};
				if (roostersString.length > 0) {
					roosters = JSON.parse(roostersString);
				}

				const school = file.split('-rooster')[0];
				if (roosters[school] !== undefined) {
					if (roosters[school].raw === undefined) {
						importer(school);
					}
					if (roosters[school].raw !== undefined && roosters[school].classsorted === undefined) {
						Sorter(school);
					}
				} else {
					importer(school);
				}
			});
		});
	});
};

export const ErrorResolver = async (
	resErrors: {
		name: string;
		value: ObjectId;
	},
	type: string
): Promise<void> => {
	// reserrors = [{name="errorname", value="ObjectId"}]
	if (!Array.isArray(resErrors) || !resErrors?.[0]) return;
	let errors = (await Util.LoadErrors('gtsm', type)) as string[];
	let fixedErrors = [];

	// Check if error's actually resolved in DB
	await Promise.all(
		(fixedErrors = resErrors.filter(async err => {
			let existdata = {};
			switch (type) {
				case 'Teacher':
					existdata = await Teacher.findById(err.value);
					break;
				case 'Class':
					existdata = await Class.findById(err.value);
					break;
				case 'Lokaal':
					existdata = await Lokaal.findById(err.value);
					break;
			}
			if (!existdata) return false;
			return true;
		}))
	);

	// Remove from error's file
	fixedErrors.map(ferr => {
		errors = errors.filter(err => !ferr.name.match(new RegExp(`${err}`, 'gmi')));
	});
	Util.SaveErrors(errors, 'gtsm', type);
};

// Busy functions
const StartBusyChecker = async () => {
	// eslint-disable-next-line no-constant-condition
	while (true) {
		if (Object.entries(actions).length > 0) {
			Busy = true;
		} else {
			Busy = false;
		}
		await new Promise<void>((res, _rej) => {
			setTimeout(() => {
				res();
			}, 1000);
		});
	}
};

export const isBusy = (): boolean => {
	return Busy;
};
