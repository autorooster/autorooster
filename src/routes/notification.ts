import { Router } from 'express';
import { INotification } from '../typings/notification';
import { verifyHigher } from './verifyToken';

/*Logic*/
let qadminnotis: INotification[] = [];

export const addAdminNotification = (msg: INotification['msg'], type: INotification['type']) => {
	if (!msg || !type) return;
	qadminnotis.push({ msg, type });
};

/*Endpoints*/
const router = Router();

router.get('/retrieve', verifyHigher, (req, res) => {
	res.status(200).send(qadminnotis);
	qadminnotis = [];
});

router.post('/add', (req, res) => {
	addAdminNotification(req.body.msg, req.body.type);
	return res.sendStatus(200);
});

export default router;
