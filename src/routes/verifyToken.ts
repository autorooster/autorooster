import * as express from 'express';
import jwt from 'jsonwebtoken';
import User from '../model/User';

export const verify = (req: express.Request, res: express.Response, next: express.NextFunction): any => {
	const validated: {
		valid: boolean;
		id?: any;
		code?: number;
	} = verifyToken(req.body.token);
	if (validated.valid !== true) {
		switch (validated.code) {
			case 400:
				return res.status(400).send({ msg: 'Invalid Token' });
			default:
				return res.sendStatus(validated.code);
		}
	}
	req.body._user = validated.id;
	next();
};

export const verifyHigher = async (
	req: express.Request,
	res: express.Response,
	next: express.NextFunction
): Promise<any> => {
	const validated: {
		valid: boolean;
		id?: any;
		code?: number;
	} = verifyToken(req.body.token);
	if (validated.valid !== true) {
		switch (validated.code) {
			case 400:
				return res.status(400).send({ msg: 'Invalid Token' });
			default:
				return res.sendStatus(validated.code);
		}
	}
	req.body._user = validated.id;
	try {
		const user = await User.findById(req.body._user);
		// check if user actually exists
		if (!user) return res.status(404).send({ msg: `User not found` });

		if (user.permlevel === 'user') return res.status(403).send({ msg: 'Je hebt hier geen toegang toe' });
		// Passed the check go on with the Request
		next();
	} catch (err) {
		return res.status(500).send({ msg: `couldn't retrieve user information` });
	}
};

const verifyToken = (token: string) => {
	if (!token)
		return {
			valid: false,
			code: 401,
		};
	try {
		const verified = jwt.verify(token, process.env.JWTSECRETTOKEN);
		return {
			valid: true,
			id: verified,
		};
	} catch (err) {
		return {
			valid: false,
			code: 400,
		};
	}
};
