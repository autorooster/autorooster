import { Router } from 'express';
import bcrypt from 'bcryptjs';
import fs from 'fs';
import { verify } from './verifyToken';
import { schoolConfig } from '../model/Config';
import User from '../model/User';
import { logWarn, logError } from '../lib/logger';
import { ISchoolConfig, IUser } from '../typings/model';

const router = Router();
let schoolname = null;

export default router;

router.get('/getinfo', verify, async (req, res) => {
	const user = (await User.findOne({ _id: req.body._user })) as IUser;
	switch (req.body.type) {
		case 'user':
			res.status(200).json({
				username: user.name,
				mail: user.email,
			});
			break;
		case 'school':
			schoolname = req.body.school;
			// eslint-disable-next-line no-case-declarations
			let schoolabbv = '';
			schoolabbv = user.school[0];
			await schoolConfig
				.findOne()
				.or([{ name: schoolname }, { abbreviation: schoolabbv }])
				.then(async (schoolinfo: ISchoolConfig) => {
					if (schoolinfo) {
						//res data from DB
						fs.access(
							'./uploads/' + schoolinfo.name + '-rooster.txt',
							fs.constants.F_OK | fs.constants.R_OK,
							async err => {
								if (err) {
									schoolinfo.roosteruploaded = false;
								} else {
									schoolinfo.roosteruploaded = true;
								}
							}
						);
						res.status(200).json(schoolinfo);
					} else {
						//res default template
						if (schoolabbv === '') {
							//return he must make contact with admin of school fz
						} else {
							const school = new schoolConfig({
								name: 'Default school name',
								abbreviation: schoolabbv,
								passlength: 6,
							});
							try {
								await school.save();
								res.type('application/json');
								res.status(200).json(school);
							} catch (err) {
								res.status(400).json({ msg: err });
							}
						}
					}
				})
				.catch(error => {
					logError(error);
					throw new Error(error);
					res.sendStatus(400);
				});
			break;
		default:
			res.sendStatus(404);
			break;
	}
});

router.post('/updateinfo', verify, async (req, res) => {
	const user = (await User.findOne({ _id: req.body._user._id })) as IUser;
	switch (req.body.type) {
		case 'user':
			if (user) {
				const usableinfo = {
					name: '',
					email: '',
					password: '',
					oldpassword: '',
				};
				const usedinfo: {
					[info: string]: string;
				} = {};
				for (const key in req.body.info) {
					if (Object.hasOwnProperty.call(req.body.info, key)) {
						const element = req.body.info[key];
						if (!Object.hasOwnProperty.call(usableinfo, key)) {
							return;
						}
						if (!key.includes('password')) {
							usedinfo[key] = element;
						} else if (key === 'password') {
							const validPass = await bcrypt.compare(req.body.info.oldpassword, user.password);
							if (!validPass) {
								return res.status(400).json({ msg: 'Foutief wachtwoord' });
							}
							//password hashing
							const salt = await bcrypt.genSalt(10);
							usedinfo['password'] = await bcrypt.hash(req.body.info.password, salt);
						}
					}
				}
				await user.updateOne(usedinfo);
				return res.status(200).end();
			} else {
				return res.status(400).end();
			}
			break;

		case 'school':
			if (user) {
				const schoolnameuser = user.school[0];
				if (schoolnameuser) {
					const school = await schoolConfig.findOne({
						abbreviation: schoolnameuser,
					});
					if (school) {
						const usableinfo = {
							name: '',
							abbreviation: '',
							passlength: 6,
						};
						const usedinfo: {
							[k: string]: string | number;
						} = {};
						for (const key in req.body.info) {
							if (Object.hasOwnProperty.call(req.body.info, key)) {
								const element = req.body.info[key];
								if (Object.hasOwnProperty.call(usableinfo, key)) {
									usedinfo[key] = element;
								}
							}
						}
						await school.updateOne(usedinfo);
						return res.status(200).end();
					} else {
						logWarn('SCHOOL NOT FOUND');
						return res.status(400).json({ msg: 'School not found' });
					}
				} else {
					logWarn('SCHOOLNAME NOT FOUND');
					return res.status(400).json({ msg: 'User not part of school' });
				}
			} else {
				logWarn('USER NOT FOUND');
				return res.sendStatus(403);
			}
			break;

		default:
			return res.status(404).end();
			break;
	}
});

router.get('/removeuploadrooster', verify, async (req, res) => {
	try {
		let resCode = 404;
		let resInfo = {};
		const user = await User.findOne({ _id: req.body._user });
		fs.access('./uploads/' + user.school[0] + '-rooster.txt', fs.constants.F_OK | fs.constants.R_OK, async err => {
			if (err) {
				resCode = 404;
				resInfo = { msg: "Rooster wasn't found" };
				return;
			} else {
				return fs.unlink('./uploads/' + user.school[0] + '-rooster.txt', err => {
					if (err) {
						resCode = 500;
						resInfo = { msg: "Couldn't remove file" };
						return;
					} else {
						resCode = 200;
						resInfo = {};
						return;
					}
				});
			}
		});
		return res.status(resCode).json(resInfo);
	} catch (err) {
		logError('error: ' + err);
		new Error(err);
		return res.sendStatus(500);
	}
});
