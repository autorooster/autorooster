import { Router } from 'express';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import User from '../model/User';
//import { verify } from './verifyToken';
import { registerValidation, loginValidation } from '../lib/validation';
import { logError } from '../lib/logger';
import { verify } from './verifyToken';

const router = Router();

export default router;

const registerErrors = [
	{
		original: '"name" length must be at least 6 characters long',
		translate: 'Je gebruikersnaam moet minstens 6 karakters hebben!',
	},
	{
		original: '"email" length must be at least 6 characters long',
		translate: 'Je email is niet lang genoeg (6 karakters)!!',
	},
	{
		original: '"email" must be a valid email',
		translate: 'Je email is niet correct!!',
	},
	{
		original: '"password" length must be at least 6 characters long',
		translate: 'Je wachtwoord is niet lang genoeg (6 karakters)!',
	},
];

router.post('/register', async (req, res) => {
	//Data validation
	const { error } = registerValidation(req.body);
	if (error) {
		return res.status(400).json({
			msg: registerErrors.find(doc => doc.original == error.details[0].message)?.translate ?? error.details[0].message,
		});
	}

	//check for double email's & usernames
	const emailExist = await User.findOne({ email: req.body.email });
	if (emailExist) {
		return res.status(400).json({ msg: 'Email is al reeds geregistreerd' });
	}
	const userExist = await User.findOne({ name: req.body.name });
	if (userExist) {
		return res.status(400).json({ msg: 'Gebruiker is al reeds geregistreerd' });
	}

	//password hashing
	const salt = await bcrypt.genSalt(10);
	const hashPassword = await bcrypt.hash(req.body.password, salt);

	//User creation
	const user = new User({
		name: req.body.name,
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		email: req.body.email,
		password: hashPassword,
	});
	try {
		const savedUser = await user.save();
		return res.status(200).json({
			token: savedUser._id,
			username: `${savedUser.lastname} ${savedUser.firstname.charAt(0)}.`,
			perms: savedUser.permlevel,
		});
	} catch (err) {
		return res.status(400).json({ msg: err });
	}
});

router.post('/login', async (req, res) => {
	const { error } = loginValidation(req.body);
	if (error) {
		return res.status(400).json({ msg: error.details[0].message });
	}
	//Checks if email exists
	const user = await User.findOne({ email: req.body.email });
	if (!user) {
		return res.status(400).json({ msg: 'Email is nog niet geregistreerd' });
	}
	//checking & decrypting pass
	const validPass = await bcrypt.compare(req.body.password, user.password);
	if (!validPass) {
		return res.status(400).json({ msg: 'Foutief wachtwoord' });
	}

	//generate JsonWebToken
	const token = jwt.sign({ _id: user._id }, process.env.JWTSECRETTOKEN);
	res.type('application/json');
	return res.status(200).json({
		token: token,
		username: `${user.lastname} ${user.firstname.charAt(0)}.`,
		perms: user.permlevel,
		klas: user.klas,
	});
});

router.get('/checktoken', verify, async (req, res) => {
	try {
		const user = await User.findOne({ _id: req.body._user });
		if (req.body.admin && user.permlevel === 'user') {
			return res.sendStatus(403);
		}
		return res.status(200).send({ perms: user?.permlevel });
	} catch (err) {
		logError(err);
		new Error(err.message);
		return res.status(500).send({ err });
	}
});
