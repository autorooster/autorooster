import path from 'path';
import { Router } from 'express';
import { verify, verifyHigher } from './verifyToken';
import User, { userInfo } from '../model/User';
import { Teacher, Class, Schema, Lokaal } from '../model/Data';
import { logError, logInfo } from '../lib/logger';
import { IClass, IClassBase, ITeacher, IUser, IUserBase } from '../typings/model';
import { isObjectIdValid, generatePassword, convertToCSV, writeTotmp } from '../lib/util';
import { createWorker } from '../lib/parser';
import { addAdminNotification } from './notification';

const router = Router();
export default router;

router.get('/overview', verify, async (_req, res) => {
	try {
		const info = {
			leerlingen: await User.countDocuments({}),
			leerkrachten: await Teacher.countDocuments({}),
			klassen: await Class.countDocuments({}),
			lokalen: await Lokaal.countDocuments({}),
		};
		return res.status(200).send(info);
	} catch (err) {
		logError(err);
		new Error(err);
		return res.status(500).send({ msg: 'Er is iets fout gegaan' });
	}
});

router.get('/leerlingen', verify, async (req, res) => {
	res.type('application/json');
	try {
		const user = await User.findOne({ _id: req.body._user });
		const users = await User.find({ school: { $eq: user.school[0] } });
		const resUsers: (IUserBase & { schemaId?: null })[] = await Promise.all(
			users.map(async user => {
				const schema = await Schema.findOne({ user: user._id });
				return schema ? { ...user.toObject(), schemaId: schema._id } : { ...user.toObject(), schemaId: null };
			})
		);
		res.status(200).send(resUsers);
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet opgehaald worden' });
	}
});

router.get('/leerkrachten', verify, async (req, res) => {
	if (req.body.klas) {
		const lkrs = await Teacher.find({ classes: req.body.klas });
		res.status(200).send(lkrs);
	} else {
		const lkrs = await Teacher.find({});
		res.status(200).send(lkrs);
	}
});

router.get('/klassen', verify, async (_req, res) => {
	const info: [(IClass[] | (IClassBase & { firstname?: string; lastname?: string })[])?, { [k: string]: number }?] = [];
	info[0] = (await Class.find({})) as IClass[];
	info[1] = {};

	// Fetch pupil counts
	await new Promise<void>(res => {
		info[0].forEach(async function (klas: IClass, index: number) {
			info[1][klas.name] = await User.countDocuments({ klas: klas.name });
			if (index === info[0].length - 1) res();
		});
	});

	// Fetch teacher information
	info[0] = (await Promise.all(
		info[0].map(async (klas: IClass) => {
			const objklas: IClassBase & {
				firstname?: string;
				lastname?: string;
			} = klas.toObject();
			if (objklas.teacher) {
				try {
					const lkr = (await Teacher.findOne({ _id: klas.teacher })) as ITeacher;
					(objklas.firstname = lkr.firstname), (objklas.lastname = lkr.lastname);
				} catch (err) {
					logError(err);
					new Error(err);
					return res.status(500).send({ msg: 'De data kon niet opgehaald worden' });
				}
			}
			return objklas;
		})
	)) as (IClassBase & {
		firstname?: string;
		lastname?: string;
	})[];

	return res.status(200).send(info);
});

router.get('/lokalen', verify, async (_req, res) => {
	res.type('application/json');
	try {
		const loks = await Lokaal.find({});
		res.status(200).send(loks);
	} catch (err) {
		logError(err), res.status(500).send({ msg: 'Er is iets fout gegaan' });
	}
});

//get data for modals
router.get('/modal/leerling', verify, async (req, res) => {
	res.type('application/json');
	try {
		const llr = await User.findOne({ _id: req.body.dataid });
		const klassen = await Class.find({});
		res.status(200).send([llr, klassen]);
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet opgehaald worden' });
	}
});

router.get('/modal/teacher', verify, async (req, res) => {
	res.type('application/json');
	try {
		const klassen = await Class.find({});
		const lkr = await Teacher.find({ _id: req.body.dataid });
		res.status(200).send([lkr, klassen]);
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet opgehaald worden' });
	}
});

router.get('/modal/klas', verify, async (req, res) => {
	res.type('application/json');
	try {
		const lkrs = await Teacher.find({});
		const klas = await Class.findOne({ _id: req.body.dataid });
		res.status(200).send([klas, lkrs]);
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet opgehaald worden' });
	}
});

router.get('/modal/lokaal', verify, async (req, res) => {
	res.type('application/json');
	try {
		const lokaal = await Lokaal.findOne({ _id: req.body.dataid });
		res.status(200).send(lokaal);
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet opgehaald worden' });
	}
});

//update data from modals
router.post('/update/leerling', verify, async (req, res) => {
	const data = req.body.llrinfo;
	try {
		const user = await User.findOne({ _id: data.iid });
		if (!user) {
			return res.status(406).send({ msg: 'De gebruiker werd niet gevonden' });
		}

		const info = {
			firstname: data.firstname,
			lastname: data.lastname,
			klas: data.klassen,
		};

		await user.updateOne(info);
		return res.status(200).end();
	} catch (err) {
		new Error(err);
		logError(err);
		return res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.post('/update/leerkracht', verify, async (req, res) => {
	const data = req.body.lkrinfo;
	try {
		const lkr = await Teacher.findOne({ _id: data.iid });

		const info = {
			firstname: data.firstname,
			lastname: data.lastname,
			classes: data.klassen,
			day: data.days,
		};

		await lkr.updateOne(info);
		res.status(200).end();
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.post('/update/klassen', verify, async (req, res) => {
	const data = req.body.klasinfo;
	try {
		const klas = await Class.findOne({ _id: data.iid });
		const info = {
			name: data.name,
			teacher: data.teacher,
		};
		await klas.updateOne(info);
		res.status(200).end();
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.post('/update/lokaal', verify, async (req, res) => {
	const data = req.body.lokinfo;
	try {
		const lokaal = await Lokaal.findOne({ _id: data.iid });
		const info = {
			name: data.name,
			blok: data.blok,
			type: data.loktype,
		};

		await lokaal.updateOne(info);
		res.status(200).end();
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.get('/info/klassen', verify, async (_req, res) => {
	try {
		const classes = await Class.find({});
		res.status(200).send(classes);
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.get('/info/leerkrachten', verify, async (req, res) => {
	try {
		const lkrs = await Teacher.find({});
		res.status(200).send(lkrs);
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

//add data
router.post('/add/leerling', verify, async (req, res) => {
	const data = req.body.llrinfo;
	const datastructure = {
		firstname: 'string',
		lastname: 'string',
		klas: 'string',
	};
	Object.keys(data).forEach((index: keyof typeof datastructure) => {
		if (!(typeof data[index] === datastructure[index])) {
			return res.status(400).send({ msg: 'Foute info ingegeven' });
		}
		return true;
	});

	try {
		// Create new user
		const user = new User({
			name: `${data.lastname}-${data.firstname}`,
			firstname: data.firstname,
			lastname: data.lastname,
			password: generatePassword(),
			klas: data.klas,
		});
		await user.save();
		return res.sendStatus(200);
	} catch (err) {
		new Error(err);
		logError(err);
		return res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.post('/add/leerkracht', verify, async (req, res) => {
	const data = req.body.lkrinfo;
	const datastructure = {
		klassen: 'object',
		days: 'object',
		sex: 'number',
		firstname: 'string',
		lastname: 'string',
	};
	Object.keys(data).forEach((index: keyof typeof datastructure) => {
		if (!(typeof data[index] === datastructure[index])) {
			return res.status(400).send({ msg: 'Foute info ingegeven' });
		}
		return null;
	});
	try {
		const lkr = new Teacher({
			firstname: data.firstname,
			lastname: data.lastname,
			sex: data.sex,
			classes: data.klassen,
			days: data.days,
		});
		await lkr.save();
		res.status(200).send({ id: lkr._id });
	} catch (err) {
		new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.post('/add/klas', verify, async (req, res) => {
	const data = req.body.klasinfo;
	const datastructure = {
		name: 'string',
		teacher: 'string',
	};
	Object.keys(data).forEach((index: keyof typeof datastructure) => {
		if (!(typeof data[index] === datastructure[index])) {
			return res.status(400).send({ msg: 'Foute info ingegeven' });
		}
		return null;
	});
	// eslint-disable-next-line no-unused-vars
	try {
		if (isObjectIdValid(data.teacher)) {
			const teacher = await Teacher.findById(data.teacher);
			if (!teacher) return res.status(404).send({ msg: 'De leerkracht kon niet gevonden worden' });
		}
		const klas = new Class({
			name: data.name,
			teacher: data.teacher,
		});
		await klas.save();
		return res.status(200).send({ id: klas._id });
	} catch (err) {
		throw new Error(err);
		logError(err);
		return res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.post('/add/lokaal', verify, async (req, res) => {
	const data = req.body.lokinfo;
	const datastructure = {
		name: 'string',
		blok: 'string',
		loktype: 'string',
	};
	Object.keys(data).forEach((index: keyof typeof datastructure) => {
		if (!(typeof data[index] === datastructure[index])) {
			res.status(400).send({ msg: 'Foute info ingegeven' });
		}
	});
	// eslint-disable-next-line no-unused-vars
	try {
		const lokaal = new Lokaal({
			name: data.name,
			blok: data.blok,
			type: data.loktype,
		});
		await lokaal.save();
		res.status(200).send({ id: lokaal._id });
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.get('/info/klas', verify, async (req, res) => {
	try {
		const klas = await Class.findOne({ _id: req.query.iid });
		res.status(200).send(klas);
	} catch (err) {
		throw new Error(err);
		logError(err);
		res.status(500).send({ msg: 'De data kon niet geupdate worden' });
	}
});

router.get('/checkclass', verify, async (req, res) => {
	const user = await User.findById(req.body._user);
	if (!user) return res.status(404).send({ msg: 'Gebruiker niet gevonden' });
	if (!req.body.admin) return res.status(200).end();

	//check if perms are admin or higher:
	return user.permlevel === 'user'
		? res.status(403).send({ msg: 'Je hebt hier geen toegang toe' })
		: res.status(200).send({});
});

router.post('/remove/:type', verifyHigher, async (req, res) => {
	switch (req.params.type) {
		case 'klassen':
			await Promise.all(
				req.body.ids.map(async (id: string) => {
					try {
						await Class.deleteOne({
							_id: id,
						});
					} catch (error) {
						new Error(error);
					}
				})
			);
			break;
		case 'leerlingen':
			await Promise.all(
				req.body.ids.map(async (id: string) => {
					try {
						await User.deleteOne({
							_id: id,
						});
					} catch (error) {
						new Error(error);
					}
				})
			);
			break;
		case 'leerkrachten':
			await Promise.all(
				req.body.ids.map(async (id: string) => {
					try {
						await Teacher.deleteOne({
							_id: id,
						});
					} catch (error) {
						new Error(error);
					}
				})
			);
			break;
		case 'lokalen':
			await Promise.all(
				req.body.ids.map(async (id: string) => {
					try {
						await Lokaal.deleteOne({
							_id: id,
						});
					} catch (error) {
						new Error(error);
					}
				})
			);
			break;
		default:
			return res.sendStatus(404);
	}
	return res.sendStatus(200);
});

router.get('/download', verifyHigher, async (req, res) => {
	try {
		const users: IUserBase[] = [];
		(await User.find({ passwordChanged: false, permlevel: 'user' })).map((user: IUser) => {
			users.push(user.toObject() as IUserBase);
		});
		const csv = await convertToCSV(users, Object.keys(userInfo));
		await writeTotmp('user.csv', csv);
		return res.status(200).sendFile(path.resolve(__dirname, '../../tpm/user.csv'));
	} catch (error) {
		logError(error, 'dashboard-download');
		new Error(error);
		return res.status(500).send({ msg: 'Er is iets fout gegaan tijdens het ophalen van de data' });
	}
});

router.post('/conversion/:type/:klas', verifyHigher, async (req, res) => {
	const klassen = await Schema.find({
		user: /standard-*/,
	});
	switch (req.params.type) {
		case 'all':
			klassen.map(doc => {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				createWorker(doc.user.replace('standard-', ''))
					.then(data => {
						logInfo(data);
						addAdminNotification(`Schikking schema's ${req.params.klas} succesvol beëindigd`, 'success');
					})
					.catch(err => {
						logError(err);
						addAdminNotification(
							`Er is iets fout gelopen tijdes het schikken van de schema's voor ${req.params.klas}`,
							'error'
						);
						throw new Error(err);
					});
			});
			return res.sendStatus(200);
		case 'seperate':
			createWorker(req.params.klas)
				.then(data => {
					logInfo(data);
					addAdminNotification(`Schikking schema's ${req.params.klas} succesvol beëindigd`, 'success');
				})
				.catch(err => {
					logError(err);
					addAdminNotification(
						`Er is iets fout gelopen tijdes het schikken van de schema's voor ${req.params.klas}`,
						'error'
					);
					throw new Error(err);
				});
			return res.sendStatus(200);
		default:
			return res.sendStatus(404);
	}
});
