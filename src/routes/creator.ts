import { Router } from 'express';
import path from 'path';
import fs from 'fs';
import { verify, verifyHigher } from './verifyToken';
import User from '../model/User';
import { Card, Hour } from '../model/Creator';
import { Schema, Class, Lokaal, Teacher } from '../model/Data';
import { isBusy, importer, ErrorResolver, Converter } from '../lib/roosterimport';
import * as creatorFunctions from '../lib/creator';
import { logError } from '../lib/logger';
import {
	ICard,
	ICardBase,
	ICardBaseFilled,
	ICardFilled,
	IClass,
	IHourSchema,
	IHourSchemaBase,
	IHourSchemaHours,
	ILokaal,
	IDifference,
	ISchema,
	ISchemaBase,
	ITeacher,
	IUser,
} from '../typings/model';
import fileUpload from 'express-fileupload';
import { IErrorData } from '../typings/data';
import { getRootFolder } from '..';

const router = Router();
export default router;

router.post('/addcard', verify, async (req, res) => {
	const card = new Card({
		name: req.body.info.name,
		klas: req.body.info.klas,
		hours: req.body.info.hours,
		teacher: req.body.info.teacher,
		lokaal: req.body.info.lokaal,
	});
	try {
		const savedCard = await card.save();
		res.status(200).send({ token: savedCard._id });
	} catch (err) {
		res.status(400).send({ msg: err });
	}
});

router.post('/updatecard', verify, async (req, res) => {
	if (req.body.cardid === undefined || req.body.cardid === null) return res.sendStatus(404);
	const card = await Card.findOne({ _id: req.body.cardid });
	if (!card) return res.status(404).send({ msg: 'Card not found' });

	const cardinfo: {
		[k: string]: string;
	} = {};
	const usableinfo = {
		name: '',
		klas: '',
		teacher: '',
		lokaal: '',
	};

	for (const key in req.body.info) {
		if (Object.hasOwnProperty.call(req.body.info, key)) {
			const element = req.body.info[key];

			if (Object.hasOwnProperty.call(usableinfo, key)) {
				cardinfo[key] = element;
			}
		}
	}

	await card.updateOne(cardinfo);
	return res.status(200).end();
});

router.get('/getcard', verify, async (req, res) => {
	const card = await creatorFunctions.getLesCard(req.body.id, true, true);
	return res.status(200).send(card);
});

router.get('/getcards', verify, async (req, res) => {
	try {
		let cards: ICard[] | ICardFilled[] | ICardBaseFilled[] = (await Card.find({ klas: req.body.klas })) as ICard[];
		let iserr = false;
		cards = (await Promise.all(
			cards.map(async card => {
				try {
					const strippedCard: ICardBase = card.toObject();
					return await creatorFunctions.getLesCard(strippedCard, true, true);
				} catch (err) {
					iserr = true;
					logError(err, 'creatorroute');
					return new Error(err);
				}
			})
		)) as ICardBaseFilled[];
		if (iserr) return res.sendStatus(500);
		return res.status(200).send(cards);
	} catch (err) {
		new Error(err);
		logError('error: ' + err, 'creatorroute');
		return res.sendStatus(500);
	}
});

router.get('/gethourcards', verify, async (req, res) => {
	try {
		const user = await User.findById(req.body._user);
		const hours = await Hour.findOne({ school: user.school[0], klas: req.body.klas });
		return res.status(200).send(hours);
	} catch (err) {
		new Error(err);
		logError('error: ' + err, 'creatorroute');
		return res.sendStatus(500);
	}
});

router.post('/updatehourcards', verify, async (req, res) => {
	try {
		const info = req.body.info;
		const user = await User.findOne({ _id: req.body._user });
		const hours = (await Hour.findOne({ school: user.school[0], klas: info.klas })) as IHourSchema;
		if (hours) {
			//updaten
			const usedinfo: IHourSchemaBase = {
				school: user.school[0],
				klas: info.klas,
			};
			const newHouramount = info.hours.length;
			let oldHourAmount = 0;
			let key: keyof IHourSchema;
			for (key in hours) {
				if (Object.prototype.hasOwnProperty.call(key, hours) && typeof hours[key] === 'object') {
					if (hours[key].starthour !== undefined) {
						if (key.includes('uur')) {
							oldHourAmount++;
						}
					}
				}
			}
			if (newHouramount == oldHourAmount) {
				// Just change the hours
				Object.keys(hours).map((key: keyof IHourSchemaBase) => {
					if (key.includes('uur')) {
						usedinfo[key] = info.hours[Number(key.split('uur')[1]) + 1];
					}
				});
				await hours.updateOne(usedinfo);
				return res.status(200).end();
			} else if (newHouramount > oldHourAmount) {
				// Add some hours
				if (newHouramount - oldHourAmount > 8) {
					return res.status(400).send({ msg: 'Too many hours' });
				}

				for (const key in info.hours) {
					const uurkey: keyof IHourSchemaBase = ('uur' + (Number(key) + 1)) as keyof IHourSchemaBase;
					usedinfo[uurkey] = info.hours[key];
				}

				await hours.updateOne(usedinfo);
				return res.status(200).end();
			} else if (newHouramount < oldHourAmount) {
				// Remove hours from cards
				// eg. delete info.uur8
				const diffhours = oldHourAmount - newHouramount;
				// Eerst de data toevoegen
				Object.keys(hours).map((key: keyof IHourSchemaBase) => {
					if (key.includes('uur')) {
						usedinfo[key] = info.hours[Number(key.split('uur')[1]) - 1];
					}
				});

				for (let i = 0; i < diffhours; i++) {
					if (usedinfo[('uur' + (oldHourAmount - i)) as keyof IHourSchemaBase]) {
						usedinfo[('uur' + (oldHourAmount - i)) as keyof IHourSchemaHours] = {};
					}
				}

				await hours.updateOne(usedinfo);
				return res.status(200).end();
			} else {
				return res.sendStatus(500);
			}
		} else if (!hours) {
			//toevoegen
			/* 
				info ={
					klas,
					hours = [{
						starthour,
						startmin,
					},{}]
				}
			*/
			const hourinfo: {
				school: string;
				klas: string;
			} = {
				school: user.school[0],
				klas: info.klas,
			};

			for (let index = 0; index < info.hours.length; index++) {
				const element = info.hours[index];
				hourinfo[('uur' + (index + 1)) as keyof typeof hourinfo] = element;
			}

			//create it in DB
			const hour = new Hour(hourinfo);
			try {
				// eslint-disable-next-line no-unused-vars
				await hour.save();
				res.status(200).end();
			} catch (err) {
				res.status(400).send({ msg: err });
			}
		}
	} catch (err) {
		new Error(err.stack);
		logError('error: ' + err.stack, 'creatorroute');
		return res.sendStatus(500);
	}
});

router.post('/uploadschemafile', verify, async (req, res) => {
	// Uploading file
	try {
		const user = await User.findOne({ _id: req.body._user });
		if (!req.files || Object.keys(req.files).length === 0) {
			return res.status(400).send({ msg: 'No files were uploaded.', msgType: 'warning' });
		}

		// The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
		const File = req.files.rooster as fileUpload.UploadedFile;
		const uploadPath = './uploads/' + user.school[0] + '-rooster' + path.extname(File.name);
		const folder = path.resolve(getRootFolder(), './uploads');
		if (!fs.existsSync(folder)) {
			fs.mkdirSync(folder);
		}

		// Use the mv() method to place the file somewhere on your server
		await new Promise<void>((resolve, reject) => {
			File.mv(uploadPath, err => {
				if (err) {
					res.status(500).send({ msg: err });
					reject();
				}
				res.send('File uploaded!');
				req.body.headers = JSON.parse(req.body.headers);
				importer(user.school[0], req.body.headers);
				resolve();
			});
		});
		return res.sendStatus(200);
	} catch (err) {
		new Error(err);
		logError('error: ' + err, 'creatorroute');
		return res.sendStatus(500);
	}
});

router.get('/isroosteruploaded', verify, async (req, res) => {
	try {
		const user = await User.findOne({ _id: req.body._user });
		const folder = path.resolve(getRootFolder(), './uploads');
		if (!fs.existsSync(folder)) {
			fs.mkdirSync(folder);
		}
		return fs.access(
			'./uploads/' + user.school[0] + '-rooster.txt',
			fs.constants.F_OK | fs.constants.R_OK,
			async err => {
				if (err) {
					return res.status(200).send({
						found: false,
					});
				} else {
					if (isBusy()) {
						return res.status(200).send({
							found: true,
							busy: true,
						});
					} else {
						importer(user.school[0]);
						return res.status(200).send({
							found: true,
							busy: false,
						});
					}
				}
			}
		);
	} catch (err) {
		new Error(err.stack);
		logError('error: ' + err.stack, 'creatorroute');
		return res.sendStatus(500);
	}
});

router
	.post('/lesschema', verify, async (req, res) => {
		// Upload
		try {
			const user = await User.findOne({ _id: req.body._user });
			const schema = (await Schema.findOne({
				user: user.permlevel === 'user' ? user._id : `standard-${req.body.klas}`,
			})) as ISchema;
			if (!schema) {
				// Upload new schema
				const differences: IDifference[] = [];
				req.body.diff.map((diff: IDifference) => {
					differences.push(diff);
				});
				const newschema = new Schema({
					user: user.permlevel === 'user' ? req.body._user : `standard-${req.body.klas}`,
					data: req.body.schema,
					differences,
				});
				// eslint-disable-next-line no-unused-vars
				await newschema.save();
				return res.status(200).send({
					msg: 'Upload succesvol',
					msgType: 'success',
				});
			}

			if (req.body.force !== undefined && req.body.force) {
				await schema.updateOne({
					data: req.body.schema,
				});

				const llrs = (await User.find({
					klas: req.body.klas,
					permlevel: 'user',
				})) as IUser[];

				llrs.map(async llr => {
					await Schema.deleteOne({ user: llr._id });
				});

				return res.status(200).send({
					msg: 'Schema succesvol overschreven',
					msgType: 'success',
				});
			}

			return res.status(200).send({
				msg: 'Er bestaat al een schema!',
				msgType: 'warning',
				action: 'confirm',
			});
		} catch (err) {
			new Error(err.stack);
			logError('error: ' + err.stack, 'creatorroute');
			return res.sendStatus(500);
		}
	})
	.get('/lesschema', verify, async (req, res) => {
		// Check if uploaded
		try {
			const user = await User.findById(req.body._user);
			let schema: ISchema = await Schema.findOne({
				user: user.permlevel === 'user' ? user._id : `standard-${req.body.klas}`,
			});
			// User doesn't has a schema, get standard schema
			if (!schema) {
				if (!user)
					return res.status(200).send({
						type: 'empty',
						payload: {},
					});
				schema = await Schema.findOne({
					user: `standard-${user.permlevel === 'user' ? user.klas : req.body.klas}`,
				}).exec();
			}
			//No standard schema to start of
			if (!schema && user.permlevel !== 'user') {
				// Start conversion
				if (user.school[0] && req.body.klas) new Converter(user.school[0], req.body.klas);
				return res.status(200).send({
					type: 'empty',
					payload: {},
				});
			}
			const schemaObj: ISchemaBase = schema.toObject() as ISchemaBase;
			if (!creatorFunctions.validSchema(schemaObj))
				return res.status(200).send({
					type: 'invalid',
					payload: {},
				});
			return res.status(200).send({
				type: 'succes',
				payload: schemaObj.data,
			});
		} catch (err) {
			logError('error: ' + err.stack, 'creatorroute');
			new Error(err.stack);
			return res.sendStatus(500);
		}
	});

router.get('/getimporterrors', verify, async (req, res) => {
	try {
		const user = await User.findOne({ _id: req.body._user });
		if (!user.school[0]) {
			return res.status(500).send({
				msg: 'School was not defined',
				msgType: 'error',
			});
		}
		return fs.readFile('./assets/errors.json', 'utf-8', async (err, ordata) => {
			if (err) {
				new Error(err.message);
				logError('error: ' + err, 'creatorroute');
				return res.status(500).send({
					msg: "Couldn't find error's",
					msgType: 'error',
				});
			}

			const data: IErrorData = JSON.parse(ordata);

			let i = 0;
			while (typeof data !== 'object') {
				if (i > 100) break;
				await new Promise<void>((res, _rej) => {
					setTimeout(() => {
						i++;
						res();
					}, 100);
				});
			}
			if (i >= 100)
				return res.status(500).send({
					msg: 'Timeout',
					msgType: 'error',
				});

			if (!data[user.school[0]]) {
				return res.status(200).json({
					msg: "Geen error's gevonden",
					msgType: 'info',
				});
			}
			if (req.body.errtype) {
				if (data?.[user.school[0]]?.[req.body.errtype]) {
					// Get errtype
					let dataDB: IClass[] | ITeacher[] | ILokaal[] | ISchema[];
					let statusCode = 200;
					let statusData: {
						errors: any;
						existing?: any;
					} = {
						errors: data[user.school[0]][req.body.errtype],
					};
					switch (req.body.errtype) {
						case 'Class':
							dataDB = (await Class.find()) as IClass[];
							statusData = {
								...statusData,
								existing: dataDB,
							};
							break;
						case 'Schema':
							dataDB = (await Schema.find()) as ISchema[];
							statusData = {
								...statusData,
								existing: dataDB,
							};
							break;
						case 'Lokaal':
							dataDB = (await Lokaal.find()) as ILokaal[];
							statusData = {
								...statusData,
								existing: dataDB,
							};
							break;
						case 'Teacher':
							dataDB = (await Teacher.find()) as ITeacher[];
							statusData = {
								...statusData,
								existing: dataDB,
							};
							break;
						default:
							statusCode = 200;
							statusData = {
								errors: data[user.school[0]][req.body.errtype],
							};
							break;
					}
					return res.status(statusCode).json(statusData);
				} else {
					return res.status(404).json({
						redirect: true,
						msg: "Geen error's gevonden",
						msgType: 'info',
					});
				}
			} else {
				return res.status(200).json({ errors: data[user.school[0]] });
			}
		});
	} catch (err) {
		logError('error: ' + err, 'creatorroute');
		new Error(err);
		return res.sendStatus(500);
	}
});

router.post('/reserrors', verify, verifyHigher, async (req, res) => {
	// Data wil be saved in req.body.errors as Array
	// Check to make sure data is an Array and that it actually has data stored in it
	if (!Array.isArray(req.body.errors) || !req.body?.errors?.[0])
		return res.status(409).send({ msg: 'No errors found', msgType: 'warning' });

	ErrorResolver(req.body.errors, req.body.errorType);

	return res.status(200).send({ msg: 'Je oplossing zijn ogelsagen' });
});
