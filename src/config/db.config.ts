import mongoose from 'mongoose';
import { logInfo, logError, logWarn } from '../lib/logger';

export const connect = (source?: string): void => {
	const MONGODB_URI =
		process.env.MONGODB_URI ||
		(() => {
			logWarn('No MongoDB URI given');
			return `mongodb://localhost/autorooster`;
		})();
	mongoose
		.connect(MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
		.then((details: any) => {
			const { name } = details.connections[0];
			logInfo(`MongoDB connected to ${name}`, source);
		})
		.catch((err: any) => {
			logError(`Error while connecting to MongoDB`, source);
			logError(err.stack, source);
			throw new Error(err.message);
		});
};
