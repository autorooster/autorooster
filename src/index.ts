import express from 'express';
import fileUpload from 'express-fileupload';
import * as path from 'path';
import cors from 'cors';
import * as dotenv from 'dotenv';
import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';
import { AutoImporter } from './lib/roosterimport';
//Routes
import authRoute from './routes/auth';
import dataRoute from './routes/data';
import configRoute from './routes/config';
import creatorRoute from './routes/creator';
import notiRoute from './routes/notification';
import { logInfo, logError } from './lib/logger';
import { connect } from './config/db.config';

const app = express();
dotenv.config();

// Sentry
Sentry.init({
	dsn: process.env.SENTRY_KEY,
	integrations: [
		// enable HTTP calls tracing
		new Sentry.Integrations.Http({ tracing: true }),
		// enable Express.js middleware tracing
		new Tracing.Integrations.Express({ app }),
	],

	// Set tracesSampleRate to 1.0 to capture 100%
	// of transactions for performance monitoring.
	// We recommend adjusting this value in production
	tracesSampleRate: 1.0,
	environment: process.env.NODE_ENV === 'production' ? 'production' : 'development',
});

//Database connection
connect();

// Set Project root as exported func
export const getRootFolder = (): string => {
	return path.resolve(__dirname, '../.');
};

// Setup CORS
const whitelist = ['localhost', 'nuttyshrimp.me'];
if (process.env.CORSIP) {
	whitelist.push(process.env.CORSIP);
}

//( req:any, callback: ( arg0: Error|null, arg1: boolean ) => void )
const corsOptions = {
	origin: function (
		origin: string | undefined,
		callback: (err: Error | null, origin?: boolean | string | RegExp | (string | RegExp)[]) => void
	) {
		// allow requests with no origin
		if (!origin) return callback(null, true);
		origin = origin.replace(/(https?:\/\/)/, '');
		if (whitelist.indexOf(origin) === -1) {
			const message = `The CORS policy for this origin doesn't allow access from the particular origin.`;
			logError(`${origin} was blocked by CORS`);
			new Error(`${origin} was blocked by CORS`);
			return callback(new Error(message), false);
		}
		return callback(null, true);
	},
};

//Middleware for request
app.use(express.json());
app.use(fileUpload());
// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());
// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());
app.use(cors(corsOptions));
app.use((req: express.Request, _res: express.Response, next: express.NextFunction) => {
	if (req.method === 'GET') {
		req.body = {
			...req.body,
			...req.query,
		};
	}
	next();
});

// Public folder
const staticPath = path.join(__dirname, '../public');
app.use(express.static(staticPath, { extensions: ['html'] }));

app.get('/', (req, res) => {
	res.sendFile('../public/index.html');
});

app.use('/api/user', authRoute);
app.use('/api/data', dataRoute);
app.use('/api/config', configRoute);
app.use('/api/creator', creatorRoute);
app.use('/api/notification', notiRoute);

// Also capture 400+ request
app.use(
	Sentry.Handlers.errorHandler({
		shouldHandleError(error) {
			if (String(error.status).charAt(0) === '4' || String(error.status).charAt(0) === '5') {
				return true;
			}
			return false;
		},
	})
);

app.listen(process.env.SERVERPORT || 3000, () => {
	logInfo('Server Up on ' + (process.env.SERVERPORT || 3000));
	// Start import for non imported schema's
	try {
		AutoImporter();
	} catch (err) {
		logError(err);
		new Error(err);
	}
});
