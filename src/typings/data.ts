import { ObjectId } from 'mongoose';
import { IWeekdagen } from './generic';

export declare type ILesSchemaData = {
	[dag in IWeekdagen]: {
		[lesnr: number]: ObjectId;
	};
};

export declare interface IErrorDataSchool {
	[type: string]: string[];
}

export declare interface IErrorData {
	[school: string]: IErrorDataSchool;
}

export declare interface IHeadersData {
	[school: string]: string[];
}

export declare interface IRoosterRawData {
	sessionid: number;
	klas: string;
	leerkracht: string;
	vak: string;
	lokaal: string;
	dag: number;
	lesuur: number;
	comment: undefined | null | string;
	comment2: undefined | null | string;
}

export declare interface IRoosterHourData {
	leerkracht: string;
	lokaal: string;
	vak: string;
}

export interface IRoosterClassSorted {
	[klas: string]: [
		(IRoosterHourData[] | null[])?,
		(IRoosterHourData[] | null[])?,
		(IRoosterHourData[] | null[])?,
		(IRoosterHourData[] | null[])?,
		(IRoosterHourData[] | null[])?
	];
}

export declare interface IRoosterDataSchool {
	raw?: IRoosterRawData[];
	classsorted?: IRoosterClassSorted;
	schema?: {
		[klas: string]:
			| {
					[k in IWeekdagen]: {
						[k: number]: ObjectId;
					};
			  }
			| Record<string, never>;
	};
}

export declare interface IRoosterData {
	[school: string]: IRoosterDataSchool | Record<string, never>;
}
