export interface INotification {
	msg: string;
	type: 'error' | 'warning' | 'info' | 'success';
}
