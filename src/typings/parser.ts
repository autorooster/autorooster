import { ICard, ISchemaBase } from './model';
import mongoose from 'mongoose';

export interface IDiff {
	schema?: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId;
	origin: {
		day: number;
		hour: number;
		/**
		 * Represents cardid
		 */
		id: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId;
		info?: ICard;
	};
	target: {
		day: number;
		hour: number;
		/**
		 * Represents cardid
		 */
		id: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId | 0;
		info?: ICard;
	};
}

export type IDiffWorker = (workerData: {
	userschemas: ({
		_id?: mongoose.Types.ObjectId | mongoose.Schema.Types.ObjectId;
	} & ISchemaBase)[];
	klas: string;
}) => Promise<
	{
		schema: IDiff['schema'];
		diffindex: number;
	}[]
>;
