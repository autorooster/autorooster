import { IWeekdagen, Modify } from './generic';
import { ObjectId, Document } from 'mongoose';
import { ILesSchemaData } from './data';

declare interface IHourSchemaObject {
	starthour: number;
	startmin: number;
	endhour: number;
	endmin: number;
}

export declare interface ISchoolConfig extends Document {
	name: string;
	abbreviation?: string;
	logo?: string;
	passlength: number;
	roosteruploaded: boolean;
}

export declare interface ICardBase {
	name: string;
	klas: string;
	teacher: ObjectId;
	hours: number;
	lokaal: ObjectId;
}

export type ICardBaseFilled = Modify<
	ICardBase,
	{
		teacher: {
			_id?: ObjectId;
		} & ITeacherBase;
		lokaal: {
			_id?: ObjectId;
		} & ILokaalBase;
	}
>;

export declare interface ICard extends Document, ICardBase {}

export type ICardFilled = Modify<
	ICard,
	{
		teacher: {
			_id: ObjectId;
		} & ITeacherBase;
		lokaal: {
			_id: ObjectId;
		} & ILokaalBase;
	}
>;

export declare interface IHourSchemaHours {
	uur1?: IHourSchemaObject | Record<string, never>;
	uur2?: IHourSchemaObject | Record<string, never>;
	uur3?: IHourSchemaObject | Record<string, never>;
	uur4?: IHourSchemaObject | Record<string, never>;
	uur5?: IHourSchemaObject | Record<string, never>;
	uur6?: IHourSchemaObject | Record<string, never>;
	uur7?: IHourSchemaObject | Record<string, never>;
	uur8?: IHourSchemaObject | Record<string, never>;
	uur9?: IHourSchemaObject | Record<string, never>;
}
export declare interface IHourSchemaBase extends IHourSchemaHours {
	school: string;
	klas: string;
}

export declare interface IHourSchema extends Document, IHourSchemaBase {}

export declare interface ITeacherBase {
	firstname: string;
	lastname: string;
	sex: number;
	classes: string[];
	days: IWeekdagen[];
	abbreviation: string;
}

export declare interface ITeacher extends Document, ITeacherBase {}

export declare interface IClassBase {
	name: string;
	teacher: ObjectId;
}
export declare interface IClass extends Document, IClassBase {}

export declare interface ILokaalBase {
	name: number | undefined;
	blok: string | number | undefined;
	type: string;
}

export declare interface ILokaal extends Document, ILokaalBase {}

export declare interface IDifference {
	origin: {
		day: number;
		hour: number;
		/**
		 * Represents cardid
		 */
		id: ObjectId;
	};
	target: {
		day: number;
		hour: number;
		/**
		 * Represents cardid
		 */
		id: ObjectId | 0;
	};
}
export declare interface ISchemaBase {
	user: string | ObjectId;
	data: ILesSchemaData;
	differences: {
		[k: string]: IDifference;
	};
}

export declare interface ISchema extends Document, ISchemaBase {}

export declare interface IUserBase {
	name: string;
	firstname: string;
	lastname: string;
	email: string;
	password: string;
	passwordChanged: boolean;
	date: Date;
	permlevel: string;
	school: string[];
	klas: string;
}

export declare interface IUser extends Document, IUserBase {}
