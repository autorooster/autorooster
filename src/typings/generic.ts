export declare type IWeekdagen = 'Maandag' | 'Dinsdag' | 'Woensdag' | 'Donderdag' | 'Vrijdag';

export type Modify<T, R> = Omit<T, keyof R> & R;
