import mongoose from 'mongoose';
import { IUser } from '../typings/model';

export const userInfo = {
	name: {
		type: String,
		required: true,
		min: 6,
		max: 255,
	},
	firstname: {
		type: String,
		required: true,
		max: 100,
	},
	lastname: {
		type: String,
		required: true,
		max: 100,
	},
	email: {
		type: String,
		required: false,
		max: 255,
		min: 6,
	},
	password: {
		type: String,
		required: true,
		max: 1024,
		min: 6,
	},
	passwordChanged: {
		type: Boolean,
		default: false,
	},
	date: {
		type: Date,
		default: Date.now,
	},
	permlevel: {
		type: String,
		default: 'user',
	},
	school: {
		type: Array,
		required: true,
		default: ['gtsm'],
	},
	klas: {
		type: String,
		required: false,
	},
};

const UserSchema = new mongoose.Schema(userInfo);

const User = mongoose.model<IUser>('User', UserSchema);

export default User;
