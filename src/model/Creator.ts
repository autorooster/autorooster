import mongoose from 'mongoose';
import { ICard, IHourSchema } from '../typings/model';

const CardSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		max: 50,
	},
	klas: {
		type: String,
		required: true,
		max: 50,
	},
	teacher: {
		type: mongoose.Types.ObjectId,
		ref: 'Teacher',
		required: true,
	},
	hours: {
		type: Number,
		required: true,
		min: 0,
		max: 9,
	},
	lokaal: {
		type: mongoose.Types.ObjectId,
		ref: 'Lokaal',
		required: true,
	},
});

const HourSchema = new mongoose.Schema({
	school: {
		type: String,
		required: true,
	},
	klas: {
		type: String,
		required: true,
	},
	uur1: {
		type: Object,
		required: true,
	},
	uur2: {
		type: Object,
	},
	uur3: {
		type: Object,
	},
	uur4: {
		type: Object,
	},
	uur5: {
		type: Object,
	},
	uur6: {
		type: Object,
	},
	uur7: {
		type: Object,
	},
	uur8: {
		type: Object,
	},
	uur9: {
		type: Object,
	},
});

export const Card = mongoose.model<ICard>('Card', CardSchema);
export const Hour = mongoose.model<IHourSchema>('Hour', HourSchema);
