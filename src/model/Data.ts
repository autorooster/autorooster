import mongoose from 'mongoose';
import { IClass, ILokaal, ISchema, ITeacher } from '../typings/model';

const TeacherSchema = new mongoose.Schema({
	firstname: {
		type: String,
		required: true,
	},
	lastname: {
		type: String,
		required: true,
	},
	sex: {
		type: Number,
		default: 0,
	},
	classes: {
		type: Array,
	},
	days: {
		type: Array,
		default: ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'],
	},
	abbreviation: {
		type: String,
	},
});

const ClassesSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	teacher: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Teacher',
		required: true,
	},
});

const LokaalSchema = new mongoose.Schema({
	name: {
		type: Number,
		required: true,
	},
	blok: {
		type: String,
		required: true,
	},
	type: {
		type: String,
		required: true,
		default: 'standaard',
	},
});

const SchemaSchema = new mongoose.Schema({
	user: {
		type: String,
		required: true,
	},
	data: {
		type: Object,
		required: true,
	},
	differences: {
		type: Object,
		default: {},
	},
});

export const Teacher = mongoose.model<ITeacher>('Teacher', TeacherSchema);
export const Class = mongoose.model<IClass>('Class', ClassesSchema);
export const Schema = mongoose.model<ISchema>('LesSchema', SchemaSchema);
export const Lokaal = mongoose.model<ILokaal>('Lokaal', LokaalSchema);
