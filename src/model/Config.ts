import mongoose from 'mongoose';
import { ISchoolConfig } from '../typings/model';

const schoolConfigSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		min: 6,
		max: 255,
	},
	abbreviation: {
		type: String,
		required: false,
		min: 6,
		max: 255,
	},
	logo: {
		type: String,
		required: false,
		min: 6,
		max: 4096,
	},
	passlength: {
		type: Number,
		required: true,
		min: 6,
	},
	roosteruploaded: {
		type: Boolean,
		default: false,
	},
});

export const schoolConfig = mongoose.model<ISchoolConfig>('schoolConfig', schoolConfigSchema);
