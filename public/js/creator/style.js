let cardheight = 0
// eslint-disable-next-line no-unused-vars
let stylefileloaded = true

// Cards all the same height

// eslint-disable-next-line no-unused-vars
const cardheightchanger = ()=>{
  const cards = $( '.creator-option-card' )
  $.each( cards, function ( _key, card ) { 
    if ( cardheight !== 0 ) {
      $( card ).css( 'min-height', cardheight );
    } else {
      cardheight = $( card ).css( 'height' )
    }
  } );
}

//DROPDOWN FOR CONFIG
var indropdown = false
var inuserdiv = false

$( document ).on( 'click', '.nav-config', function( e ) {
  e.preventDefault();
  window.location.href = "../html/config.html"
} )

$( document ).on( 'mouseenter',".nav-user", function( e ) {
  e.preventDefault();
  $( '.nav-dropdown' ).animate( {
    top: $( '.nav-user' ).css( 'height' ).split( 'px' )[0]*1-10,
    opacity: 1.0
  }, 500, "swing" )
  inuserdiv = true
} )

$( document ).on( 'mouseleave',".nav-user", function( e ) {
  e.preventDefault();
  setTimeout( ()=>{
    if( !indropdown ) {
      $( '.nav-dropdown' ).animate( {
        top: 0,
        opacity: 0.0
      }, 500, "swing" )
    }
  },50 )
  inuserdiv = false
} )

$( document ).on( 'mouseenter', '.nav-dropdown', function( e ) {
  e.preventDefault();
  indropdown = true;
} )

$( document ).on( 'mouseleave', '.nav-dropdown', function( e ) {
  e.preventDefault();
  indropdown = false;
  setTimeout( ()=>{
    if( !inuserdiv ) {
      $( '.nav-dropdown' ).animate( {
        top: 0,
        opacity: 0.0
      }, 500, "swing" )
    }
  },50 )
} )

// ADD ALL TEACHERS AS AN OPTION FOR THE SELECT OF CARDS
// eslint-disable-next-line no-unused-vars
const SetupCardsOptions = ( teachers, lokalen ) => { 
  return new Promise( ( res, rej )=>{
		if( !teachers || !lokalen ) rej();

		// Teachers
		$( '.creator-options-collection select[name="teacher"]' ).each( ( _i, select )=>{
			$( select ).empty().append(  `<option disabled value="reset">--Kies een leerkracht--</option>` ).val( "reset" );
			teachers.forEach( teacher =>{
				$( select ).append( `<option value="${ teacher._id }" >${ teacher.firstname } ${ teacher.lastname }</option>` )
			} )
		} )

		// Lokalen
		$( '.creator-options-collection select[name="lokaal"]' ).each( ( _i, select )=>{
			$( select ).empty().append(  `<option disabled value="reset">--Kies een lokaal--</option>` ).val( "reset" );
			// Alle lkr's toevoegen
			lokalen.forEach( lokaal =>{
				$( select ).append( `<option value="${ lokaal._id }" >${ ( lokaal.blok ?? "" ) + ( lokaal.name ?? "" ) } (${ lokaal.type ?? "standaard" })</option>` )
			} )
		} )

		res( true )
  } )
}
// eslint-disable-next-line no-unused-vars
const SetupCardOptions = ( wrapper, teachers, lokalen ) => {
  return new Promise( ( res, rej )=>{
    if ( !wrapper || !teachers || !lokalen ) rej( true );
		// Teacher section
		const teacherselect = $( wrapper ).find( 'select[name="teacher"]' );
		if( !teacherselect ) rej( true );
    $( teacherselect ).empty().val( "reset" ).append(  `<option disabled value="reset">--Kies een leerkracht--</option>` );
    // Alle lkr's toevoegen
    teachers.forEach( teacher =>{
      $( teacherselect ).append( `<option value="${ teacher._id }" >${ teacher.firstname } ${ teacher.lastname }</option>` )
    } )
		
		// lokalen section
		const lokalenselect = $( wrapper ).find( 'select[name="lokaal"]' )
		if ( !lokalenselect ) rej( true );
		$( lokalenselect ).empty().val( "reset" ).append(  `<option disabled value="reset">--Kies een lokaal--</option>` );
		// Alle lkr's toevoegen
    lokalen.map( lokaal =>{
      $( lokalenselect ).append( `<option value="${ lokaal._id }" >${ ( lokaal.blok ?? "" ) + ( lokaal.name ?? "" ) } (${ lokaal.type ?? "standaard" })</option>` )
    } )
    res( true )
  } )
}

// change can't be greater then max
$( document ).on( 'change', '.hour-card input', function( _e ) {
  if ( $( this ).val() * 1 > $( this ).attr( 'max' ) ) {
    $( this ).val( $( this ).attr( 'max' ) )
  }
  if ( $( this ).val() * 1 < $( this ).attr( 'min' ) ) {
    $( this ).val( $( this ).attr( 'min' ) )
  }
} )

// Actions on card hover
$( document ).on( 'mouseenter', '.card-hover-options', ( e ) => {
	// eslint-disable-next-line no-undef
	if( !admin || currentmode==="drag-mode" ) {
		showOptionsWrapper( e.currentTarget )
	}
} )

$( document ).on( 'mouseleave', '.card-hover-options', ( e ) => {
	// eslint-disable-next-line no-undef
	if( !admin || currentmode==="drag-mode" ) {
		hideOptionsWrapper( e.currentTarget )	
	}
} )

$( document ).on( 'mouseenter', '.card-option-move', ( e )=>{
	// eslint-disable-next-line no-undef
	makeSchemaDraggable( e.currentTarget.parentNode.parentNode )
} )

$( document ).on( 'mouseleave', '.card-option-move', ( e )=>{
	// eslint-disable-next-line no-undef
	RemoveDraggable( e.currentTarget.parentNode.parentNode )
} )

const showOptionsWrapper = ( target ) => {
	$( target ).prepend(
		`<div class="card-options-wrapper">
			<div class="card-option-remove">
				<i class="fa fa-trash-alt"></i>
			</div>
			<div class="card-option-move">
				<i class="fa fa-arrows-alt"></i>
			</div>
		</div>`
	)
	$( '.card-options-wrapper' ).css( {
		minHeight: $( '.card:not(.pauze)' ).height(),
		display: "flex",
		opacity: 0
	} )
	$( '.card-options-wrapper' ).animate( {
		opacity: 1
	}, {
		duration: 200,
		queue: false
	} )
}

const hideOptionsWrapper = ( target ) => {
	$( '.card-options-wrapper' ).animate( {
		opacity: 0
	}, {
		duration: 200,
		queue: false,
		always: ()=>{
			$( target ).find( '.card-options-wrapper' ).each( ( index,elem )=>{
				$( elem ).remove()
			} )
		}
	} )
}