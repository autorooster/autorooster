let currentCat = 'home';
let loadershown = false;
let klassen = [];
let klasnames = [];

jQuery(async function () {
	if (sessionStorage.token === undefined) {
		window.location.replace('../.');
		sessionStorage.errorType = 'redirect';
		sessionStorage.error = 'Je moet eerst inloggen';
	}
	await Ajax.get('/api/user/checktoken', { admin: true })
		.then(async _res => {
			if (_res.perms === 'user') {
				window.location.replace('../.');
				sessionStorage.errorType = 'redirect';
				sessionStorage.error = 'Je hebt hier geen toegang toe';
			}
			setupNavbar();
			await Ajax.get('/api/data/overview')
				.then(res => {
					$.each(res, function (type, amount) {
						$('.dashboard-overview')
							.find('[data-type=' + type + ']')
							.find('span')
							.html(amount);
						if (currentCat === 'home') {
							$('.dashboard-data').css({ display: 'none' });
						}
					});
				})
				.catch(() => {});
			// Check if dashboard has a category in URL
			if (getUrlParameter('cat')) {
				currentCat = getUrlParameter('cat');
				let currentselected = $('.dashboard-categories').find('.dashboard-category-selected');
				if (currentselected.length > 0) {
					$(currentselected[0]).removeClass('dashboard-category-selected');
				}
				$(`.dashboard-category[data-category="${currentCat}"]`).addClass('dashboard-category-selected');
				if (!$($('.dashboard-data-header div')[0]).hasClass('dashboard-data-remove-selected')) {
					$($('.dashboard-data-header div')[0]).html(currentCat.charAt(0).toUpperCase() + currentCat.slice(1));
				} else {
					$('.dashboard-data-remove-selected').remove();
					$($('.dashboard-data-header div')[0]).html(currentCat.charAt(0).toUpperCase() + currentCat.slice(1));
				}
				reloadData();
				$('.infoheader span').html('Home/' + currentCat.charAt(0).toUpperCase() + currentCat.slice(1));
				$('.dashboard-data').css({ display: 'block' });
			}
		})
		.catch(() => {
			window.location.replace('../.');
			sessionStorage.errorType = 'redirect';
			sessionStorage.error = 'Je token is ongeldig';
		});
});

$(document).on('click', '.nav-logout', async function (e) {
	e.preventDefault();

	if (sessionStorage.username !== undefined && sessionStorage.token !== undefined) {
		sessionStorage.removeItem('username');
		sessionStorage.removeItem('token');
		sessionStorage.logout = true;
		window.location.replace('../.');
	} else {
		Notify.info('Je bent niet ingelogd', 4000);
	}
});

$(document).on('click', '.dashboard-category', async function (e) {
	e.preventDefault();
	if (currentCat == $(this).data('category')) return;
	currentCat = $(this).data('category');
	let currentselected = $('.dashboard-categories').find('.dashboard-category-selected');
	if (currentselected.length > 0) {
		$(currentselected[0]).removeClass('dashboard-category-selected');
	}
	$(this).addClass('dashboard-category-selected');
	if (!$($('.dashboard-data-header div')[0]).hasClass('dashboard-data-remove-selected')) {
		$($('.dashboard-data-header div')[0]).html(currentCat.charAt(0).toUpperCase() + currentCat.slice(1));
	} else {
		$('.dashboard-data-remove-selected').remove();
		$($('.dashboard-data-header div')[0]).html(currentCat.charAt(0).toUpperCase() + currentCat.slice(1));
	}
	reloadData();
	$('.infoheader span').html('Home/' + currentCat.charAt(0).toUpperCase() + currentCat.slice(1));
	$('.dashboard-data').css({ display: 'block' });
	window.history.replaceState(
		{ category: currentCat },
		document.title + ' - ' + currentCat,
		window.location.origin + window.location.pathname + `?cat=${currentCat}`
	);
});

$(document).on('click', '.dashboard-btn span', async function (e) {
	e.preventDefault();
	let itemid;
	let type;
	$.each($(this.parentNode.parentNode).data(), function (i, val) {
		if (i.includes('id')) {
			type = i.slice(0, i.length - 2);
			itemid = val;
			return false;
		}
	});
	switch (type) {
		case 'leerling':
			await Ajax.get('/api/data/modal/leerling', { dataid: itemid })
				.then(res => {
					let info = res[0]; //alle info
					klassen = res[1];
					klassen.forEach(klas => {
						klasnames.push(klas.name);
					});
					Modal.New(
						'.modal-leerlingen-data',
						'Leerling bewerken',
						{
							input: {
								label: {
									type: 'before',
									text: 'Voornaam',
								},
								name: 'firstname',
								value: `${info.firstname}`,
								type: 'text',
							},
							input2: {
								label: {
									type: 'before',
									text: 'Achternaam',
								},
								name: 'lastname',
								value: `${info.lastname}`,
								type: 'text',
							},
							select1: {
								text: 'Klassen',
								name: 'klassen',
								options: klasnames,
							},
						},
						{
							button1: {
								text: 'Cancel',
								class: 'modal-leerlingen-data-cancel',
							},
							button2: {
								text: 'Save',
								class: 'modal-leerlingen-data-accept',
							},
						}
					);
					Modal.Open('.modal-leerlingen-data');
					$('.modal-leerlingen-data').data('llrid', itemid);
				})
				.catch(() => {});
			break;

		case 'teacher':
			await Ajax.get('/api/data/modal/teacher', { dataid: itemid })
				.then(res => {
					let info = res[0][0]; //alle info
					klassen = res[1];
					klassen.forEach(klas => {
						klasnames.push(klas.name);
					});
					Modal.New(
						'.modal-leerkrachten-data',
						'Leerkracht bewerken',
						{
							input: {
								label: {
									type: 'before',
									text: 'Voornaam',
								},
								name: 'firstname',
								value: `${info.firstname}`,
								type: 'text',
							},
							input2: {
								label: {
									type: 'before',
									text: 'Achternaam',
								},
								name: 'lastname',
								value: `${info.lastname}`,
								type: 'text',
							},
							select1: {
								text: 'Geslacht',
								name: 'geslacht',
								options: ['Man', 'Vrouw', 'Andere'],
								default: info.sex,
							},
							dropdown1: {
								data: {
									droptype: 'klassen',
								},
								options: klasnames,
							},
							dropdown2: {
								data: {
									droptype: 'days',
								},
								options: ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'],
							},
						},
						{
							button1: {
								text: 'Cancel',
								class: 'modal-leerkrachten-data-cancel',
							},
							button2: {
								text: 'Save',
								class: 'modal-leerkrachten-data-accept',
							},
						}
					);
					Modal.Open('.modal-leerkrachten-data');
					$('.modal-leerkrachten-data').data('lkrid', itemid);
				})
				.catch(() => {});
			break;

		case 'klas':
			await Ajax.get('/api/data/modal/klas', { dataid: itemid })
				.then(res => {
					let info = res[0]; //alle info
					let leerkrachten = res[1];
					let lkrnames = [];
					leerkrachten.forEach(lkr => {
						lkrnames.push(lkr.firstname + ' ' + lkr.lastname);
					});
					Modal.New(
						'.modal-klas-data',
						'Klas bewerken',
						{
							input1: {
								label: {
									type: 'before',
									text: 'klasnaam',
								},
								name: 'name',
								value: info.name,
								type: 'text',
								data: {
									inputinfo: 'name',
								},
							},
							select1: {
								text: 'Leekrachten',
								name: 'teachers',
								options: lkrnames,
							},
							button3: {
								text: 'Lessen',
								class: 'modal-klas-data-klassen',
							},
							button4: {
								text: 'Verwerk verzoeken',
								class: 'modal-klas-data-diffs',
							},
						},
						{
							button1: {
								text: 'Cancel',
								class: 'modal-klas-data-cancel',
							},
							button2: {
								text: 'Save',
								class: 'modal-klas-data-accept',
							},
						}
					);
					Modal.Open('.modal-klas-data');
					$('.modal-klas-data').data('klasid', itemid);
				})
				.catch(() => {});
			break;

		case 'lokaal':
			await Ajax.get('/api/data/modal/lokaal', { dataid: itemid })
				.then(res => {
					let info = res; //alle info
					Modal.New(
						'.modal-lokaal-data',
						'Lokaal bewerken',
						{
							input1: {
								label: {
									type: 'before',
									text: 'Lokaalnummer',
								},
								name: 'name',
								value: info.name,
								type: 'text',
								data: {
									inputinfo: 'name',
								},
							},
							input2: {
								label: {
									type: 'before',
									text: 'Blok',
								},
								name: 'blok',
								value: info.blok,
								type: 'text',
								data: {
									inputinfo: 'blok',
								},
							},
							select: {
								text: 'Lokaaltype',
								name: 'loktype',
								options: ['standaard', 'wetenschap', 'praktijk'],
							},
						},
						{
							button1: {
								text: 'Cancel',
								class: 'modal-lokaal-data-cancel',
							},
							button2: {
								text: 'Save',
								class: 'modal-lokaal-data-accept',
							},
						}
					);
					Modal.Open('.modal-lokaal-data');
					$('.modal-lokaal-data').data('lokaalid', itemid);
				})
				.catch(() => {});
			break;

		default:
			Notify.error('Er is iets misgegaan');
			break;
	}
});

$(document).on('click', '.modal-leerlingen-data-cancel', function (e) {
	e.preventDefault();
	Modal.Close('.modal-leerlingen-data').finally(() => {
		Modal.Delete('.modal-leerlingen-data');
	});
});

$(document).on('click', '.modal-leerlingen-data-accept', async function (e) {
	e.preventDefault();

	let itemid;
	$.each($(this.parentNode.parentNode.parentNode).data(), function (i, val) {
		if (i.includes('id')) {
			itemid = val;
			return false;
		}
	});

	let info = {
		iid: itemid,
	};

	$($($(this.parentNode.parentNode.parentNode).find('.modal-main')).find('input')).each(function (index, element) {
		info[element.name] = element.value;
	});

	$($($(this.parentNode.parentNode.parentNode).find('.modal-main')).find('select')).each(function (index, element) {
		if (element.name === 'klassen') {
			let klas = klassen.find(obj => obj.name == element.value);
			if (klas === undefined) {
				klas = klassen.find(obj => obj.name == element.value.toUpperCase());
				if (klas === undefined) {
					klas = klassen.find(obj => obj.name == element.value.toLowerCase());
				}
			}
			info[element.name] = klas._id;
		} else {
			info[element.name] = element.value;
		}
	});
	await Ajax.post('/api/data/update/leerling', { llrinfo: info })
		.then(_res => {
			Modal.Close('.modal-leerlingen-data').finally(() => {
				Modal.Delete('.modal-leerlingen-data');
			});
			ListStudents();
		})
		.catch(res => {
			Notify.error(res.responseText);
		});
});

$(document).on('click', '.modal-leerkrachten-data-cancel', function (e) {
	e.preventDefault();
	Modal.Close('.modal-leerkrachten-data').finally(() => {
		Modal.Delete('.modal-leerkrachten-data');
	});
});

$(document).on('click', '.modal-leerkrachten-data-accept', async function (e) {
	e.preventDefault();

	let info = {
		klassen: [],
		days: [],
		sex: 0,
	};

	//console.log($(this.parentNode.parentNode.parentNode).find('select[name="geslacht"]')[0].options)
	$.each($(this.parentNode.parentNode.parentNode).find('select[name="geslacht"]')[0].options, function (i, val) {
		if (val.value === $(this.parentNode.parentNode.parentNode).find('select[name="geslacht"]')[0].value) {
			info.sex = i;
			return false;
		}
	});

	$.each($(this.parentNode.parentNode.parentNode).data(), function (i, val) {
		if (i.includes('id')) {
			info.iid = val;
			return false;
		}
	});

	$($($(this.parentNode.parentNode.parentNode).find('.modal-main')).find('input')).each(function (index, element) {
		if (element.type !== 'checkbox') {
			if (element.name !== '') {
				info[element.name] = element.value;
			}
		}
	});

	$.each($('.dropdown-label'), function (index, element) {
		if (element.childNodes[0] === undefined) return false;
		let datatype = $(element.parentNode.parentNode).data('droptype');
		info[datatype] = [];
		$.each(element.childNodes, function (i, elem) {
			info[datatype].push($(elem).html().split('<span>')[0]);
		});
	});

	await Ajax.post('/api/data/update/leerkracht', { lkrinfo: info })
		.then(_res => {
			Modal.Close('.modal-leerkrachten-data').finally(() => {
				Modal.Delete('.modal-leerkrachten-data');
			});
			ListTeachers();
		})
		.catch(res => {
			Notify.error(res.responseText);
		});
});

$(document).on('click', '.modal-klas-data-cancel', function (e) {
	e.preventDefault();
	Modal.Close('.modal-klas-data').finally(() => {
		Modal.Delete('.modal-klas-data');
	});
});

$(document).on('click', '.modal-klas-data-accept', async function (e) {
	e.preventDefault();

	let info = {
		klassen: [],
		days: [],
		sex: 0,
	};

	//console.log($(this.parentNode.parentNode.parentNode).find('select[name="geslacht"]')[0].options)
	$.each($(this.parentNode.parentNode.parentNode).find('select[name="teachers"]')[0].options, function (i, val) {
		if (val.value === $(this.parentNode.parentNode.parentNode).find('select[name="teachers"]')[0].value) {
			info.teacher = i + 1;
			return false;
		}
	});

	$.each($(this.parentNode.parentNode.parentNode).data(), function (i, val) {
		if (i.includes('id')) {
			info.iid = val;
			return false;
		}
	});

	$($($(this.parentNode.parentNode.parentNode).find('.modal-main')).find('input')).each(function (index, element) {
		if (element.type !== 'checkbox') {
			if (element.name !== '') {
				info[element.name] = element.value;
			}
		}
	});

	await Ajax.post('/api/data/update/klassen', { klasinfo: info })
		.then(_res => {
			Modal.Close('.modal-klas-data').finally(() => {
				Modal.Delete('.modal-klas-data');
			});
			ListClasses();
		})
		.catch(res => {
			Notify.error(res.responseText);
		});
});

$(document).on('click', '.modal-lokaal-data-cancel', function (e) {
	e.preventDefault();
	Modal.Close('.modal-lokaal-data').finally(() => {
		Modal.Delete('.modal-lokaal-data');
	});
});

$(document).on('click', '.modal-lokaal-data-accept', async function (e) {
	e.preventDefault();

	let info = {};

	$.each($(this.parentNode.parentNode.parentNode).data(), async function (i, val) {
		if (i.includes('id')) {
			info.iid = val;
			return false;
		}
	});

	$.each($(this.parentNode.parentNode.parentNode).find('select[name="loktype"]')[0].options, function (i, val) {
		if (val.value === $(this.parentNode.parentNode.parentNode).find('select[name="loktype"]')[0].value) {
			info.loktype = val.value;
			return false;
		}
	});

	$($($(this.parentNode.parentNode.parentNode).find('.modal-main')).find('input')).each(function (index, element) {
		if (element.type !== 'checkbox') {
			if (element.name !== '') {
				info[element.name] = element.value;
			}
		}
	});

	await Ajax.post('/api/data/update/lokaal', { lokinfo: info })
		.then(_res => {
			Modal.Close('.modal-lokaal-data').finally(() => {
				Modal.Delete('.modal-lokaal-data');
			});
			ListLokalen();
		})
		.catch(res => {
			Notify.error(res.responseText);
		});
});

//data toevoegen
$(document).on('click', '.dashboard-data-add', async function (e) {
	e.preventDefault();
	let type = $(this.parentNode)
		.find('div:not(.dashboard-data-remove-selected):not(.dashboard-data-add)')
		.html()
		.toLowerCase();
	switch (type) {
		case 'leerlingen':
			await Ajax.get('/api/data/info/klassen')
				.then(res => {
					res.forEach(klas => {
						klasnames.push(klas.name);
					});
					Modal.New(
						'.modal-leerlingen-add',
						'Leerling toevoegen',
						{
							input: {
								label: {
									type: 'before',
									text: 'Voornaam',
								},
								name: 'firstname',
								type: 'text',
							},
							input2: {
								label: {
									type: 'before',
									text: 'Achternaam',
								},
								name: 'lastname',
								type: 'text',
							},
							select1: {
								text: 'Klassen',
								name: 'klassen',
								options: klasnames,
							},
						},
						{
							button1: {
								text: 'Cancel',
								class: 'modal-leerlingen-add-cancel',
							},
							button2: {
								text: 'Add',
								class: 'modal-leerlingen-add-accept',
							},
						}
					);
					Modal.Open('.modal-leerlingen-add');
				})
				.catch(res => {
					Notify.error(res.responseText);
				});
			break;

		case 'leerkrachten':
			await Ajax.get('/api/data/info/klassen')
				.then(res => {
					res.forEach(klas => {
						klasnames.push(klas.name);
					});
					Modal.New(
						'.modal-leerkrachten-add',
						'Leerkracht toevoegen',
						{
							input: {
								label: {
									type: 'before',
									text: 'Voornaam',
								},
								name: 'firstname',
								type: 'text',
							},
							input2: {
								label: {
									type: 'before',
									text: 'Achternaam',
								},
								name: 'lastname',
								type: 'text',
							},
							select1: {
								text: 'Geslacht',
								name: 'geslacht',
								options: ['Man', 'Vrouw', 'Andere'],
							},
							dropdown1: {
								data: {
									droptype: 'klassen',
								},
								options: klasnames,
							},
							dropdown2: {
								data: {
									droptype: 'days',
								},
								options: ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'],
							},
						},
						{
							button1: {
								text: 'Cancel',
								class: 'modal-leerkrachten-add-cancel',
							},
							button2: {
								text: 'Save',
								class: 'modal-leerkrachten-add-accept',
							},
						}
					);
					Modal.Open('.modal-leerkrachten-add');
				})
				.catch(() => {});
			break;
		case 'klassen':
			await Ajax.get('/api/data/info/leerkrachten')
				.then(res => {
					let lkrnames = {};
					res.forEach(lkr => {
						lkrnames[lkr._id] = `${lkr.firstname} ${lkr.lastname}`;
					});
					Modal.New(
						'.modal-klas-add',
						'Klas toevoegen',
						{
							input1: {
								label: {
									type: 'before',
									text: 'Klasnaam',
								},
								name: 'name',
								type: 'text',
								data: {
									inputinfo: 'name',
								},
							},
							select1: {
								text: 'Klastitularis',
								name: 'teacher',
								options: lkrnames,
							},
						},
						{
							button1: {
								text: 'Cancel',
								class: 'modal-klas-add-cancel',
							},
							button2: {
								text: 'Save',
								class: 'modal-klas-add-accept',
							},
						}
					);
					Modal.Open('.modal-klas-add');
				})
				.catch(() => {});
			break;
		case 'lokalen':
			Modal.New(
				'.modal-lokaal-add',
				'Lokaal bewerken',
				{
					input1: {
						label: {
							type: 'before',
							text: 'Lokaalnummer',
						},
						name: 'name',
						type: 'text',
						data: {
							inputinfo: 'name',
						},
					},
					input2: {
						label: {
							type: 'before',
							text: 'Blok',
						},
						name: 'blok',
						type: 'text',
						data: {
							inputinfo: 'blok',
						},
					},
					select: {
						text: 'Lokaaltype',
						name: 'loktype',
						options: ['standaard', 'wetenschap', 'praktijk'],
					},
				},
				{
					button1: {
						text: 'Cancel',
						class: 'modal-lokaal-add-cancel',
					},
					button2: {
						text: 'Save',
						class: 'modal-lokaal-add-accept',
					},
				}
			);
			Modal.Open('.modal-lokaal-add');
			break;
		default:
			Notify.error('Er is iets misgegaan');
			break;
	}
});

$(document).on('click', '.modal-leerlingen-add-cancel', function (e) {
	e.preventDefault();
	Modal.Close('.modal-leerlingen-add').finally(() => {
		Modal.Delete('.modal-leerlingen-add');
	});
});

$(document).on('click', '.modal-leerlingen-add-accept', async function (e) {
	e.preventDefault();

	let info = {};
	let check = true;

	$.each($(this.parentNode.parentNode.parentNode).find('select[name="klassen"]')[0].options, function (i, val) {
		if (val.value === $(this.parentNode.parentNode.parentNode).find('select[name="klassen"]')[0].value) {
			info.klas = klasnames[i];
			return false;
		}
	});

	$($($(this.parentNode.parentNode.parentNode).find('.modal-main')).find('input')).each(function (index, element) {
		if (element.type !== 'checkbox') {
			if (element.name !== '') {
				if (element.value.trim() == '') {
					$(this.parentNode.parentNode.parentNode)
						.find('input[name=' + element.name + ']')
						.addClass('input-empty');
					$(this.parentNode.parentNode.parentNode)
						.find('input[name=' + element.name + ']')
						.css('border', '2px solid red');
					check = false;
				} else {
					//reset input styling
					if (
						$(this.parentNode.parentNode.parentNode)
							.find('input[name=' + element.name + ']')
							.hasClass('input-empty')
					) {
						$(this.parentNode.parentNode.parentNode)
							.find('input[name=' + element.name + ']')
							.removeClass('input-empty');
					}
					$(this.parentNode.parentNode.parentNode)
						.find('input[name=' + element.name + ']')
						.css('border', '0.1vh solid #2c3e50');
					//add data to table
					info[element.name] = element.value;
				}
			}
		}
	});
	if (check) {
		await Ajax.post('/api/data/add/leerling', { llrinfo: info })
			.then(_res => {
				Modal.Close('.modal-leerlingen-add').finally(() => {
					Modal.Delete('.modal-leerlingen-add');
				});
				Notify.success(`${info.firstname} ${info.lastname} is succesvol toegevoegd`);
				ListStudents();
			})
			.catch(() => {});
	} else {
		Notify.error('Niet alle velden zijn ingevuld');
	}
});

$(document).on('click', '.modal-leerkrachten-add-cancel', function (e) {
	e.preventDefault();
	Modal.Close('.modal-leerkrachten-data').finally(() => {
		Modal.Delete('.modal-leerkrachten-data');
	});
});

$(document).on('click', '.modal-leerkrachten-add-accept', async function (e) {
	e.preventDefault();

	let info = {
		klassen: [],
		days: [],
		sex: 0,
	};
	let check = true;

	$.each($(this.parentNode.parentNode.parentNode).find('select[name="geslacht"]')[0].options, function (i, val) {
		if (val.value === $(this.parentNode.parentNode.parentNode).find('select[name="geslacht"]')[0].value) {
			info.sex = i;
			return false;
		}
	});

	$.each($(this.parentNode.parentNode.parentNode).data(), function (i, val) {
		if (i.includes('id')) {
			info.iid = val;
			return false;
		}
	});

	$.each($('.dropdown-label'), function (index, element) {
		if (element.childNodes[0] === undefined) return false;
		let datatype = $(element.parentNode.parentNode).data('droptype');
		info[datatype] = [];
		$.each(element.childNodes, function (i, elem) {
			info[datatype].push($(elem).html().split('<span>')[0]);
		});
	});

	$($($(this.parentNode.parentNode.parentNode).find('.modal-main')).find('input')).each(function (index, element) {
		if (element.type !== 'checkbox') {
			if (element.name !== '') {
				if (element.value.trim() == '') {
					$(this.parentNode.parentNode.parentNode)
						.find('input[name=' + element.name + ']')
						.addClass('input-empty');
					$(this.parentNode.parentNode.parentNode)
						.find('input[name=' + element.name + ']')
						.css('border', '2px solid red');
					check = false;
				} else {
					//reset input styling
					if (
						$(this.parentNode.parentNode.parentNode)
							.find('input[name=' + element.name + ']')
							.hasClass('input-empty')
					) {
						$(this.parentNode.parentNode.parentNode)
							.find('input[name=' + element.name + ']')
							.removeClass('input-empty');
					}
					$(this.parentNode.parentNode.parentNode)
						.find('input[name=' + element.name + ']')
						.css('border', '0.1vh solid #2c3e50');
					//add data to table
					info[element.name] = element.value;
				}
			}
		}
	});

	if (check) {
		await Ajax.post('/api/data/add/leerkracht', { lkrinfo: info })
			.then(_res => {
				Modal.Close('.modal-leerkrachten-add').finally(() => {
					Modal.Delete('.modal-leerkrachten-add');
				});
				ListTeachers();
			})
			.catch(() => {});
	} else {
		Notify.error('Niet alle velden zijn ingevuld');
	}
});

$(document).on('click', '.modal-klas-add-cancel', function (e) {
	e.preventDefault();
	Modal.Close('.modal-klas-add').finally(() => {
		Modal.Delete('.modal-klas-add');
	});
});

$(document).on('click', '.modal-klas-add-accept', async function (e) {
	e.preventDefault();
	let data = $('.modal-klas-add').serializeArray();

	// Check if fields are empty
	let check = true;
	$.each(data, function (index, input) {
		if (typeof input.value === 'string') {
			if (input.value.trim() == '') {
				$('.modal-klas-add')
					.find('input[name=' + input.name + ']')
					.addClass('input-empty');
				$('.modal-klas-add')
					.find('input[name=' + input.name + ']')
					.css('border', '2px solid red');
				check = false;
			} else {
				if (
					$('.modal-klas-add')
						.find('input[name=' + input.name + ']')
						.hasClass('input-empty')
				) {
					$('.modal-klas-add')
						.find('input[name=' + input.name + ']')
						.removeClass('input-empty');
				}
				$('.modal-klas-add')
					.find('input[name=' + input.name + ']')
					.css('border', '0.1vh solid #2c3e50');
			}
		}
	});

	// Adding data to info Object
	let info = {};
	$.each(data, function (index, elem) {
		info[elem.name] = elem.value;
	});

	if (check) {
		await Ajax.post('/api/data/add/klas', { klasinfo: info })
			.then(_res => {
				Modal.Close('.modal-klas-add').finally(() => {
					Modal.Delete('.modal-klas-add');
				});
				ListClasses();
			})
			.catch(() => {});
	} else {
		Notify.error('Niet alle velden zijn ingevuld');
	}
});

$(document).on('click', '.modal-lokaal-add-cancel', function (e) {
	e.preventDefault();
	Modal.Close('.modal-lokaal-add').finally(() => {
		Modal.Delete('.modal-lokaal-add');
	});
});

$(document).on('click', '.modal-lokaal-add-accept', async function (e) {
	e.preventDefault();
	let data = $('.modal-lokaal-add').serializeArray();
	let check = true;
	$.each(data, function (index, input) {
		if (typeof input.value === 'string') {
			if (input.value.trim() == '') {
				$('.modal-lokaal-add')
					.find('input[name=' + input.name + ']')
					.addClass('input-empty');
				$('.modal-lokaal-add')
					.find('input[name=' + input.name + ']')
					.css('border', '2px solid red');
				check = false;
			} else {
				if (
					$('.modal-lokaal-add')
						.find('input[name=' + input.name + ']')
						.hasClass('input-empty')
				) {
					$('.modal-lokaal-add')
						.find('input[name=' + input.name + ']')
						.removeClass('input-empty');
				}
				$('.modal-lokaal-add')
					.find('input[name=' + input.name + ']')
					.css('border', '0.1vh solid #2c3e50');
			}
		}
	});
	let info = {};
	$.each(data, function (index, elem) {
		info[elem.name] = elem.value;
	});
	console.log(info);
	if (check) {
		await Ajax.post('/api/data/add/lokaal')
			.then(_res => {
				Modal.Close('.modal-lokaal-add').finally(() => {
					Modal.Delete('.modal-lokaal-add');
				});
				ListLokalen();
			})
			.catch(() => {});
	} else {
		Notify.error('Niet alle velden zijn ingevuld');
	}
});

$(document).on('click', '.modal-klas-data-klassen', async function (_e) {
	//redirect to overall lessen page. eg: localhost/lessen?klas=6iw
	let iid = 0;
	$.each($(this.parentNode.parentNode.parentNode).data(), function (i, val) {
		if (i.includes('id')) {
			iid = val;
			return false;
		}
	});
	//api call to get class
	await Ajax.get('/api/data/info/klas', { iid: iid })
		.then(res => {
			window.location.href = `/html/schemacreator.html?klas=${res.name}&admin=true`;
		})
		.catch(() => {});
});

$(document).on('click', '.modal-klas-data-diffs', async e => {
	let iid = 0;
	$.each($(e.currentTarget.parentNode.parentNode.parentNode).data(), function (i, val) {
		if (i.includes('id')) {
			iid = val;
			return false;
		}
	});
	await Ajax.get('/api/data/info/klas', { iid: iid })
		.then(res => {
			Ajax.post(`/api/data/conversion/seperate/${res.name}`);
		})
		.catch(() => {});
});

$('.dashboard-data-download').on('click', e => {
	if (currentCat !== 'leerlingen') return;
	Ajax.get('/api/data/download/leerlingen').then(res => {});
});
$('.dashboard-data-diff').on('click', e => {
	if (currentCat !== 'klassen') return;
	Ajax.post('/api/data/conversion/all').then(res => {});
});

$('.dashboard-data-item').on('change', '.dashboard-data-item-checkbox', e => {
	e.preventDefault();
	let elem = `<button class='dashboard-data-remove-selected' type="submit">Delete</button>`;
	let retval = false;
	if (!$($('.dashboard-data-header button')[0]).hasClass('dashboard-data-remove-selected')) {
		$('.dashboard-data-header').prepend(elem);
		$('.dashboard-data-remove-selected').css(
			'left',
			$('.dashboard-categories').css('width').split('px')[0] * 1 + 40 + 'px'
		);
	}
	$('.dashboard-data-item-checkbox').each((i, val) => {
		if ($(val).prop('checked')) {
			retval = true;
		}
	});
	if (!retval) {
		$('.dashboard-data-remove-selected').remove();
	}
});

$(document).on('submit', '.dashboard-data', function (e) {
	e.preventDefault();
	if (currentCat === 'home') return;
	//all checked inputs
	const toBeDeleted = [];
	$('.dashboard-data-item-checkbox:checked').each((i, checkbox) => {
		toBeDeleted.push(Object.values(checkbox.parentNode.dataset)[0]);
	});
	Ajax.post(`/api/data/remove/${currentCat}`, { ids: toBeDeleted })
		.then(_res => {
			reloadData();
			Notify.success('Items succesvol verwijderd');
		})
		.catch(() => {});
});

$(window).on('resize', () => {
	if ($('.dashboard-data-remove-selected') !== undefined) {
		$('.dashboard-data-remove-selected').css(
			'left',
			$('.dashboard-categories').css('width').split('px')[0] * 1 + 40 + 'px'
		);
	}
});

const Loader = function (toggle) {
	if (toggle === undefined) {
		if (loadershown === true) {
			$('.loading-bar-con').fadeOut(500);
			loadershown = false;
		} else {
			$('.loading-bar-con').fadeIn(500);
			loadershown = true;
		}
	} else {
		if (toggle) {
			$('.loading-bar-con').fadeIn(500);
			loadershown = true;
		} else if (!toggle) {
			$('.loading-bar-con').fadeOut(500);
			loadershown = false;
		}
	}
};

const getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
		}
	}
};

const setupNavbar = function () {
	$('.nav-list').html(
		`<a class="nav nav-dashboard nav-left nav-selected">Dashboard</a>
      <a class="nav nav-center nav-selected infoheader"><span>Home</span></a>
      <a class="nav nav-right nav-logout text-hover text-hover-both-go-up"><i class="fas fa-sign-out-alt"></i> Log-out</a>
      <a class="nav nav-right nav-user">WELCOME ${sessionStorage.username.toUpperCase()} </a>
      <div class="nav nav-dropdown"><a class="nav nav-config"><i class="fa fa-cog"></i>CONFIG</a></div>
			<div class="nav-darkmode-toggle nav-right">
				<span>☀️</span>
				<input type="checkbox" id="toggle-switch" />
				<label for="toggle-switch"><span class="screen-reader-text">Toggle Color Scheme</span></label>
				<span>🌙</span>
			</div>`
	);
	$('.nav-dropdown').css({
		top: 0,
		right: $('.nav-logout').css('width').split('px')[0] * 1 + 30,
		'min-width': $('.nav-user').css('width').split('px')[0] * 1 - 20,
		display: 'block',
		opacity: 0.0,
	});
};

const ListStudents = async () => {
	Loader(true);
	await Ajax.get('/api/data/leerlingen')
		.then(res => {
			$('.dashboard-data-item').html('');
			res.forEach(llr => {
				let elem;
				if (!llr.schemaId) {
					elem = `<div class="dashboard-data-item-con" data-leerlingid=${
						llr._id
					}><input type="checkbox" class="dashboard-data-item-checkbox" /><div class="dashboard-data-item-text">${
						llr.firstname
					} ${
						llr.lastname
					}</div><div class="dashboard-data-item-text">Schema ingediend: <i class="fas fa-times"></i><span></span></div><div class="dashboard-data-item-text">Klas: ${
						llr.klas ? llr.klas : 'Geen'
					}</div><div class="dashboard-data-item-text dashboard-btn"><span class="text-hover text-hover-both-go-up" style="padding-top:0;padding-bottom:0;">Edit</span></div></div>`;
				} else {
					elem = `<div class="dashboard-data-item-con" data-leerlingid=${
						llr._id
					}><input type="checkbox" class="dashboard-data-item-checkbox" /><div class="dashboard-data-item-text">${
						llr.firstname
					} ${
						llr.lastname
					}</div><div class="dashboard-data-item-text">Schema ingediend: <i class="fas fa-check"></i><span></span></div><div class="dashboard-data-item-text">Klas: ${
						llr.klas ? llr.klas : 'Geen'
					}</div><div class="dashboard-data-item-text dashboard-btn"><span class="text-hover text-hover-both-go-up" style="padding-top:0;padding-bottom:0;">Edit</span></div></div>`;
				}
				$('.dashboard-data-item').append(elem);
			});
			Loader(false);
		})
		.catch(res => {
			Notify.error(res.responseText);
		});
};

const ListTeachers = async () => {
	Loader(true);
	await Ajax.get('/api/data/leerkrachten')
		.then(res => {
			$('.dashboard-data-item').html('');
			res.forEach(lkr => {
				let elem;
				elem = `<div class="dashboard-data-item-con" data-teacherid=${lkr._id}><input type="checkbox" class="dashboard-data-item-checkbox" /><div class="dashboard-data-item-text">${lkr.firstname} ${lkr.lastname}</div><div class="dashboard-data-item-text dashboard-data-item-classes"></div><div class="dashboard-data-item-text dashboard-data-item-days"></div><div class="dashboard-data-item-text dashboard-btn"><span class="text-hover text-hover-both-go-up" style="padding-top:0;padding-bottom:0;">Edit</span></div></div>`;
				$('.dashboard-data-item').append(elem);
				for (let index = 0; index < lkr.classes.length; index++) {
					const element = lkr.classes[index];

					if (index === 0) {
						$('[data-teacherid=' + lkr._id + ']')
							.find('.dashboard-data-item-classes')
							.append(element);
					} else {
						$('[data-teacherid=' + lkr._id + ']')
							.find('.dashboard-data-item-classes')
							.append(', ' + element);
					}
				}
				for (let index = 0; index < lkr.days.length; index++) {
					const element = lkr.days[index];
					if (index === 0) {
						$('[data-teacherid=' + lkr._id + ']')
							.find('.dashboard-data-item-days')
							.append(element);
					} else {
						$('[data-teacherid=' + lkr._id + ']')
							.find('.dashboard-data-item-days')
							.append(', ' + element);
					}
				}
			});
			Loader(false);
		})
		.catch(() => {});
};

const ListClasses = async () => {
	Loader(true);
	await Ajax.get('/api/data/klassen')
		.then(res => {
			$('.dashboard-data-item').html('');
			res[0].forEach(klas => {
				let elem;
				elem = `<div class="dashboard-data-item-con" data-klasid=${
					klas._id
				}><input type="checkbox" class="dashboard-data-item-checkbox" /><div class="dashboard-data-item-text">${
					klas.name
				}</div><div class="dashboard-data-item-text">${
					(res[1][klas.name] !== undefined &&
						((res[1][klas.name] === 1 && res[1][klas.name] + ' leerling') || res[1][klas.name] + ' leerlingen')) ||
					'0 leerlingen'
				} </div><div class="dashboard-data-item-text">Klastitularis: ${klas.firstname} ${
					klas.lastname
				}</div><div class="dashboard-data-item-text dashboard-btn"><span class="text-hover text-hover-both-go-up" style="padding-top:0;padding-bottom:0;">Edit</span></div></div>`;
				$('.dashboard-data-item').append(elem);
			});
			Loader(false);
		})
		.catch(() => {});
};

const ListLokalen = async () => {
	Loader(true);
	await Ajax.get('/api/data/lokalen')
		.then(res => {
			$('.dashboard-data-item').html('');
			res.forEach(lokaal => {
				let elem;
				elem = `<div class="dashboard-data-item-con " data-lokaalid=${lokaal._id}><input type="checkbox" class="dashboard-data-item-checkbox" /><div class="dashboard-data-item-text">${lokaal.blok}${lokaal.name}</div><div class="dashboard-data-item-text">type: ${lokaal.type}</div><div class="dashboard-data-item-text"></div><div class="dashboard-data-item-text dashboard-btn"><span class="text-hover text-hover-both-go-up" style="padding-top:0;padding-bottom:0;">Edit</span></div></div>`;
				$('.dashboard-data-item').append(elem);
			});
			Loader(false);
		})
		.catch(() => {});
};

const reloadData = () => {
	switch (currentCat) {
		case 'leerlingen':
			$('.dashboard-data-diff')
				.fadeOut()
				.promise()
				.done(() => {
					$('.dashboard-data-download').fadeIn();
				});
			ListStudents();
			break;
		case 'leerkrachten':
			$('.dashboard-data-download').fadeOut();
			$('.dashboard-data-diff').fadeOut();
			ListTeachers();
			break;
		case 'klassen':
			$('.dashboard-data-download')
				.fadeOut()
				.promise()
				.done(() => {
					$('.dashboard-data-diff').fadeIn();
				});
			ListClasses();
			break;
		case 'lokalen':
			$('.dashboard-data-download').fadeOut();
			$('.dashboard-data-diff').fadeOut();
			ListLokalen();
			break;
		default:
			Notify.error('Something went wrong');
			break;
	}
};

//DROPDOWN FOR CONFIG
var indropdown = false;
var inuserdiv = false;

$(document).on('click', '.nav-config', function (e) {
	e.preventDefault();
	window.location.href = '/html/config.html';
});

$(document).on('mouseenter', '.nav-user', function (e) {
	e.preventDefault();
	$('.nav-dropdown').animate(
		{
			top: $('.nav-user').css('height').split('px')[0] * 1 - 10,
			opacity: 1.0,
		},
		500,
		'swing'
	);
	inuserdiv = true;
});

$(document).on('mouseleave', '.nav-user', function (e) {
	e.preventDefault();
	setTimeout(() => {
		if (!indropdown) {
			$('.nav-dropdown').animate(
				{
					top: 0,
					opacity: 0.0,
				},
				500,
				'swing'
			);
		}
	}, 50);
	inuserdiv = false;
});

$(document).on('mouseenter', '.nav-dropdown', function (e) {
	e.preventDefault();
	indropdown = true;
});

$(document).on('mouseleave', '.nav-dropdown', function (e) {
	e.preventDefault();
	indropdown = false;
	setTimeout(() => {
		if (!inuserdiv) {
			$('.nav-dropdown').animate(
				{
					top: 0,
					opacity: 0.0,
				},
				500,
				'swing'
			);
		}
	}, 50);
});
