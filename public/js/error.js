let klas = "";
let errtype = "";
let currentselect = null;
let errorLoop = false;

// eslint-disable-next-line no-undef
addLoadEvent( async () => {
  klas = getUrlParameter( "klas" );
  errtype = getUrlParameter( "errtype" );

  if ( !klas || !errtype ) {
    window.history.back();
    sessionStorage.errorType = "redirect";
    sessionStorage.error = "Je URL is niet correct probeer opnieuw!";
  }

  // Prevent closing tab without saving
  window.addEventListener( "beforeunload", ( e ) => {
    const inputs = $( ".error-container" ).serializeArray();
    if ( inputs[0] ) {
      e.preventDefault();
      e.returnValue = "";
      return;
    }
  } );

  Ajax.get( "/api/user/checktoken", {admin:true} )
    .then( async ( _res ) => {
      SetupNavBar();
      Ajax.get( "/api/creator/getimporterrors", {
        errtype: errtype
      } )
        .then( async ( res ) => {
          if ( res.redirect ) {
            window.history.back();
            sessionStorage.errorType = "redirect";
            sessionStorage.error = "Er zijn geen error's gevonden!";
          }
          if ( res.errors ) {
            await SetupErrorCon( res.errors, res.existing );
            $( ".spinner-con" ).animate( { opacity: 0 }, 750 );
            $( ".spinner-con" ).css( {
              display: "none",
              pointerEvents: "none"
            } );
          } else {
            window.history.back();
            sessionStorage.errorType = "redirect";
            sessionStorage.error = "Er zijn geen error's gevonden!";
          }
        } )
        .catch( ( res ) => {
          if ( res.redirect ) {
            window.history.back();
            sessionStorage.errorType = "redirect";
            sessionStorage.error =
              res.message || "Er zijn geen error's gevonden!";
          }
        } );
    } )
    .catch( () => {
      window.location.replace( "../." );
      sessionStorage.errorType = "redirect";
      sessionStorage.error = "Je moet opnieuw inloggen!";
    } );
} );

$( document ).on( "change", ".error-item select", ( e ) => {
  if ( e.currentTarget.value === "new" ) {
    openModal( e.currentTarget.id );
    currentselect = e.currentTarget;
  }
  $( ".savebar" ).css( { display: "flex" } );
} );

$( document ).on( "click", ".modal-leerkrachten-add-cancel", ( _e ) => {
  Modal.Close( ".modal-leerkrachten-add" ).finally( () => {
    Modal.Delete( ".modal-leerkrachten-add" );
  } );
  currentselect = null;
} );

$( document ).on( "click", ".modal-leerkrachten-add-accept", ( e ) => {
  $( e.currentTarget.parentNode.parentNode.parentNode ).submit();
} );

$( document ).on( "click", ".modal-klas-add-cancel", ( _e ) => {
  Modal.Close( ".modal-klas-add" ).finally( () => {
    Modal.Delete( ".modal-klas-add" );
  } );
  currentselect = null;
} );

$( document ).on( "click", ".modal-klas-add-accept", ( e ) => {
  $( e.currentTarget.parentNode.parentNode.parentNode ).submit();
} );

$( document ).on( "click", ".modal-lokaal-add-cancel", ( _e ) => {
  Modal.Close( ".modal-lokaal-add" ).finally( () => {
    Modal.Delete( ".modal-lokaal-add" );
  } );
  currentselect = null;
} );

$( document ).on( "click", ".modal-lokaal-add-accept", ( e ) => {
  $( e.currentTarget.parentNode.parentNode.parentNode ).submit();
} );

$( document ).on( "submit", ".modal-leerkrachten-add", ( e ) => {
  e.preventDefault();
  let info = {
    firstname: null,
    lastname: null,
    sex: null,
    klassen: null,
    days: null
  };
  let retval = true;

  $( e.currentTarget )
    .serializeArray()
    .map( ( item ) => {
      if ( item.name !== "geslacht" ) info[item.name] = item.value;
    } );

  $( ".modal-leerkrachten-add .dropdown-label" ).each( ( i, elem ) => {
    if ( elem.childNodes[0] === undefined ) return false;
    let datatype = $( elem.parentNode.parentNode ).data( "droptype" );
    info[datatype] = [];
    $.each( elem.childNodes, function ( i, elem ) {
      info[datatype].push( $( elem ).html().split( "<span>" )[0] );
    } );
  } );

  $.each(
    $( e.currentTarget.parentNode.parentNode.parentNode ).find(
      'select[name="geslacht"]'
    )[0].options,
    function ( i, val ) {
      if (
        val.value ===
        $( e.currentTarget.parentNode.parentNode.parentNode ).find(
          'select[name="geslacht"]'
        )[0].value
      ) {
        info.sex = i;
        return false;
      }
    }
  );

  $.each( info, ( _, info ) => {
    if ( info === null ) retval = false;
  } );

  if ( retval ) {
    Ajax.post( "/api/data/add/leerkracht", { lkrinfo: info } )
      .then( ( res ) => {
        Modal.Close( ".modal-leerkrachten-add" ).finally( () => {
          Modal.Delete( ".modal-leerkrachten-add" );
        } );
        if ( !currentselect ) {
          Notify.warning( "No option added, please reload!" );
          return;
        }
        if ( !( res && res.id ) ) {
          Notify.info( "New options not auto-selected!" );
          currentselect = null;
          return;
        }

        // Adding new options
        refreshSelect()
          .then( () => {
            $( currentselect ).val( res.id );
            currentselect = null;
          } )
          .catch( () => {} );
      } )
      .catch( () => {} );
  } else {
    Notify.error( "Niet alle velden zijn ingevuld" );
  }
} );

$( document ).on( "submit", ".modal-klas-add", ( e ) => {
  e.preventDefault();
  let info = {
    name: null,
    teacher: null
  };
  let retval = true;

  $( ".modal-klas-add" )
    .serializeArray()
    .map( ( input ) => {
      if ( !input.name || !input.value ) {
        retval = false;
        return;
      }
      info[input.name] = input.value;
    } );

  if ( !retval ) {
    Notify.error( "Niet alle velden zijn ingevuld" );
    return;
  }

  Ajax.post( "/api/data/add/klas", { klasinfo: info } )
    .then( ( res ) => {
      Modal.Close( ".modal-klas-add" ).finally( () => {
        Modal.Delete( ".modal-klas-add" );
      } );
      if ( !currentselect ) {
        Notify.warning( "No option added, please reload!" );
        return;
      }
      if ( !( res && res.id ) ) {
        Notify.info( "New options not auto-selected!" );
        currentselect = null;
        return;
      }

      // Adding new options
      refreshSelect()
        .then( () => {
          $( currentselect ).val( res.id );
          currentselect = null;
        } )
        .catch( () => {} );
    } )
    .catch( () => {} );
} );

$( document ).on( "submit", ".modal-lokaal-add", ( e ) => {
  e.preventDefault();
  let info = {};
  let retval = true;

  $( e.currentTarget )
    .serializeArray()
    .map( ( input ) => {
      if ( !input.name || !input.value ) {
        retval = false;
        return;
      }
      info[input.name] = input.value;
    } );

  if ( !retval ) {
    Notify.error( "Niet alle velden zijn ingevuld" );
    return;
  }

  Ajax.post( "/api/data/add/lokaal", { lokinfo: info } )
    .then( ( res ) => {
      Modal.Close( ".modal-lokaal-add" ).finally( () => {
        Modal.Delete( ".modal-lokaal-add" );
      } );
      if ( !currentselect ) {
        Notify.warning( "No option added, please reload!" );
        return;
      }
      if ( !( res && res.id ) ) {
        Notify.info( "New options not auto-selected!" );
        currentselect = null;
        return;
      }

      // Adding new options
      refreshSelect()
        .then( () => {
          $( currentselect ).val( res.id );
          currentselect = null;
        } )
        .catch( () => {} );
    } )
    .catch( () => {} );
} );

// Handles save btn click
$( document ).on( "click", ".savebar-save", ( _e ) => {
  $( ".error-container" ).submit();
} );

$( document ).on( "submit", ".error-container", ( e ) => {
  e.preventDefault();
  Ajax.post( "/api/creator/reserrors", {
    errors: $( e.currentTarget ).serializeArray() ?? [],
    errorType: errtype
  } )
    .then( ( res ) => {
      if ( res.msg ) {
        Notify.success( res.msg );
      }
      importErrorLoop( $( e.currentTarget ).serializeArray() );
    } )
    .catch( () => {} );
} );

//DROPDOWN FOR CONFIG
var indropdown = false;
var inuserdiv = false;

$( document ).on( "click", ".nav-config", function ( e ) {
  e.preventDefault();
  window.location.href = "/html/config.html";
} );

$( document ).on( "mouseenter", ".nav-user", function ( e ) {
  e.preventDefault();
  $( ".nav-dropdown" ).animate(
    {
      top: $( ".nav-user" ).css( "height" ).split( "px" )[0] * 1 - 10,
      opacity: 1.0
    },
    500,
    "swing"
  );
  inuserdiv = true;
} );

$( document ).on( "mouseleave", ".nav-user", function ( e ) {
  e.preventDefault();
  setTimeout( () => {
    if ( !indropdown ) {
      $( ".nav-dropdown" ).animate(
        {
          top: 0,
          opacity: 0.0
        },
        500,
        "swing"
      );
    }
  }, 50 );
  inuserdiv = false;
} );

$( document ).on( "mouseenter", ".nav-dropdown", function ( e ) {
  e.preventDefault();
  indropdown = true;
} );

$( document ).on( "mouseleave", ".nav-dropdown", function ( e ) {
  e.preventDefault();
  indropdown = false;
  setTimeout( () => {
    if ( !inuserdiv ) {
      $( ".nav-dropdown" ).animate(
        {
          top: 0,
          opacity: 0.0
        },
        500,
        "swing"
      );
    }
  }, 50 );
} );

$(document).on('click', '.nav-dashboard', function (e) {
	e.preventDefault();
	window.location.href = '/html/dashboard.html';
});

$(document).on('click', '.nav-schemacreator', (e)=>{
	e.preventDefault()
	window.location.href = `/html/schemacreator.html?klas=${klas}&admin=true`
})

const getUrlParameter = ( sParam ) => {
  var sPageURL = window.location.search.substring( 1 ),
    sURLVariables = sPageURL.split( "&" ),
    sParameterName,
    i;

  for ( i = 0; i < sURLVariables.length; i++ ) {
    sParameterName = sURLVariables[i].split( "=" );

    if ( sParameterName[0] === sParam ) {
      return sParameterName[1] === undefined
        ? true
        : decodeURIComponent( sParameterName[1] );
    }
  }
};

const SetupNavBar = () => {
  $( ".nav-list" ).html(
    `<a class="nav nav-schemacreator text-hover text-hover-both-go-up nav-left nav-selected">Schemacreator</a> 
			<a class="nav nav-dashboard text-hover text-hover-both-go-up nav-left">Dashboard</a> 
			<a class="nav nav-right nav-logout text-hover text-hover-both-go-up"><i class="fas fa-sign-out-alt"></i> Log-out</a>
			<a class="nav nav-right nav-user">WELCOME ${ sessionStorage.username.toUpperCase() }</a>
			<div class="nav nav-dropdown"><a class="nav nav-config"><i class="fa fa-cog"></i>CONFIG</a></div>
			<div class="nav-darkmode-toggle nav-right">
				<span>☀️</span>
				<input type="checkbox" value="off" id="toggle-switch" />
				<label for="toggle-switch"><span class="screen-reader-text">Toggle Color Scheme</span></label>
				<span>🌙</span>
			</div>`
  );
  $( ".nav-dropdown" ).css( {
    top: 0,
    right: $( ".nav-logout" ).css( "width" ).split( "px" )[0] * 1 + 30,
    "min-width": $( ".nav-user" ).css( "width" ).split( "px" )[0] * 1 - 20,
    display: "block",
    opacity: 0.0
  } );
};

const SetupErrorCon = ( errors, existing ) => {
  $( ".error-container" ).html( "" );
  return new Promise( ( res, _rej ) => {
    if ( !existing ) existing = [];
    errors.map( ( err, index ) => {
      let elem = `<div class="error-item">
				<label for="${ err.toLowerCase() }">${ err.toLowerCase() }</label>
				<select name="${ err.toLowerCase() }" required id="error-${ index }-options">
					<option disabled selected value="reset"> -- select an option -- </option>`;

      switch ( errtype ) {
        case "Teacher":
          existing.map( ( existdata ) => {
            elem = `${ elem } <option value="${
              existdata._id
            }">${ `${ existdata.firstname } ${ existdata.lastname }` }</option>`;
          } );
          break;
        case "Class":
          existing.map( ( existdata ) => {
            elem = `${ elem } <option value="${ existdata._id }">${ existdata.name }</option>`;
          } );
          break;
        case "Lokaal":
          existing.map( ( existdata ) => {
            elem = `${ elem } <option value="${ existdata._id }">${
              existdata.blok + existdata.name
            }</option>`;
          } );
          break;
        default:
          break;
      }

      elem = `${ elem }<option value="new">Nieuwe optie toevoegen</option></select>
			</div>`;
      $( ".error-container" ).append( elem );
    } );
    res();
  } );
};

const openModal = ( id ) => {
  switch ( errtype ) {
    case "Teacher":
      Ajax.get( "/api/data/info/klassen" )
        .then( ( res ) => {
          let klasnames = [];
          res.forEach( ( klas ) => {
            klasnames.push( klas.name );
          } );
          Modal.New(
            `.modal-leerkrachten-add modal-${ id }`,
            "Leerkracht toevoegen",
            {
              input: {
                label: {
                  type: "before",
                  text: "Voornaam"
                },
                name: "firstname",
                type: "text"
              },
              input2: {
                label: {
                  type: "before",
                  text: "Achternaam"
                },
                name: "lastname",
                type: "text"
              },
              select1: {
                text: "Geslacht",
                name: "geslacht",
                options: [ "Man", "Vrouw", "Andere" ]
              },
              dropdown1: {
                data: {
                  droptype: "klassen"
                },
                options: klasnames
              },
              dropdown2: {
                data: {
                  droptype: "days"
                },
                options: [
                  "Maandag",
                  "Dinsdag",
                  "Woensdag",
                  "Donderdag",
                  "Vrijdag"
                ]
              }
            },
            {
              button1: {
                text: "Cancel",
                class: "modal-leerkrachten-add-cancel"
              },
              button2: {
                text: "Save",
                class: "modal-leerkrachten-add-accept"
              }
            }
          );
          Modal.Open( `.modal-${ id }` );
        } )
        .catch( () => {} );
      break;
    case "Class":
      Ajax.get( "/api/data/info/leerkrachten" )
        .then( ( res ) => {
          const lkrnames = {};
          res.forEach( ( lkr ) => {
            lkrnames[lkr._id] = `${ lkr.firstname } ${ lkr.lastname }`;
          } );
          Modal.New(
            `.modal-klas-add modal-${ id }`,
            "Klas toevoegen",
            {
              input1: {
                label: {
                  type: "before",
                  text: "Klasnaam"
                },
                name: "name",
                type: "text",
                data: {
                  inputinfo: "name"
                }
              },
              select1: {
                text: "Klastitularis",
                name: "teacher",
                options: lkrnames
              }
            },
            {
              button1: {
                text: "Cancel",
                class: "modal-klas-add-cancel"
              },
              button2: {
                text: "Save",
                class: "modal-klas-add-accept"
              }
            }
          );
          Modal.Open( `.modal-${ id }` );
        } )
        .catch( () => {} );
      break;
    case "Lokaal":
      Modal.New(
        `.modal-lokaal-add modal-${ id }`,
        "Lokaal bewerken",
        {
          input1: {
            label: {
              type: "before",
              text: "Lokaalnummer"
            },
            name: "name",
            type: "text",
            data: {
              inputinfo: "name"
            }
          },
          input2: {
            label: {
              type: "before",
              text: "Blok"
            },
            name: "blok",
            type: "text",
            data: {
              inputinfo: "blok"
            }
          },
          select: {
            text: "Lokaaltype",
            name: "loktype",
            options: [ "standaard", "wetenschap", "praktijk" ]
          }
        },
        {
          button1: {
            text: "Cancel",
            class: "modal-lokaal-add-cancel"
          },
          button2: {
            text: "Save",
            class: "modal-lokaal-add-accept"
          }
        }
      );
      Modal.Open( `.modal-${ id }` );
      break;

    default:
      break;
  }
};

const refreshSelect = () => {
  return new Promise( ( resolve, reject ) => {
    // Caching the old answers
    let optioncache = $( ".error-container" ).serializeArray();

    // Setting up the options
    Ajax.get( "/api/creator/getimporterrors", {
      errtype: errtype
    } )
      .then( ( res ) => {
        // res {errors, existing}
        let elem = `<option disabled selected value="reset"> -- select an option -- </option>`;
        switch ( errtype ) {
          case "Teacher":
            res.existing.map( ( existdata ) => {
              elem = `${ elem } <option value="${
                existdata._id
              }">${ `${ existdata.firstname } ${ existdata.lastname }` }</option>`;
            } );
            break;
          case "Class":
            res.existing.map( ( existdata ) => {
              elem = `${ elem } <option value="${ existdata._id }">${ existdata.name }</option>`;
            } );
            break;
          case "Lokaal":
            res.existing.map( ( existdata ) => {
              elem = `${ elem } <option value="${ existdata._id }">${
                existdata.blok + existdata.name
              }</option>`;
            } );
            break;
          default:
            break;
        }

        elem = `${ elem }<option value="new">Nieuwe optie toevoegen</option>`;
        $( ".error-item select" ).each( ( _, select ) => {
          $( select ).html( elem );
        } );

        $.each( optioncache, ( i, option ) => {
          $( `select[name="${ option.name }"]` ).val( option.value );
        } );

        resolve();
      } )
      .catch( ( res ) => {
        Notify.error( res.responseText );
        reject();
      } );
  } );
};

const importErrorLoop = async ( pResErrors ) => {
  while ( errorLoop ) {
    await new Promise( ( res ) => {
      setTimeout( () => {
        Ajax.get( "/api/creator/getimporterrors", { errtype } )
          .then( ( response ) => {
            let newErr = false;
            // Check if new error's have been added
            response.errors.map( ( err ) => {
              let retval = false;
              // Check if resolved error still exist in backend (Not resolved)
              pResErrors = pResErrors.map( ( resErr ) => {
                if ( resErr.value.toLowerCase() == err.toLowerCase() ) {
                  $( `.error-item select[name="${ resErr.name }"]` ).val( "reset" );
                  // Return nothing bcs error is not resolved
                  return;
                }
                // Error is resolved so error table is changed
                newErr = true;
                return resErr;
              } );

              // Check if old error's need to be removed
              retval = $( `.error-item select[name="${ err.toLowerCase() }"]` )
                ? true
                : false;
              if ( !retval ) {
                // Add new select
                newErr = true;
                let elem = `<div class="error-item">
							<label for=${ err }>${ err }</label>
							<select name="${ err }" required id="error-${
                  $( $( ".error-item select" ).splice( -1 )[0] )
                    .attr( "id" )
                    .split( /error-|-options/gim )[1] + 1
                }-options">
								${ $( ".error-item select" ).html() }
							</select>
							</div>`;
                $( ".error-container" ).append( elem );
              }
            } );
            // Remove all resolved error's from site
            pResErrors.map( ( resErr ) => {
              $(
                $( `.error-item select[name="${ resErr.name }"]` ).parentNode
              ).remove();
            } );
            if ( newErr ) {
              // Cancel the loop, inform user there are new selects added
              errorLoop = false;
              Notify.info( `Error's lijst geupdate!` );
            }
            res();
          } )
          .catch( () => {
            res();
          } );
      }, 5000 );
    } );
  }
};
