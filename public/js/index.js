jQuery(function () {
	$('.wrapper').css('display', 'none');
	if (sessionStorage.errorType !== undefined) {
		Notify.warning(sessionStorage.error, 4000);
		sessionStorage.removeItem('error');
		sessionStorage.removeItem('errorType');
	}
	if (sessionStorage.logout !== undefined && sessionStorage.logout) {
		Notify.info('Succesvol uitgelogd!');
		sessionStorage.removeItem('logout');
	}
	setupNavbar();
});

$('.login-form').on('submit', async function (e) {
	e.preventDefault();
	let data = $('.login-form').serializeArray();
	let check = true;
	$.each(data, function (index, input) {
		if (input.value.trim() == '') {
			$('.login-form')
				.find('input[name=' + input.name + ']')
				.addClass('input-empty');
			$('.login-form')
				.find('input[name=' + input.name + ']')
				.css('border', '2px solid red');
			check = false;
		} else {
			if (
				$('.login-form')
					.find('input[name=' + input.name + ']')
					.hasClass('input-empty')
			) {
				$('.login-form')
					.find('input[name=' + input.name + ']')
					.removeClass('input-empty');
			}
			$('.login-form')
				.find('input[name=' + input.name + ']')
				.css('border', '2px solid #5fbae9;');
		}
	});

	if (check) {
		await $.ajax({
			url: window.location.origin + '/api/user/login',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Accept: '*/*',
			},
			dataType: 'json',
			cache: false,
			data: JSON.stringify({
				email: $('.login-form').find('input[name=username]').val(),
				password: btoa($('.login-form').find('input[name=password]').val()),
			}),
			success: function (response) {
				if (typeof Storage !== 'undefined') {
					if (response.perms !== 'user') {
						Notify.success('Succesvol ingelogd');
						$('.fadeInDown').animate({ 'margin-top': -40 + '%', opacity: 0 }, () => {
							$('.wrapper').css('display', 'none');
						});
						sessionStorage.token = response.token;
						sessionStorage.username = response.username;
						setupNavbar();
					} else if (response.perms === 'user') {
						sessionStorage.token = response.token;
						sessionStorage.username = response.username;
						window.location.replace(`html/schemacreator.html?klas=${response.klas}`);
						Notify.success('Succesvol ingelogd');
					} else {
						Notify.error('Er is iets fout gelopen. Probeer opnieuw!');
					}
				}
			},
			error: function (res) {
				console.error(`login error`, res);
				Notify.error(res.msg ?? res.responseText);
			},
		});
	}
});
$('.register-form').on('submit', async function (e) {
	e.preventDefault();
	let data = $('.register-form').serializeArray();
	let check = true;
	$.each(data, function (_index, input) {
		if (input.value.trim() == '') {
			$('.register-form')
				.find('input[name=' + input.name + ']')
				.addClass('input-empty');
			$('.register-form')
				.find('input[name=' + input.name + ']')
				.css('border', '2px solid red');
			check = false;
		} else {
			if (
				$('.register-form')
					.find('input[name=' + input.name + ']')
					.hasClass('input-empty')
			) {
				$('.register-form')
					.find('input[name=' + input.name + ']')
					.removeClass('input-empty');
			}
			$('.register-form')
				.find('input[name=' + input.name + ']')
				.css('border', '2px solid #5fbae9;');
		}
	});

	if (check) {
		await $.ajax({
			url: window.location.origin + '/api/user/register',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Accept: '*/*',
			},
			dataType: 'json',
			cache: false,
			data: JSON.stringify({
				name: $('.register-form').find('input[name=username]').val(),
				email: $('.register-form').find('input[name=email]').val(),
				firstname: $('.register-form').find('input[name=firstname]').val(),
				lastname: $('.register-form').find('input[name=lastname]').val(),
				password: btoa($('.register-form').find('input[name=password]').val()),
			}),
			success: await function (response) {
				if (typeof Storage !== 'undefined') {
					if (response.perms !== 'user') {
						Notify.success('Succesvol Geregistreerd');
						$('.fadeInDown').animate({ 'margin-top': -40 + '%', opacity: 0 }, () => {
							$('.wrapper').css('display', 'none');
						});
						sessionStorage.token = response.token;
						sessionStorage.username = response.username;
						setupNavbar();
					} else if (response.perms === 'user') {
						sessionStorage.token = response.token;
						sessionStorage.username = response.username;
						Notify.success('Succesvol Geregistreerd');
					} else {
						Notify.error('Er is iets fout gelopen. Probeer opnieuw!');
					}
				}
			},
			error: await function (res) {
				console.error(`registration error:`, res);
				Notify.error(res.responseJSON.msg ?? res.responseText);
			},
		});
	}
});

$(document).on('click', '.nav-login', function () {
	if ($('#form-titles').find('[data-header="signin"]').hasClass('inactive')) {
		$('#form-titles').find('[data-header="signin"]').removeClass('inactive');
		$('#form-titles').find('[data-header="signin"]').addClass('active');
	}
	if ($('#form-titles').find('[data-header="signin"]').hasClass('underlineHover')) {
		$('#form-titles').find('[data-header="signin"]').removeClass('underlineHover');
	}
	if ($('#form-titles').find('[data-header="signup"]').hasClass('active')) {
		$('#form-titles').find('[data-header="signup"]').addClass('inactive');
		$('#form-titles').find('[data-header="signup"]').addClass('underlineHover');
		$('#form-titles').find('[data-header="signup"]').removeClass('active');
	}
	$('.fadeInDown').css('margin-top', 0 + '%');
	$('.fadeInDown').css('opacity', 100 + '%');
	$('.login-form').css('display', 'block');
	$('#formFooter').css('display', 'block');
	$('.register-form').css('display', 'none');
	$('.wrapper').css('display', 'flex');
});

$(document).on('click', '.nav-register', function () {
	if ($('#form-titles').find('[data-header="signup"]').hasClass('inactive')) {
		$('#form-titles').find('[data-header="signup"]').removeClass('inactive').addClass('active');
	}
	if ($('#form-titles').find('[data-header="signup"]').hasClass('underlineHover')) {
		$('#form-titles').find('[data-header="signup"]').removeClass('underlineHover');
	}
	if ($('#form-titles').find('[data-header="signin"]').hasClass('active')) {
		$('#form-titles')
			.find('[data-header="signin"]')
			.addClass('inactive')
			.addClass('underlineHover')
			.removeClass('active');
	}
	$('.login-form').css('display', 'none');
	$('#formFooter').css('display', 'none');
	$('.register-form').css('display', 'block');
	$('.wrapper').css('display', 'flex');
});

$(document).on('click', '.form-title', function () {
	let btntype = $(this).data('header');
	if (!$(this).hasClass('active')) {
		$(this).removeClass('inactive');
		$(this).removeClass('underlineHover');
		$(this).addClass('active');
		if (btntype === 'signup') {
			//$('#form-titles').find('[data-header="signin"]')
			$('#form-titles')
				.find('[data-header="signin"]')
				.removeClass('active')
				.addClass('inactive')
				.addClass('underlineHover');
			$('.login-form').css('display', 'none');
			$('#formFooter').css('display', 'none');
			$('.register-form').css('display', 'block');
		} else if (btntype === 'signin') {
			//$('#form-titles').find('[data-header="signup"]')
			$('#form-titles')
				.find('[data-header="signup"]')
				.removeClass('active')
				.addClass('inactive')
				.addClass('underlineHover');
			$('.register-form').css('display', 'none');
			$('.login-form').css('display', 'block');
			$('#formFooter').css('display', 'block');
		}
	}
});

$(document).on('click', '.nav-dashboard', function (e) {
	e.preventDefault();

	window.location.href = '/html/dashboard.html';
});

$(document).on('click', '.nav-logout', function (e) {
	e.preventDefault();

	if (sessionStorage.username !== undefined && sessionStorage.token !== undefined) {
		sessionStorage.removeItem('username');
		sessionStorage.removeItem('token');
		Notify.info('Succesvol uitgelogd!');
		setupNavbar();
	} else {
		Notify.info('Je bent niet ingelogd', 4000);
	}
});

$(document).on('click', '.nav-user', e => {
	Notify.info($(e.target).html());
});

$(document).keydown(function (e) {
	if (e.keyCode == 27) {
		if ($('.wrapper').css('display') !== 'none') {
			$('.fadeInDown').animate({ 'margin-top': -40 + '%', opacity: 0 }, () => {
				$('.wrapper').css('display', 'none');
			});
		}
	}
});

const setupNavbar = async function () {
	if (sessionStorage.token !== undefined) {
		await Ajax.get('/api/user/checktoken')
			.then(res => {
				let elem = '';
				if (res.perms !== 'user') {
					elem = elem + '<a class="nav nav-dashboard nav-left text-hover text-hover-both-go-up">Dashboard</a>';
				}
				elem = `${elem}<a class="nav nav-right nav-logout"><i class="fas fa-sign-out-alt"></i> Log-out</a>
			<a class="nav nav-right nav-user">WELCOME ${sessionStorage.username.toUpperCase()}</a>
			<div class="nav nav-dropdown"><a class="nav nav-config"><i class="fa fa-cog"></i>CONFIG</a></div>
			<div class="nav-darkmode-toggle nav-right">
				<span>☀️</span>
				<input type="checkbox" value="off" id="toggle-switch" />
				<label for="toggle-switch"><span class="screen-reader-text">Toggle Color Scheme</span></label>
				<span>🌙</span>
			</div>`;
				$('.nav-list').html(elem);
				$('.nav-dropdown').css({
					top: 0,
					right: $('.nav-logout').css('width').split('px')[0] * 1 + 30,
					'min-width': $('.nav-user').css('width').split('px')[0] * 1 - 20,
					display: 'block',
					opacity: 0.0,
				});
			})
			.catch(res => {
				console.error(`Token error:`, res);
				Notify.error(res.responseJSON.msg ?? res.responseText);
			});
	} else {
		$('.nav-list').html(
			'<a class="nav nav-register text-hover nav-right text-hover-both-go-up"><i class="fas fa-user-plus"></i> Sign-up </a>' +
				'<a class="nav nav-login text-hover nav-right text-hover-both-go-up"><i class="fas fa-sign-in-alt"></i> Login</a>'
		);
	}
};

//DROPDOWN FOR CONFIG
var indropdown = false;
var inuserdiv = false;

$(document).on('click', '.nav-config', function (e) {
	e.preventDefault();
	window.location.href = '/html/config.html';
});

$(document).on('mouseenter', '.nav-user', function (e) {
	e.preventDefault();
	$('.nav-dropdown').animate(
		{
			top: $('.nav-user').css('height').split('px')[0] * 1 - 10,
			opacity: 1.0,
		},
		500,
		'swing'
	);
	inuserdiv = true;
});

$(document).on('mouseleave', '.nav-user', function (e) {
	e.preventDefault();
	setTimeout(() => {
		if (!indropdown) {
			$('.nav-dropdown').animate(
				{
					top: 0,
					opacity: 0.0,
				},
				500,
				'swing'
			);
		}
	}, 50);
	inuserdiv = false;
});

$(document).on('mouseenter', '.nav-dropdown', function (e) {
	e.preventDefault();
	indropdown = true;
});

$(document).on('mouseleave', '.nav-dropdown', function (e) {
	e.preventDefault();
	indropdown = false;
	setTimeout(() => {
		if (!inuserdiv) {
			$('.nav-dropdown').animate(
				{
					top: 0,
					opacity: 0.0,
				},
				500,
				'swing'
			);
		}
	}, 50);
});
