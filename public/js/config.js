jQuery(async function () {
	if (sessionStorage.token === undefined) {
		window.location.replace('../.');
		sessionStorage.errorType = 'redirect';
		sessionStorage.error = 'Je moet eerst inloggen';
	} else {
		Ajax.get('/api/user/checktoken', {})
			.then(() => {
				setupNavbar();
				setupUser();
			})
			.catch(() => {
				window.location.replace('../.');
				sessionStorage.errorType = 'redirect';
				sessionStorage.error = 'Je token is ongeldig';
			});
	}
});

$(document).on('click', '.config-category', async function (e) {
	e.preventDefault();
	let currentcat = $($(this.parentNode).find('.active')[0]).data('category');
	let selectedcat = $(this).data('category');
	let elem = '';
	if (currentcat !== selectedcat) {
		switch (selectedcat) {
			case 'user':
				elem = `<div class="config-title">Gebruikserinfo</div> <div class="config-input"> <label for="username">Gebruikersnaam:</label> <input name="username" type="text"></div> <div class="config-input"> <label for="email">Email:</label> <input name="email" type="text"></div><div class="config-title">Wachtwoorden</div><div class="config-input">  <label for="oldpassword">Oud Wachtwoord:</label>  <input name="oldpassword" type="password"></div><div class="config-input">  <label for="newpassword">Nieuw Wachtwoord:</label>  <input name="newpassword" type="password"></div><div class="config-input">  <label for="repeatnewpassword">Herhaal Nieuw Wachtwoord:</label>  <input name="repeatnewpassword" type="password"></div><div class="config-action">  <button id="config-action-user-save-info">Save</button></div>`;
				if ($($(this.parentNode).find('.active')[0]).hasClass('active')) {
					$($(this.parentNode).find('.active')[0]).removeClass('active');
				}
				$(this).addClass('active');
				$('.config-actions').html('');
				$('.config-actions').append(elem);
				setupUser();
				break;
			case 'school':
				Ajax.get('/api/user/checktoken')
					.then(res => {
						if (res.perms !== 'user') {
							elem = `
							<div class="config-title">Schoolinfo</div>
							<div class="config-input">
								<label for="schoolname">Schoolnaam:</label>
								<input name="schoolname" type="text">
							</div>
							<div class="config-input">
								<label for="schoolabbr">School afkorting:</label>
								<input name="schoolabbr" type="text">
							</div>
							<div class="config-title">Actie's</div>
							<div class="config-input">
								<label for="passwordlength">Standard password lengte:</label> 
								<input id="passwordlength" name="passwordlength" min="6" max="255" type="number" value="6"></div>
							  ${
									''
									// <div class="config-action">
									// <button id="config-download-users">Download User list</button>
									// </div>
								}
							<div class="config-action">
								<button id="config-save-info">Save</button>
							</div>`;
							if ($($(e.target.parentNode).find('.active')[0]).hasClass('active')) {
								$($(e.target.parentNode).find('.active')[0]).removeClass('active');
							}
							$(e.target).addClass('active');
							$('.config-actions').html('');
							$('.config-actions').append(elem);
							setupSchool();
						} else {
							Notify.warning('Je hebt hier de rechten niet voor');
						}
					})
					.catch(() => {});
				break;
			default:
				break;
		}
	}
});

$(document).on('submit', '.config-actions', async function (e) {
	e.preventDefault();
	let data = $('.config-actions').serializeArray();
	let info = {};
	let retval = true;
	switch ($($(this.parentNode).find('.active')[0]).data('category')) {
		case 'user':
			$.each(data, function (i, input) {
				if (input.name !== undefined && input.name !== '' && input.value !== undefined && input.value !== '') {
					info[input.name] = input.value;
				}
			});

			if (
				info['oldpassword'] !== undefined &&
				info['newpassword'] !== undefined &&
				info['repeatnewpassword'] !== undefined
			) {
				info['oldpassword'] = btoa(info['oldpassword']);
				info['newpassword'] = btoa(info['newpassword']);
				info['repeatnewpassword'] = btoa(info['repeatnewpassword']);
				if (info['oldpassword'] === info['newpassword']) {
					$('.config-input').find('input[name="oldpassword"]').css('border', '0.1vh solid red');
					$('.config-input').find('input[name="newpassword"]').css('border', '0.1vh solid red');
					$('.config-input').find('input[name="repeatnewpassword"]').css('border', '0.1vh solid #2e3f4e');
					retval = false;
				} else if (info['newpassword'] !== info['repeatnewpassword']) {
					$('.config-input').find('input[name="oldpassword"]').css('border', '0.1vh solid #2e3f4e');
					$('.config-input').find('input[name="newpassword"]').css('border', '0.1vh solid red');
					$('.config-input').find('input[name="repeatnewpassword"]').css('border', '0.1vh solid red');
					retval = false;
				} else {
					info['password'] = info['newpassword'];
					delete info['newpassword'];
					delete info['repeatnewpassword'];
					$('.config-input').find('input[name="oldpassword"]').css('border', '0.1vh solid #2e3f4e');
					$('.config-input').find('input[name="newpassword"]').css('border', '0.1vh solid #2e3f4e');
					$('.config-input').find('input[name="repeatnewpassword"]').css('border', '0.1vh solid #2e3f4e');
				}
			}
			if (retval) {
				Ajax.post('/api/config/updateinfo', {
					type: 'user',
					info: info,
				})
					.then(() => {
						Notify.success('Wijzigingen opgeslagen');
						setupUser();
					})
					.catch(() => {});
			}
			break;

		case 'school':
			$.each(data, function (i, input) {
				if (input.name !== undefined && input.name !== '' && input.value !== undefined && input.value !== '') {
					info[input.name] = input.value;
				}
			});
			Ajax.post('/api/config/updateinfo', {
				type: 'school',
				info: {
					name: info.schoolname,
					abbreviation: info.schoolabbr,
					passlength: info.passwordlength,
				},
			})
				.then(() => {
					Notify.success('Wijzigingen opgeslagen');
					setupSchool();
				})
				.catch(() => {});
			break;
		default:
			break;
	}
});

$(document).on('click', '#config-download-users', function (e) {
	e.preventDefault();
});

$(document).on('click', '.nav-dashboard', function (e) {
	e.preventDefault();

	window.location.href = '/html/dashboard.html';
});

$(document).on('click', '.config-remove-uploaded-rooster', async function (_e) {
	Ajax.get('/api/config/removeuploadrooster')
		.then(() => {
			Notify.success('Rooster is succesvol verwijderd');
		})
		.catch(() => {});
});

const setupNavbar = async function () {
	Ajax.get('/api/user/checktoken')
		.then(res => {
			let elem = '';
			if (res.perms !== 'user') {
				elem = elem + '<a class="nav nav-dashboard nav-left text-hover text-hover-both-go-up">Dashboard</a>';
			}
			elem =
				elem +
				`<a class="nav nav-center nav-selected infoheader"><span>CONFIG</span></a>
        <a class="nav nav-right nav-logout text-hover text-hover-both-go-up"><i class="fas fa-sign-out-alt"></i> Log-out</a>
        <a class="nav nav-right nav-user">WELCOME ${sessionStorage.username.toUpperCase()}</a>
        <div class="nav nav-dropdown"><a class="nav nav-config"><i class="fa fa-cog"></i>CONFIG</a></div>
				<div class="nav-darkmode-toggle nav-right">
					<span>☀️</span>
					<input type="checkbox" id="toggle-switch" />
					<label for="toggle-switch"><span class="screen-reader-text">Toggle Color Scheme</span></label>
					<span>🌙</span>
				</div>`;
			$('.nav-list').html(elem);
			$('.nav-dropdown').css({
				top: 0,
				right: $('.nav-logout').css('width').split('px')[0] * 1 + 30,
				'min-width': $('.nav-user').css('width').split('px')[0] * 1 - 20,
				display: 'block',
				opacity: 0.0,
			});
		})
		.catch(() => {});
};

const setupUser = async function () {
	Ajax.get('/api/config/getinfo', {
		type: 'user',
	})
		.then(res => {
			if (res !== undefined && Object.entries(res).length > 0) {
				$($('.config-actions').find('input[name="username"]')[0]).attr('placeholder', res.username);
				$($('.config-actions').find('input[name="email"]')[0]).attr('placeholder', res.mail);
			}
		})
		.catch(() => {});
};

const setupSchool = async function () {
	Ajax.get('/api/config/getinfo', {
		type: 'school',
	})
		.then(res => {
			if (res !== undefined && Object.entries(res).length > 0) {
				$($('.config-actions').find('input[name="schoolname"]')[0]).attr('placeholder', res.name);
				$($('.config-actions').find('input[name="schoolabbr"]')[0]).attr('placeholder', res.abbreviation);
				$($('.config-actions').find('input[name="passwordlength"]')[0]).attr('placeholder', res.passlength);
				$('.config-remove-uploaded-rooster').css({
					display: res.roosteruploaded ? 'block' : 'none',
				});
				if (res.logo !== undefined) {
					$($('.config-actions').find('button.schoollogo')[0].parentNode).append(`<img src="${res.logo}" alt="LOGO">`);
				}
			}
		})
		.catch(() => {});
};

//DROPDOWN FOR CONFIG
var indropdown = false;
var inuserdiv = false;

$(document).on('click', '.nav-config', function (e) {
	e.preventDefault();
	Notify.info('Je zit al op deze pagina');
});

$(document).on('mouseenter', '.nav-user', function (e) {
	e.preventDefault();
	$('.nav-dropdown').animate(
		{
			top: $('.nav-user').css('height').split('px')[0] * 1 - 10,
			opacity: 1.0,
		},
		500,
		'swing'
	);
	inuserdiv = true;
});

$(document).on('mouseleave', '.nav-user', function (e) {
	e.preventDefault();
	setTimeout(() => {
		if (!indropdown) {
			$('.nav-dropdown').animate(
				{
					top: 0,
					opacity: 0.0,
				},
				500,
				'swing'
			);
		}
	}, 50);
	inuserdiv = false;
});

$(document).on('mouseenter', '.nav-dropdown', function (e) {
	e.preventDefault();
	indropdown = true;
});

$(document).on('mouseleave', '.nav-dropdown', function (e) {
	e.preventDefault();
	indropdown = false;
	setTimeout(() => {
		if (!inuserdiv) {
			$('.nav-dropdown').animate(
				{
					top: 0,
					opacity: 0.0,
				},
				500,
				'swing'
			);
		}
	}, 50);
});

$(document).on('click', '.nav-logout', async function (e) {
	e.preventDefault();

	if (sessionStorage.username !== undefined && sessionStorage.token !== undefined) {
		sessionStorage.removeItem('username');
		sessionStorage.removeItem('token');
		sessionStorage.logout = true;
		window.location.replace('../.');
	} else {
		Notify.info('Je bent niet ingelogd', 4000);
	}
});
