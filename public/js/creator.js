let klas = '';
let currentmode = 'edit-mode';
let admin = false;
let loadershown = false;
let teachers = null;
let lokalen = null;
let curhour = null;
let file = null;
let currentDragCard = null;
let schemaerr = {};
let schemadifferences = [];

const names = ['Eerste', 'Tweede', 'Derde', 'Vierde', 'Vijfde', 'Zesde', 'Zevende', 'Achtste', 'Negende'];
const dagen = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'];

$(async function () {
	if (sessionStorage.errorType !== undefined) {
		Notify.warning(sessionStorage.error, 4000);
		sessionStorage.removeItem('error');
		sessionStorage.removeItem('errorType');
	}
	if (sessionStorage.token === undefined) {
		window.location.replace('../.');
		sessionStorage.errorType = 'redirect';
		sessionStorage.error = 'Je moet eerst inloggen';
	}
});

$(document).on('click', '.creator-option-card:not(.used)', async function (e) {
	e.preventDefault();
	let retval = false;
	if (admin && currentmode === 'edit-mode') {
		// Check if values of all used cons are filled in
		let inputs = $('.creator-option-card.used');
		$.each(inputs, (_i, inputcard) => {
			$.each($(inputcard).find('input'), (_i, input) => {
				if ($(input).val().trim() === '') {
					$(input).css('border', '.1vh red solid');
					retval = true;
				} else {
					if ($(input).css('border') !== 0 && $(input).css('border-bottom') !== '.3vh solid #727272') {
						$(input).css({
							border: 0,
							'border-bottom': '.3vh solid #727272',
						});
					}
				}
			});
			$.each($(inputcard).find('select'), (_i, select) => {
				if ($(select).val().trim() === '' || $(select).val() === $(select).find('option').html()) {
					$(select).css('border', '.1vh red solid');
					retval = true;
				} else {
					if ($(select).css('border') !== 0 && $(select).css('border-bottom') !== '.3vh solid #727272') {
						$(select).css({
							border: 0,
							'border-bottom': '.3vh solid #727272',
						});
					}
				}
			});
		});

		// Replace <i> with input cons
		if (!retval) {
			if ($('.creator-option-card:not(.used)').length <= 1) {
				for (let i = 0; i < 6; i++) {
					$('.creator-options-collection').append(`
					<div class="creator-option-card">
						<div class="creator-option-card-con">
							<i class="fa fa-plus"></i>
						</div>
					</div>
				`);
				}
				// eslint-disable-next-line no-undef
				cardheightchanger();
			}
			$($('.creator-option-card:not(.used)')[0]).html(
				'<div class="creator-option-card-con"><input type="text" name="name" placeholder="Les"></div><div class="creator-option-card-con"><select name="teacher"><option>--Leerkracht--</option></select></div><div class="creator-option-card-con"><select name="lokaal"><option value="">--Kies een lokaal--</option></select></div><div class="creator-option-card-con"><input type="number" min="1" max="40" name="hours" placeholder="Aantal uren"></div>'
			);
			// eslint-disable-next-line no-undef
			await SetupCardOptions($('.creator-option-card:not(.used)'), teachers, lokalen);
			$($('.creator-option-card:not(.used)')[0]).addClass('used');
		}
	}
});

$('.creator-options').on('click', '#save', function (_e) {
	const cards = $('.creator-options-collection').find('.creator-option-card.used');
	const usablecards = [];
	Loader(true);
	$.each(cards, (_i, card) => {
		let retval = false;
		$.each($(card).find('input'), (_i, input) => {
			if ($(input).val().trim() === '') {
				retval = true;
			}
		});
		$.each($(card).find('select'), (_i, select) => {
			if ($(select).val().trim() === '' || $(select).val() === 'reset') {
				retval = true;
			}
		});
		if (!retval) {
			usablecards.push(card);
		}
	});

	if (usablecards.length > 0) {
		usablecards.forEach(card => {
			// Get if data is not undefined or null before making ajax request
			let info = {
				klas: klas,
			};
			$.each($(card).find('input'), (_i, input) => {
				if ($(input).attr('name')) {
					info[$(input).attr('name')] = $(input).val();
				}
			});
			$.each($(card).find('select'), (_i, select) => {
				if ($(select).attr('name')) {
					info[$(select).attr('name')] = $(select).val();
				}
			});

			// Check if a id is added to the card
			if ($(card).data('id')) {
				Ajax.post('/api/creator/updatecard', {
					cardid: $(card).data('id'),
					info: info,
				})
					.then(_res => {
						Loader(false);
					})
					.catch(() => {
						Loader(false);
					});
			} else {
				Ajax.post('/api/creator/addcard', {
					info: info,
				})
					.then(res => {
						if (res.token !== undefined) {
							$(card).data('id', res.token);
						}
						Notify.success('succesvol opgeslagen');
						Loader(false);
					})
					.catch(() => {
						Loader(false);
					});
			}
			// After request assign id to card to edit card instead of creating one
		});
	} else {
		Loader(false);
	}
});

$('.creator-options').on('click', '#switch-mode', e => {
	$('.spinner-con').css({
		display: 'flex',
		pointerEvents: 'all',
		opacity: 1,
	});
	currentmode = currentmode === 'edit-mode' ? 'drag-mode' : 'edit-mode';
	$(e.currentTarget).html(`<i class="fa fa-sync"></i> ${currentmode === 'edit-mode' ? 'Drag mode' : 'Edit mode'}`);
	switchCardMode();
	$('.spinner-con').css({
		display: 'none',
		pointerEvents: 'none',
		opacity: 0,
	});
	Notify.info(`Switched to ${currentmode === 'edit-mode' ? 'Edit mode' : 'Drag mode'}`);
});

$('.creator-hours').on('click', '#save', function (_e) {
	/* 
    info ={
      klas,
      hours = [{
        starthour,
        startmin,
      },{}]
    }
  */
	Loader(true);
	const info = {
		klas: klas,
		hours: [],
	};

	$.each($('.hour-cards').find('.hour-card'), (_i, card) => {
		let hour = $(card).data('hour') * 1;

		if (!hour) {
			console.log('NO HOUR DATA FOUND ON CARD');
			Notify.error('Er is iets fout gelopen');
			return false;
		}

		if ($(card).css('display') !== 'none') {
			let inputs = $(card).find('input');
			let retval = false;

			$.each(inputs, (_i, input) => {
				if ($(input).val().trim() === '') {
					retval = true;
				}
			});

			if (!retval) {
				info.hours[hour - 1] = {
					starthour: $(card).find('input[name="starthour"]').val(),
					startmin: $(card).find('input[name="startmin"]').val(),
					endhour: $(card).find('input[name="endhour"]').val(),
					endmin: $(card).find('input[name="endmin"]').val(),
				};
			}
		}
	});

	// do AJAX Request
	Ajax.post('/api/creator/updatehourcards', {
		info: info,
	})
		.then(() => {
			Notify.success('De uren zijn succesvol geupdate!');
			Loader(false);
		})
		.catch(() => {
			Loader(false);
		});
});

$('.creator-board').on('click', '#save', function (e) {
	e.preventDefault();
	const schema = {};
	$('.creator-board-card-list .creator-board-card-item').each((index, elem) => {
		schema[dagen[elem.dataset.day]] = GetHoursOfDay(dagen[elem.dataset.day]);
	});
	Ajax.post('/api/creator/lesschema', {
		schema,
		klas,
		diff: schemadifferences,
	})
		.then(res => {
			if (res.msg) {
				Notify[res.msgType || 'info'](res.msg);
				if (res.action === 'confirm') {
					Modal.New(
						'.modal-overwrite-schema',
						'Schemacreator',
						{
							p1: {
								text: 'Wil je je het reeds bestaande schema overschrijven?',
							},
						},
						{
							button1: {
								text: 'Cancel',
								class: 'modal-overwrite-schema-cancel',
							},
							button2: {
								text: 'Overwrite',
								class: 'modal-overwrite-schema-accept',
							},
						}
					);
					Modal.Open('.modal-overwrite-schema');
					$('.modal-overwrite-schema').data('schema', schema);
					return;
				}
				return;
			}
			Notify.success('Succesvol opgeslagen');
			Loader(false);
		})
		.catch(() => {
			Loader(false);
		});
});

$(document).on('click', '.modal-overwrite-schema-cancel', function (_e) {
	Notify.info('Schema niet opgeslagen!');
	Modal.Close('.modal-overwrite-schema').finally(() => {
		Modal.Delete('.modal-overwrite-schema');
	});
});

$(document).on('click', '.modal-overwrite-schema-accept', function (_e) {
	Modal.Close('.modal-overwrite-schema').then(() => {
		Modal.Delete('.modal-overwrite-schema');
	});
	Ajax.post('/api/creator/lesschema', {
		schema: $('.modal-overwrite-schema').data('schema'),
		force: true,
		klas,
		diff: schemadifferences,
	})
		.then(res => {
			if (res.msg) {
				Notify[res.msgType || 'info'](res.msg);
			}
			Loader(false);
		})
		.catch(() => {
			Loader(false);
		});
});

$(document).on('change', '#auren', function (_e) {
	if ($(this).val() * 1 > $(this).attr('max')) {
		$(this).val($(this).attr('max'));
	}
	if ($(this).val() * 1 < $(this).attr('min')) {
		$(this).val($(this).attr('min'));
	}

	let currentCardsam = 0;
	const newCardsam = $(this).val() * 1;

	$('.hour-card').each(function (_i, card) {
		if ($(card).css('display') !== 'none') {
			currentCardsam++;
		}
	});

	if (newCardsam - currentCardsam === 0) {
		return;
	} else if (newCardsam - currentCardsam < 0) {
		// Need to remove cards
		for (let i = 1; i < Math.abs(newCardsam - currentCardsam) + 1; i++) {
			$($('.hour-card')[$('.hour-card').length - i]).css('display', 'none');
		}
	} else if (newCardsam - currentCardsam > 0) {
		// Need to add cards
		for (let i = 0; i < Math.abs(currentCardsam + (newCardsam - currentCardsam)); i++) {
			if ($('.hour-card')[i]) {
				$($('.hour-card')[i]).css('display', 'block');
			} else {
				let elem = `<div class="hour-card" data-hour="${i + 1}"> <p class="hour-card-title">${
					names[i]
				} lesuur</p> <div> <label for="">Startuur</label> <input type="number" placeholder="08" min="1" max="24" name="starthour"> : <input type="number" placeholder="30" min="1" max="60" name="startmin"> </div><br><div>  <label for="">Einduur</label>  <input type="number" placeholder="09" min="1" max="24" name="endhour">  :  <input type="number" placeholder="30" min="1" max="60" name="endmin"></div> </div>`;
				$('.hour-cards').append(elem);
			}
		}
	}
});

$(document).on('click', '.nav-dashboard', function (e) {
	e.preventDefault();

	window.location.href = '/html/dashboard.html';
});

$(document).on('click', '.nav-logout', async function (e) {
	e.preventDefault();

	if (sessionStorage.username !== undefined && sessionStorage.token !== undefined) {
		sessionStorage.removeItem('username');
		sessionStorage.removeItem('token');
		sessionStorage.logout = true;
		window.location.replace('../.');
	} else {
		Notify.info('Je bent niet ingelogd', 4000);
	}
});

$(document).on('click', '.creator-board #import-label', function (_e) {
	Loader(true);
	Ajax.get('/api/creator/isroosteruploaded')
		.then(res => {
			if (res.found) {
				Notify.info('Er bestaat al een rooster voor deze school');
				// Check if there are errors
				Ajax.get('/api/creator/getimporterrors')
					.then(res => {
						if (res.msg) {
							Notify[res.msgType || 'info'](res.msg);
						}
						if (res.errors) {
							schemaerr = { ...res.errors };
							let options = {
								p1: {
									text: `Er zijn error's gevonden, Via de knoppen hieronder kunt u deze problemen oplossen`,
								},
							};
							if (schemaerr.Teacher) {
								options = {
									...options,
									button1: {
										text: 'Leerkrachten',
										class: 'modal-error-main-teachers',
									},
								};
							}
							if (schemaerr.Class) {
								options = {
									...options,
									button2: {
										text: 'Klassen',
										class: 'modal-error-main-classes',
									},
								};
							}
							if (schemaerr.Lokaal) {
								options = {
									...options,
									button3: {
										text: 'Lokalen',
										class: 'modal-error-main-lokalen',
									},
								};
							}
							if (Object.entries(schemaerr).length > 0) {
								Modal.New('.modal-error-main', `Gevonden errors`, options, {
									button1: {
										text: 'Close',
										class: 'modal-error-main-close',
									},
								});
								Modal.Open('.modal-error-main');
							}
						}
					})
					.catch(() => {});
			} else {
				$('#creater-board-rooster-upload').click();
			}
			Loader(false);
		})
		.catch(() => {
			Loader(false);
		});
});

$(document).on('change', '#creater-board-rooster-upload', function (e) {
	if (!e.target.files[0]) {
		Notify.error('Je hebt geen bestand geselecteerd??');
		return;
	}
	file = e.target.files[0];
	let reader = new FileReader();
	reader.readAsText(file);
	reader.onload = () => {
		let firstline = reader.result.split(/\r\n|\r|\n/g)[0];
		// Amount of headers: firstline.split( /,/ ).length-1
		let inputs = {
			p1: {
				text: 'Definiëer de namen van de velden van het CSV bestand',
			},
		};
		for (let index = 0; index < firstline.split(/,/).length; index++) {
			inputs['input' + (index + 1)] = {
				label: {
					type: 'before',
					text: 'Header nr.' + (index + 1),
				},
				name: 'header' + (index + 1),
				type: 'text',
			};
		}
		Modal.New(
			'.modal-upload-rooster',
			'Upload je lessenrooster',
			{ ...inputs },
			{
				button1: {
					text: 'Cancel',
					class: 'modal-upload-rooster-cancel',
				},
				button2: {
					text: 'Upload w/o header',
					class: 'modal-upload-rooster-rawupload',
				},
				button3: {
					text: 'Upload',
					class: 'modal-upload-rooster-upload',
				},
			}
		);
		Modal.Open('.modal-upload-rooster');
	};
	reader.onerror = err => {
		console.error(err);
		Notify.error('Couldn\t read file');
	};
});

$(document).on('click', '.modal-upload-rooster-cancel', _e => {
	Modal.Close('.modal-upload-rooster').finally(() => {
		$('#creater-board-rooster-upload').val('');
		Modal.Delete('.modal-upload-rooster');
	});
});

$(document).on('click', '.modal-upload-rooster-rawupload', _e => {
	Modal.Close('.modal-upload-rooster').finally(() => {
		Modal.Delete('.modal-upload-rooster');
	});
	var formData = new FormData();
	formData.append('rooster', file, file.name);
	formData.append('token', sessionStorage.token);
	var xhr = new XMLHttpRequest();

	// Open the connection
	xhr.open('POST', 'http://' + window.location.hostname + '/api/creator/uploadschemafile', true);
	// Set up a handler for when the task for the request is complete
	xhr.onload = function () {
		if (xhr.status == 200) {
			Notify.info('Upload copmlete!');
		} else {
			Notify.error('Upload error. Try again.');
		}
	};
	// Send the data.
	xhr.send(formData);
	$('#creater-board-rooster-upload').val('');
});

$(document).on('click', '.modal-upload-rooster-upload', _e => {
	let data = $('.modal-upload-rooster').serializeArray();
	let valid = true;
	var formData = new FormData();

	Modal.Close('.modal-upload-rooster').finally(() => {
		Modal.Delete('.modal-upload-rooster');
	});

	const headers = [];
	data.forEach(headinfo => {
		if (headinfo.value.trim() === '') {
			$('.modal-upload-rooster')
				.find('input[name=' + headinfo.name + ']')
				.addClass('input-empty');
			$('.modal-upload-rooster')
				.find('input[name=' + headinfo.name + ']')
				.css('border', '0.2vh solid red');
			valid = false;
		} else {
			if (
				$('.modal-upload-rooster')
					.find('input[name=' + headinfo.name + ']')
					.hasClass('input-empty')
			) {
				$('.modal-upload-rooster')
					.find('input[name=' + headinfo.name + ']')
					.removeClass('input-empty');
			}
			$('.modal-upload-rooster')
				.find('input[name=' + headinfo.name + ']')
				.css('border', '0.1vh solid #2c3e50');
			headers.push(headinfo.value);
		}
	});

	if (valid) {
		formData.append('rooster', file, file.name);
		formData.append('headers', JSON.stringify(headers));
		formData.append('token', sessionStorage.token);
		var xhr = new XMLHttpRequest();

		// Open the connection
		xhr.open('POST', 'http://' + window.location.hostname + '/api/creator/uploadschemafile', true);
		// Set up a handler for when the task for the request is complete
		xhr.onload = function () {
			if (xhr.status == 200) {
				Notify.info('Upload copmlete!');
			} else {
				Notify.error('Upload error. Try again.');
			}
		};
		// Send the data.
		xhr.send(formData);
		Modal.Close('.modal-upload-rooster').finally(() => {
			Modal.Delete('.modal-upload-rooster');
		});
		$('#creater-board-rooster-upload').val('');
	} else {
		Notify.warning('Niet alle headers zijn ingevuld');
	}
});

$(document).on('click', '.card-option-remove', function (_e) {
	let target = this.parentNode.parentNode;
	$('.creator-options-collection .creator-option-card.used').each((index, card) => {
		if ($(card).data('id') === $(target).data('id')) {
			curhour = $(card).find('.creator-option-card-con > p')[3];
			$(curhour).html($(curhour).html() * 1 + 1);
			if ($(curhour).html() * 1 == 1) {
				$(card).draggable('enable');
				$(card).draggable('option', { cursor: 'move' });
			}
			// clear card
			$(target).data('id', undefined);
			$($(target).find('p')[0]).html('&nbsp;');
			$($(target).find('p')[1]).html('&nbsp;');
			// remove hover elem
			$(target)
				.find('.card-options-wrapper')
				.each((index, elem) => {
					$(elem).remove();
				});
			$(target).removeClass('card-hover-options');
		}
	});
});

$(document).on('click', '.modal-error-main-close', async function (_e) {
	Modal.Close('.modal-error-main').then(() => {
		Modal.Delete('.modal-error-main');
	});
});

$(document).on('click', '.modal-error-main-teachers', _e => {
	if (schemaerr.Teacher === undefined) {
		return Notify.error("Geen error's i.v.m. de leerkrachten gevonden");
	}
	Modal.Close('.modal-error-main').then(() => {
		window.location.href = `/html/error.html?klas=${klas}&errtype=Teacher`;
		Modal.Delete('.modal-error-main');
	});
});

$(document).on('click', '.modal-error-main-classes', _e => {
	if (schemaerr.Class === undefined) {
		return Notify.error("Geen error's i.v.m. de klassen gevonden");
	}
	Modal.Close('.modal-error-main').then(() => {
		window.location.href = `/html/error.html?klas=${klas}&errtype=Class`;
		Modal.Delete('.modal-error-main');
	});
});

$(document).on('click', '.modal-error-main-lokalen', _e => {
	if (schemaerr.Lokaal === undefined) {
		return Notify.error("Geen error's i.v.m. de leerkrachten gevonden");
	}
	Modal.Close('.modal-error-main').then(() => {
		Modal.Delete('.modal-error-main');
		window.location.href = `/html/error.html?klas=${klas}&errtype=Lokaal`;
	});
});

$(document).on('mouseenter', '.diff-item', (e)=>{
	e.preventDefault()
	anime({
		targets: e.currentTarget.querySelector('i.fa-exchange-alt'),
		duration: 400,
		opacity: 0,
		loopComplete: () => {
			$(e.currentTarget).find('i.fa-exchange-alt').css({display:'none'})
			$(e.currentTarget).find('i.fa-times-circle').css({display:'block', opacity: 0})
			anime({
				targets: e.currentTarget.querySelector('i.fa-times-circle'),
				duration: 400,
				opacity: 1
			})
		}
	});
})

$(document).on('mouseleave', '.diff-item', (e)=>{
	e.preventDefault()
	anime({
		targets: e.currentTarget.querySelector('i.fa-times-circle'),
		duration: 400,
		opacity: 0,
		loopComplete: () => {
			$(e.currentTarget).find('i.fa-times-circle').css({display:'none'})
			$(e.currentTarget).find('i.fa-exchange-alt').css({display:'block', opacity: 0})
			anime({
				targets: e.currentTarget.querySelector('i.fa-exchange-alt'),
				duration: 400,
				opacity: 1
			})
		}
	});
})

$(document).on('click', '.diff-item i.fa-times-circle', (e)=>{
	e.preventDefault();
	const id = e.currentTarget.parentNode.dataset.diffid
	removeDiffItem(id, true)
})

$(document).on('click', '#diff-title span', (e)=>{
	$(e.currentTarget).find('i').hasClass('fa-minus')
	? (
		// Should minimize
		anime({
			targets: document.querySelector('.diff-wrapper'),
			duration: 300,
			easing: 'linear',
			bottom: -($('.diff-wrapper').height() - $('#diff-title').outerHeight()),
			loopComplete: () => {
				$(e.currentTarget).find('i').removeClass('fa-minus').addClass('fa-plus')
			}
		})
	)
	: (
		// Should maximize
			anime({
				targets: document.querySelector('.diff-wrapper'),
				duration: 300,
				bottom: 0,
				easing: 'linear',
				loopComplete: () => {
					$(e.currentTarget).find('i').removeClass('fa-plus').addClass('fa-minus')
				}
			})
		)
})

// eslint-disable-next-line no-undef
addLoadEvent(async () => {
	// eslint-disable-next-line no-undef
	while (!stylefileloaded) {
		await new Promise(resolve => {
			setTimeout(() => {
				resolve();
			}, 50);
		});
	}
	klas = getUrlParameter('klas');
	admin = getUrlParameter('admin');
	const url = 'http://' + window.location.hostname;
	Ajax.get('/api/user/checktoken')
		.then(async _res => {
			if (klas === undefined || klas === '') {
				window.location.replace(window.location.origin);
				sessionStorage.errorType = 'redirect';
				sessionStorage.error = 'Je hebt hier geen toegang toe';
			} else {
				//setup nav
				let elem = '';
				if (admin) {
					elem = `<a class="nav nav-dashboard text-hover text-hover-both-go-up nav-left">Dashboard</a>`;
				}
				$('.nav-list').html(
					elem +
					`<a class="nav nav-right nav-logout text-hover text-hover-both-go-up"><i class="fas fa-sign-out-alt"></i> Log-out</a>
					<a class="nav nav-right nav-user">WELCOME ${sessionStorage.username.toUpperCase()}</a>
					<div class="nav nav-dropdown"><a class="nav nav-config"><i class="fa fa-cog"></i>CONFIG</a></div>
					<div class="nav-darkmode-toggle nav-right">
						<span>☀️</span>
						<input type="checkbox" id="toggle-switch" />
						<label for="toggle-switch"><span class="screen-reader-text">Toggle Color Scheme</span></label>
						<span>🌙</span>
					</div>`
				);
				$('.nav-dropdown').css({
					top: 0,
					right: $('.nav-logout').css('width').split('px')[0] * 1 + 30,
					'min-width': $('.nav-user').css('width').split('px')[0] * 1 - 20,
					display: 'block',
					opacity: 0.0,
				});

				if (!admin) {
					$('.creator-option-card-con .fa-plus').css('display', 'none');
					$('.creator-options #save').css('display', 'none');
					$('.creator-hours').css('display', 'none');
					$('.creator-board #import').css('display', 'none');
					$('#switch-mode').css('display', 'none');
				}

				//check DB if class exist & of token part is of class if not admin
				await Ajax.get('/api/data/checkclass', { admin })
					.then(async _res => {
						//leerkrachten ophalen
						await Ajax.get('/api/data/leerkrachten', { klas })
							.then(async res => {
								teachers = res;
								await Ajax.get('/api/data/lokalen')
									.then(async res => {
										lokalen = res;
										// eslint-disable-next-line no-undef
										await SetupCardsOptions(teachers, lokalen);
									})
									.catch(res => {
										console.error("Couldn't get lok-data");
										window.location.replace(url);
										sessionStorage.errorType = 'redirect';
										sessionStorage.error = res.msg || 'We konden geen data ophalen!';
									});
							})
							.catch(res => {
								console.error("Couldn't get lkr-data");
								window.location.replace(url);
								sessionStorage.errorType = 'redirect';
								sessionStorage.error = res.msg || 'We konden geen data ophalen!';
							});

						//collect the card
						await Ajax.get('/api/creator/getcards', { klas })
							.then(async res => {
								await FormatCards(res);
								// eslint-disable-next-line no-undef
								cardheightchanger();
							})
							.catch(res => {
								console.error("Couldn't get cards-data");
								window.location.replace(url);
								sessionStorage.errorType = 'redirect';
								sessionStorage.error = res.responseJSON.message || 'We konden geen data ophalen!';
							});

						await Ajax.get('/api/creator/gethourcards', { klas })
							.then(async res => {
								// Collect the scema
								await Ajax.get('/api/creator/lesschema', { klas })
									.then(async lesschemares => {
										if (lesschemares.type === 'invalid') Notify.warning('Je vorig schema was verkeerd opgeslagen');
										if (admin) {
											await FormatHourCards(res);
											await FormatSchema(res, lesschemares.payload);
										} else {
											await FormatSchema(res, lesschemares.payload);
										}
									})
									.catch(res => {
										console.error("Couldn't get lesschema");
										window.location.replace(url);
										sessionStorage.errorType = 'redirect';
										sessionStorage.error = res.msg || 'We konden geen data ophalen!';
									});
							})
							.catch(res => {
								console.error("Couldn't get hourcards-data");
								window.location.replace(url);
								sessionStorage.errorType = 'redirect';
								sessionStorage.error = res.msg || 'We konden geen data ophalen!';
							});
						if (!admin) {
							// Drag cards registreren
							const dragcards = $('.creator-options-collection').find('.creator-option-card.used');
							$(dragcards).each((_i, elem) => {
								// Add move cursor to cards
								$(elem).css('cursor', 'move');
								// Make elem draggable
								$(elem).draggable({
									cursor: 'move',
									cursorAt: {
										top: $(elem).outerHeight(true) / 2,
										left: $(elem).outerWidth(true) / 2,
									},
									start: function (e, ui) {
										ui.helper.data('dropped', false);
										if ($(e.currentTarget).data('id')) {
											ui.helper.data('id', $(e.currentTarget).data('id'));
										}
										$('.creator-option-card-draggable').css({
											minWidth: $(elem).outerWidth(true),
											opacity: 0.8,
										});
										$(this).data('item', { yes: true });
										// reducer hour after starting the drag
										curhour = $(e.currentTarget).find('.creator-option-card-con > p')[3];
										$(curhour).html($(curhour).html() * 1 - 1);
										if ($(curhour).html() * 1 <= 0) {
											$(elem).draggable('disable');
											$(elem).draggable('option', { cursor: 'pointer' });
										}
									},
									stop: function (event, ui) {
										if (!ui.helper.data('dropped')) {
											// reset amount of hours
											$(curhour).html($(curhour).html() * 1 + 1);
										}
									},
									containment: 'document',
									helper: CardDragHelper,
								});
							});
						}
						// Drop cards registeren
						$('.creator-board-card-item').each((_i, elems) => {
							let dropcards = $(elems).find('.card');
							dropcards.each((_i, elem) => {
								$(elem).droppable({
									drop: CardDropHelper,
								});
							});
						});
						$('.spinner-con').animate({ opacity: 0 }, 750);
						$('.spinner-con').css({
							display: 'none',
							pointerEvents: 'none',
						});
					})
					.catch(res => {
						window.location.replace(url);
						sessionStorage.errorType = 'redirect';
						sessionStorage.error = res.msg || 'Je zit niet in deze klas!';
					});
			}
		})
		.catch(_res => {
			window.location.replace('../.');
			sessionStorage.errorType = 'redirect';
			sessionStorage.error = 'Je token is ongeldig';
		});
});

// FUNCTIONS
const getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
		}
	}
};

const Loader = function (toggle) {
	if (toggle === undefined) {
		if (loadershown === true) {
			$('.loading-bar-con').fadeOut(500);
			loadershown = false;
		} else {
			$('.loading-bar-con').fadeIn(500);
			loadershown = true;
		}
	} else {
		if (toggle) {
			$('.loading-bar-con').fadeIn(500);
			loadershown = true;
		} else if (!toggle) {
			$('.loading-bar-con').fadeOut(500);
			loadershown = false;
		}
	}
};

const FormatCards = cards => {
	// eslint-disable-next-line no-async-promise-executor
	return new Promise(async (res, _rej) => {
		//should give different cards when admin or not admin
		if (cards.length > 0) {
			let currentcard = 0;
			let cardelems = $('.creator-options-collection').find('.creator-option-card');
			if (admin) {
				//everything should be an input
				await Promise.all(
					cards.map(async card => {
						const curcard = currentcard;
						currentcard++;
						if (cardelems[curcard] === undefined) {
							$('.creator-options-collection').append(`<div class="creator-option-card"></div>`);
							cardelems = $('.creator-options-collection').find('.creator-option-card');
						}
						$(cardelems[curcard]).html(
							`<div class="creator-option-card-con"> <input type="text" name="name" value=${card.name} placeholder="Les"> </div> <div class="creator-option-card-con"> <select name="teacher"> <option value="">--Leerkracht--</option> </select> </div> <div class="creator-option-card-con"> <select name="lokaal"> <option value="">--Kies een lokaal--</option></select> </div> <div class="creator-option-card-con"> <input type="number" min="1" max="40" value=${card.hours} name="hours" placeholder="Aantal uren"> </div>`
						);
						$(cardelems[curcard]).addClass('used');
						$(cardelems[curcard]).data('id', card._id);
						// eslint-disable-next-line no-undef
						await SetupCardOptions($(cardelems[curcard]), teachers, lokalen);
						$(cardelems[curcard]).find('select[name="teacher"]').val(card.teacher._id);
						$(cardelems[curcard]).find('select[name="lokaal"]').val(card.lokaal._id);
					})
				);
				// Add new card
				if (cardelems[currentcard] === undefined) {
					$('.creator-options-collection').append(`<div class="creator-option-card"></div>`);
					cardelems = $('.creator-options-collection').find('.creator-option-card');
				}
				// Add empty card
				$(cardelems[currentcard]).html(`
					<div class="creator-option-card-con">
						<input type="text" name="name" placeholder="Les">
					</div>
					<div class="creator-option-card-con">
						<select type="text" name="teacher" placeholder="--Kies een leerkracht--" value="reset">
							<option disabled value="reset">--Kies een leerkracht--</option>
						</select>
					</div>
					<div class="creator-option-card-con">
						<select type="text" value="reset" name="lokaal" placeholder="--Kies een lokaal--">
							<option disabled value="reset">--Kies een lokaal--</option>
						</select>
					</div>
					<div class="creator-option-card-con">
						<input type="number" min="1" max="40" name="hours" placeholder="Aantal uren">
					</div>
				`);
				// eslint-disable-next-line no-undef
				await SetupCardOptions($(cardelems[currentcard]), teachers, lokalen);
				if ($('.creator-option-card').length % 6 > 0) {
					const diff = 6-$('.creator-option-card').length % 6
					for (let i = 0; i < diff; i++) {
						$('.creator-options-collection').append(`
						<div class="creator-option-card">
							<div class="creator-option-card-con">
								<i class="fa fa-plus"></i>
							</div>
						</div>
					`);
					}
				}
			} else {
				//everything should be p elem
				cards.map(card => {
					const curcard = currentcard;
					currentcard++;
					if (cardelems[curcard] == undefined) {
						$('.creator-options-collection').append(`<div class="creator-option-card"></div>`);
						cardelems = $('.creator-options-collection').find('.creator-option-card');
					}
					// {
					// 	name --> Naam van les
					// 	teachername --> Naam van lkr (firstname + lastname)
					//	lokaalname --> Naam van lok (blok+name)
					//	hours --> aantal uur
					// }
					$(cardelems[curcard]).html(
						`<div class="creator-option-card-con"> <p>${
							card.name
						}</p> </div> <div class="creator-option-card-con"> <p>${card.teacher.sex ? 'Mevr.' : 'M.'} ${
							card.teacher.lastname
						}</p> </div> <div class="creator-option-card-con"> <p>${
							(card.lokaal.blok ?? '') + (card.lokaal.name ?? '')
						} (${card.lokaal.type})</p> </div> <div class="creator-option-card-con"> <p>${card.hours}</p> </div>`
					);
					$(cardelems[curcard]).data('id', card._id);
					$(cardelems[curcard]).addClass('used');
				});
				if ($('.creator-option-card').length % 8 > 0) {
					// add cards
					for (let i = 0; i < $('.creator-option-card').length % 8; i++) {
						let elem = `
						<div class="creator-option-card">
							<div class="creator-option-card-con">
								<i class="fa fa-plus" style="display: none;"></i>
							</div>
						</div>`;
						$('.creator-options-collection').append(elem);
					}
				}
				$('.creator-option-card').css({ flex: '1 0 11%' });
			}
		}
		res(true);
	});
};

const FormatHourCards = function (hours) {
	return new Promise((res, _rej) => {
		if (hours) {
			var houramount = 0;
			for (const key in hours) {
				if (key.includes('uur') && hours[key] !== undefined && hours[key] !== null) {
					houramount++;
				}
			}
			$('.aantal-uren input').val(houramount);
			$('#auren').trigger('change');

			let cards = $('.hour-card');
			$.each(cards, (i, card) => {
				if (!hours['uur' + (i + 1)]) {
					$(card).css('display', 'none');
					return false;
				}

				if (!$(`.hour-card[data-hour="${i + 1}"]`)) {
					let elem = `<div class="hour-card" data-hour="${i + 1}"> <p class="hour-card-title">${
						names[i]
					} lesuur</p> <div> <label for="">Startuur</label> <input type="number" placeholder="08" min="1" max="24" value="${
						hours['uur' + (i + 1)].starthour || ''
					}" name="starthour"> : <input type="number" placeholder="30" min="1" max="60" value="${
						hours['uur' + (i + 1)].startmin || ''
					}" name="startmin"> </div><br><div>  <label for="">Einduur</label>  <input type="number" placeholder="09" min="1" max="24" value="${
						hours['uur' + (i + 1)].endhour || ''
					}" name="endhour">  :  <input type="number" placeholder="30" min="1" max="60" value="${
						hours['uur' + (i + 1)].endmin || ''
					}" name="endmin"></div> </div>`;
					$('.hour-cards').append(elem);
				} else {
					let excard = $(`.hour-card[data-hour="${i + 1}"]`);
					for (let key in hours['uur' + (i + 1)]) {
						const elem = hours['uur' + (i + 1)][key];
						$(excard).find(`input[name="${key}"]`).val(elem);
					}
				}
			});
			res(true);
		} else {
			res(true);
		}
	});
};

const FormatSchema = function (lesuren, schema) {
	return new Promise((res, _rej) => {
		let houramount = 0;
		let elem = '';
		// Add the hours to the side
		$('.creator-board-hour-table').html('');
		$.each(lesuren, (key, subinfo) => {
			if (key.includes('uur') && subinfo !== undefined && subinfo !== null) {
				houramount++;
				$.each(subinfo, (uur, hour) => {
					if (hour.length < 2) {
						subinfo[uur] = '0' + hour;
					}
				});
				elem = `<div class="creator-board-hour-item">${subinfo.starthour}:${subinfo.startmin} - ${subinfo.endhour}:${subinfo.endmin}</div>`;
				$('.creator-board-hour-table').append(elem);
			}
		});

		// generate the cards for 1 row

		elem = '';
		for (let i = 0; i < houramount; i++) {
			elem = `${elem}<div class="card" data-hour="${i}"> <p><b>&nbsp;</b></p> <p>&nbsp;</p> </div>`;
		}
		// 5x runnen
		$('.creator-board-card-list').html('');
		for (let i = 0; i < 5; i++) {
			$('.creator-board-card-list').append(
				`<div class='creator-board-card-item' data-day="${i}" data-name="${dagen[i]}">${elem}</div>`
			);
		}

		// Space the hours to the correct height
		$('.creator-board-hour-table').css({
			marginTop:
				$('.creator-board-card-headers').css('height').replace('px', '') * 1 +
				$('.creator-board-card-headers').css('margin-top').replace('px', '') * 1 +
				$('.creator-board-card-headers').css('margin-bottom').replace('px', '') * 1,
			minHeight: $('.creator-board-card-item').outerHeight(),
		});
		// $( '.creator-board-hour-item' ).each( ( i, item )=>{
		//   $( item ).css( 'margin-top',
		//     ( $( '.card' ).outerHeight() / 2 ) +
		//     ( $( '.card' ).outerHeight() * i ) +
		//     ( ( $( '.creator-board-hour-item' ).height() / 1.5 ) * i )
		//   )
		// } )

		// Place the cards in the place of the exisiting schema
		if (!schema) {
			res(true);
		}
		if (Object.entries(schema).length > 0) {
			$.each(schema, (day, daydata) => {
				$.each(daydata, (hour, hourcard) => {
					const card = findElemWithData('.creator-option-card', 'id', hourcard);
					let info = {
						les: $(card).find('p')?.[0]?.innerText,
						teacher: $(card).find('p')?.[1]?.innerText,
					};
					if (admin) {
						info = {
							les: $(card).find('input')?.[0]?.value,
							teacher: $(card)
								.find(`select[name="teacher"] option[value="${$(card).find(`select[name="teacher"]`).val()}"]`)
								.html(),
						};
					}
					const schemaplace = $(findElemWithData('.creator-board-card-item', 'name', day)).find('.card')[hour];
					const ptags = $(schemaplace).find('p');
					$(ptags[0]).html(info.les);
					$(ptags[1]).html(info.teacher);
					$(schemaplace).data('id', hourcard);
					$(schemaplace).addClass('card-hover-options');
					// reduce lesuur
					if (!admin || admin & (currentmode === 'drag-model')) {
						$($(card).find('p')[3]).html($($(card).find('p')[3]).html() - 1);
					}
				});
			});
		}
		res(true);
	});
};

const CardDragHelper = function (e, _ui) {
	// get info from original card
	let elem = '<div class="creator-option-card-draggable">';
	// e.currentTarget.childNodes[0].innerText
	e.currentTarget.childNodes.forEach(element => {
		if (element.innerText) {
			elem = elem + `<div><p>${element.innerText}</p></div>`;
		}
	});
	elem = elem + '</div>';
	return elem;
};

const CardDropHelper = function (e, ui) {
	ui.helper.data('dropped', true);

	// Check if schemadifferences is @ max && if maybe is undoing the difference
	let undoing = false;
	schemadifferences.forEach((diff, i) => {
		// check if days and hours are same
		if(diff.origin.day == $(e.target.parentNode).data('day') && diff.origin.hour == $(e.target).data('hour')) {
			if (diff.origin.id == ui.helper.data('id') && diff.target.id == ($(e.target).data('id') ?? 0)) {
				undoing = true;
				// Remove difference
				removeDiffItem(i, false)
				schemadifferences.splice(i, 1);
			}
		}
	});
	if (!undoing && schemadifferences.length >= 3) {
		schemadifferences.splice(3, schemadifferences.length - 3);
		return;
	}

	const switchinfo = {
		id: $(e.target).data('id'),
		name: $($(e.target).find('p')[0]).html(),
		teacher: $($(e.target).find('p')[1]).html(),
	};
	$(e.target).data('id', ui.helper.data('id'));
	$($(e.target).find('p')[0]).html($(ui.draggable).find('p')[0].innerText);
	$($(e.target).find('p')[1]).html($(ui.draggable).find('p')[1].innerText);
	$(e.target).addClass('card-hover-options');
	if (currentDragCard && ui.helper.data('schemacard')) {
		if (!switchinfo.id) {
			if (!undoing){
				schemadifferences.push({
					// origin: Card where drag started
					origin: {
						day: currentDragCard.parentNode.dataset.day,
						hour: currentDragCard.dataset.hour,
						id: ui.helper.data('id'),
					},
					// target: ward where draghelper was dropped
					target: {
						day: e.target.parentNode.dataset.day,
						hour: e.target.dataset.hour,
						id: 0,
					},
				});
				addDiffItem(schemadifferences.length-1)
			}
			$($(ui.draggable).find('p')[0]).html('&nbsp;');
			$($(ui.draggable).find('p')[1]).html('&nbsp;');
			$(ui.draggable)
				.find('.card-options-wrapper')
				.each((index, elem) => {
					$(elem).remove();
				});
			$(ui.draggable).removeClass('card-hover-options');
			$(ui.draggable).data('id', null);
			RemoveDraggable(ui.draggable);
			return;
		}
		// Check if card is same ifso return
		if (switchinfo.id === ui.helper.data('id')) return (currentDragCard = null);

		// switch
		const ps = $(currentDragCard).find('p');
		$(ps[0]).html(switchinfo.name);
		$(ps[1]).html(switchinfo.teacher);

		if (!undoing) {
			schemadifferences.push({
				// origin: Card where drag started
				origin: {
					day: currentDragCard.parentNode.dataset.day,
					hour: currentDragCard.dataset.hour,
					id: ui.helper.data('id'),
				},
				// target: ward where draghelper was dropped
				target: {
					day: e.target.parentNode.dataset.day,
					hour: e.target.dataset.hour,
					id: switchinfo.id,
				},
			});
			addDiffItem(schemadifferences.length-1)
		}

		$(currentDragCard).data('id', switchinfo.id);
		currentDragCard = null;
	}
};

const SchemaCardDragHelper = (e, _ui) => {
	currentDragCard = e.currentTarget;
	const infoelem = $(e.currentTarget).find('p');
	// eslint-disable-next-line no-undef
	hideOptionsWrapper(e.currentTarget);
	return `<div class="creator-schema-card-draggable">
		<div>
			<p>${$(infoelem[0]).html()}</p>
			<p>${$(infoelem[1]).html()}</p>
		</div>
	</div>`;
};

const GetHoursOfDay = function (day) {
	const wrapper = $(`.creator-board-card-item[data-name="${day}"]`);
	const hours = {};
	if (!wrapper) {
		console.error('No hours found');
		return;
	}
	const cards = $(wrapper).find('.card');
	cards.each(function (index, elem) {
		if ($(elem).data('id')) {
			hours[index] = $(elem).data('id');
		}
	});
	return hours;
};

const findElemWithData = (element, datatype, value) => {
	let retval = null;
	$(element).each((_, elem) => {
		if ($(elem).data(datatype) == String(value)) {
			retval = elem;
			return false;
		}
	});
	return retval;
};

const getInfoFromOptionCard = element => {
	const template = ["les", "lkr", 'klas', 'auren']
	const _info = []
	const info = {}
	$.each($(element).find('.creator-option-card-con p'), (i, text)=>{
		_info[i] = $(text)[0].textContent
	})
	_info.forEach((infotxt,i)=>{
		info[template[i]] = infotxt
	})
	return info
}

const switchCardMode = () => {
	switch (currentmode) {
		case 'edit-mode':
			// From drag-mode to edit-mode (from p to input)
			//collect the card
			Ajax.get('/api/creator/getcards', { klas })
				.then(async res => {
					await FormatCards(res);
					$('.card-hover-options.ui-draggable:not(.ui-draggable-disabled)').draggable('disable');
					$('.card-hover-options.ui-draggable:not(.ui-draggable-disabled)').draggable('option', { cursor: 'pointer' });
					// eslint-disable-next-line no-undef
					cardheightchanger();
				})
				.catch(() => {});
			break;

		case 'drag-mode':
			// From edit to drag (from input to p)
			$('.creator-option-card.used').each((_, card) => {
				if (!$(card).data('id')) return;
				$(card)
					.find('.creator-option-card-con')
					.each((_, con) => {
						if (!con || !con.firstElementChild) return;
						if ($(con.firstElementChild).prop('localName') === 'select') {
							$(con).html(
								`<p>${
									$(con.firstElementChild)
										.find(`option[value="${$(con.firstElementChild).val()}"]`)
										.html() ?? 'Onbekend'
								}</p>`
							);
						} else if ($(con.firstElementChild).attr('name') === 'hours') {
							let amount = $(con.firstElementChild).val() * 1;
							$(`.card`).each((_, subcard) => {
								if ($(subcard).data('id') === $(card).data('id')) amount--;
							});
							$(con).html(`<p>${amount ?? 'Onbekend'}</p>`);
						} else {
							$(con).html(`<p>${$(con.firstElementChild).val() ?? 'Onbekend'}</p>`);
						}
					});

				// Add move cursor to cards
				$(card).css('cursor', 'move');
				// Make elem draggable
				$(card).draggable({
					cursor: 'move',
					cursorAt: {
						top: $(card).outerHeight(true) / 2,
						left: $(card).outerWidth(true) / 2,
					},
					start: function (e, ui) {
						ui.helper.data('dropped', false);
						if ($(e.currentTarget).data('id')) {
							ui.helper.data('id', $(e.currentTarget).data('id'));
						}
						$('.creator-option-card-draggable').css({
							minWidth: $(card).outerWidth(true),
							opacity: 0.8,
						});
						// reducer hour after starting the drag
						curhour = $(e.currentTarget).find('.creator-option-card-con > p')[3];
						$(curhour).html($(curhour).html() * 1 - 1);
						if ($(curhour).html() * 1 <= 0) {
							$(card).draggable('disable');
							$(card).draggable('option', { cursor: 'pointer' });
							$(card).css('cursor', 'pointer');
						}
					},
					stop: function (event, ui) {
						if (!ui.helper.data('dropped')) {
							// reset amount of hours
							$(curhour).html($(curhour).html() * 1 + 1);
						}
					},
					containment: 'document',
					helper: CardDragHelper,
				});

				if ($($(card).find('.creator-option-card-con > p')[3]).html() * 1 < 1) {
					$(card).draggable('disable');
					$(card).draggable('option', { cursor: 'pointer' });
					$(card).css('cursor', 'pointer');
				}
			});

			$('.creator-option-card:not(.used)').each((_, con) => {
				$(con.childNodes).each((_, child) => {
					$(child).remove();
				});
			});

			if ($('.creator-option-card').length % 6 !== 0) {
				// Add empty cards
				for (let i = 0; i < $('.creator-option-card').length % 6; i++) {
					$('.creator-options-collection').append(
						'<div class="creator-option-card" style="min-height: 150.5px;"></div>'
					);
				}
			}
			break;

		default:
			break;
	}
	return;
};

// eslint-disable-next-line no-unused-vars
const makeSchemaDraggable = card => {
	const div = $(card).find('.card-option-move');
	if ($(card).hasClass('ui-draggable-disabled')) {
		$(card).draggable('enable');
		$(card).draggable('option', { cursor: 'move' });
		return;
	}
	$(card).draggable({
		cursor: 'move',
		cursorAt: {
			top: $(div).outerHeight(true) / 2,
			left: $(card).outerWidth(true) / 2 + $(div).outerWidth(true) / 2,
		},
		start: function (_e, ui) {
			ui.helper.data('dropped', false);
			ui.helper.data('schemacard', true);
			if ($(card).data('id')) {
				ui.helper.data('id', $(card).data('id'));
			}
			$('.creator-schema-card-draggable').css({
				minWidth: $(card).outerWidth(true),
				opacity: 0.8,
			});
		},
		stop: function (_e, ui) {
			if (!ui.helper.data('dropped')) {
				currentDragCard = null;
			}
		},
		containment: 'document',
		helper: SchemaCardDragHelper,
	});
};

// eslint-disable-next-line no-unused-vars
const RemoveDraggable = card => {
	if ($(card).hasClass('ui-draggable')) {
		$(card).draggable('disable');
		$(card).draggable('option', { cursor: 'pointer' });
	}
};

/**
 *@param index of schemadiff array
 * */
const addDiffItem = index => {
	const diff = schemadifferences[index]
	const info = {
		origin: getInfoFromOptionCard(findElemWithData('.creator-option-card', 'id', diff.origin.id)),
		target: diff.target.id ? getInfoFromOptionCard(findElemWithData('.creator-option-card', 'id', diff.target.id)) : {}
	}
	const element = `
		<div class="diff-item" data-diffid="${index}">
			<i class="fas fa-exchange-alt" style="opacity: 1; display: block;"></i>
			<i class="fas fa-times-circle" style="display: none; opacity: 1;"></i>
			<div class="diff-info">
				<p>${info.origin.les}, ${diff.origin.hour === '1' ? '1ste' : `${diff.origin.hour}de`} lesuur, ${dagen[diff.origin.day]}</p>
				<p>${diff.target.id ? `${info.target.les}, ${diff.target.hour === '1' ? '1ste' : `${diff.target.hour}de`} lesuur, ${dagen[diff.target.day]}` : `${diff.target.hour === '1' ? '1ste' : `${diff.target.hour}de`} lesuur, ${dagen[diff.target.day]}`}</p>
			</div>
		</div>
	`
	$('.diff-wrapper').append(element)
}

/**
 * @param index of schemadiff array
 */
const removeDiffItem = (index, shouldswitch) => {
	// Check if card on schema is free
	const diffinfo = schemadifferences[index]
	if (shouldswitch) {
		diffinfo.origin.info = getInfoFromOptionCard(findElemWithData('.creator-option-card', 'id', diffinfo.origin.id))
		diffinfo.target.info = diffinfo.target.id ? getInfoFromOptionCard(findElemWithData('.creator-option-card', 'id', diffinfo.target.id)) : {}
		const originelem = $(`.creator-board-card-item[data-day="${diffinfo.origin.day}"] .card[data-hour="${diffinfo.origin.hour}"]`)
		const targetelem = $(`.creator-board-card-item[data-day="${diffinfo.target.day}"] .card[data-hour="${diffinfo.target.hour}"]`)
		// switch elements
		$($(originelem).find('p')[0]).html(diffinfo.origin.info.les ?? '&nbsp')
		$($(originelem).find('p')[1]).html(diffinfo.origin.info.lkr ?? '&nbsp')
		$(originelem).data('id', diffinfo.origin.id)
		$(originelem).hasClass('card-hover-options') ? null : $(originelem).addClass('card-hover-options')
		$($(targetelem).find('p')[0]).html(diffinfo.target.info.les ?? '&nbsp')
		$($(targetelem).find('p')[1]).html(diffinfo.target.info.lkr ?? '&nbsp')
		diffinfo.target.id ? (
			$(targetelem).data('id', diffinfo.target.id),
			!$(targetelem).hasClass('card-hover-options') ? $(targetelem).addClass('card-hover-options') : null
		) : (
			$(targetelem).removeData('id'),
			!$(targetelem).hasClass('card-hover-options') ? null : $(targetelem).removeClass('card-hover-options')
		)
	}
	// Actually remove item
	const jqelem = $(`.diff-item[data-diffid="${index}"]`)
	if (jqelem.length <= 0) return jqelem.length
	anime({
		targets: document.querySelector(`.diff-item[data-diffid="${index}"]`),
		duration: 500,
		opacity: 0,
		left: jqelem.width()*5,
		complete: () => {
			jqelem.remove()
			schemadifferences.splice(index, 1);
		}
	})
}