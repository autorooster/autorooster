const Ajax = {}
Ajax.get = ( endpoint, data ) => {
	data = data ?? {}
	if( endpoint.charAt( 0 ) !== "/" ) {
		endpoint = '/'+endpoint
	}
	return new Promise( ( res, rej )=>{
		$.ajax( {
			url: window.location.origin + endpoint,
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Accept: '*/*'
			},
			cache: false,
			data: {
				token: sessionStorage.token,
				...data
			}
		} ).then( ( response )=>{
			res( response )
		} ).catch( ( response )=>{
			if ( response.status < 300 ) {
				res( response );
			} else {
				Notify[response.msgType ?? 'error']( `${ response.status }: ${ response.msg ?? response.statusText }` )
	
				// Handle unauthorized calls
				if ( response.status === 403 ) {
					sessionStorage.removeItem( 'username' );
					sessionStorage.removeItem( 'token' );
					sessionStorage.errorType = 'redirect';
					sessionStorage.error = 'Je bent hier niet geauthoriseerd voor';
					return window.location.replace( '../.' );
				}

				rej( response )
			}
		} )
	} )
}

Ajax.post = ( endpoint, data ) => {
	data = data ?? {}
	if( endpoint.charAt( 0 ) !== "/" ) {
		'/'+endpoint
	}
	return new Promise( ( res, rej )=>{
		$.ajax( {
			url: window.location.origin + endpoint,
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Accept: '*/*'
			},
			cache: false,
			data: JSON.stringify( {
				token: sessionStorage.token,
				...data
			} )
		} ).then( ( response )=>{
			res( response )
		} ).catch( ( response )=>{
			if ( response.status < 300 ) {
				res( response );
			} else {
				Notify[response.msgType ?? 'error']( `${ response.status }: ${ response.msg ?? response.statusText }` )

				// Handle unauthorized calls
				if ( response.status === 403 ) {
					sessionStorage.removeItem( 'username' );
					sessionStorage.removeItem( 'token' );
					sessionStorage.errorType = 'redirect';
					sessionStorage.error = 'Je bent hier niet geauthoriseerd voor';
					return window.location.replace( '../.' );
				}
				rej( response )
			}
		} )
	} )
}