/*
{
  'input': {
    'label': {
      'type': "before",
      'text': "name"
    },
    'name': "yeet",
    'value': "Jan Lecoutere",
    'type': "text",
    "data":{
			"inputinfo":"firstname"
    }
  },
  'button': {
    'text': 'Yeet btn',
    'class': 'yeet-btn'
  },
  'img': {
    'src': 'https://comps.canstockphoto.com/happy-architect-picture_csp3296591.jpg',
  },
  'p':{
    'text':"yeet"
  }
  'select':{
    'text':'Yeet list',
    'name':'lister',
    options:[
      "case1",
      "case3",
      "case4"
    ]
  }
}
*/

var inlistitem = false;

const Modal = {};
Modal.New = function ( modal, header, main, footer ) {
  //setting up the modal
  if ( $( '.modals' ).find( modal ).length > 0 ) {
    $.each( $( '.modals' ).find( modal ), function ( i, fmodal ) {
      if ( i !== 0 ) {
        $( fmodal ).remove();
      } else {
        $( fmodal ).html( '' );
      }
    } );
    $( modal ).html(
      '<div class="modal-header"></div><hr>' +
        '<div class="modal-main"></div><hr>' +
        '<div class="modal-footer"></div>'
    );
  } else {
		let retval = false
    if ( modal.includes( '.' ) ) {
      //setup modal with class
      let elem =
        '<form method="post" class="' +
        modal.slice( 1 ) +
        ' modal-con">' +
        '<div class="modal-header"></div><hr>' +
        '<div class="modal-main"></div><hr>' +
        '<div class="modal-footer"></div></form>';
      $( '.modals' ).append( elem );
			retval = true;
    }
		if ( modal.includes( '#' ) ) {
      //setup modal with ID
      let elem =
        '<form method="post" id="' +
        modal.slice( 1 ) +
        '" class="modal-con">' +
        '<div class="modal-header"></div><hr>' +
        '<div class="modal-main"></div><hr>' +
        '<div class="modal-footer"></div></form>';
      $( '.modals' ).append( elem );
			retval = true;
    }
		if( !retval ) {
      //throw back err
      return 'Misconfigured modal check first parameter';
    }
  }
  //creating the modal on input
	modal = modal.split( /\s/gm )[0]
  //header
  if ( typeof header === 'string' ) {
    $( modal ).find( '.modal-header' ).html( header );
  } else {
    setupModal( modal, 'modal-header', header );
  }

  //main
  if ( typeof main === 'string' ) {
    $( modal ).find( '.modal-main' ).html( header );
  } else {
    setupModal( modal, 'modal-main', main );
  }

  //footer
  if ( typeof footer === 'string' ) {
    $( modal ).find( '.modal-footer' ).html( footer );
  } else {
    setupModal( modal, 'modal-footer', footer );
  }

  //adding standard close btn
  let elem = "<i class='fa fa-times modal-close-standard'></i>";
  $( modal ).find( '.modal-header' ).append( elem );
};

Modal.Open = function ( modal ) {
  $( '.modals' )
    .find( '.modal-con' )
    .each( function ( i, fmodal ) {
      if ( $( fmodal ).css( 'margin-top' ).split( 'px' )[0]*1 > ( viewportToPixels( '10vh' )*( -1 ) ) ) {
        Modal.Close( fmodal, false );
      }
    } );
  $( modal ).css( 'display', 'none' );
  $( modal ).css( 'margin-top', -100 + 'vh' );
  $( '.modals' ).css( { display: 'block', 'pointer-events': 'all' } );
  $( modal ).css( 'display', 'block' );
	if ( $( window ).height()*0.8 < $( modal ).find( '.modal-main' ).height() ) {
		$( modal ).find( '.modal-main' ).css( {
			maxHeight: $( modal ).find( '.modal-main' ).height() *.6,
			overflow: 'auto',
			justifyContent: "initial"
		} )
	}
  $( modal ).animate( { 'margin-top': '5vh' } );
};

Modal.Close = function ( modal, bool ) {
	return new Promise( ( res, _rej )=>{
		if ( !bool ) {
			$( '.modals' ).css( 'pointer-events', 'none' );
		}
		$( modal ).animate( { 'margin-top': '-100vh' }, {
			duration: 300,
			queue: false,
			complete: () => {
				res()
			}
		} );
	} )
};

Modal.Delete = function ( modal ) {
  if ( $( '.modals' ).find( modal ).length > 0 ) {
    $.each( $( '.modals' ).find( modal ), function ( i, fmodal ) {
      if ( i !== 0 ) {
        $( fmodal ).remove();
      } else {
        $( fmodal ).html( '' );
      }
    } );
    $( modal ).html(
      '<div class="modal-header"></div><hr>' +
        '<div class="modal-main"></div><hr>' +
        '<div class="modal-footer"></div>'
    );
  }
};

$( document ).keydown( function ( e ) {
  if ( e.keyCode == 27 ) {
    //cycle trough all modal's and close them all
    let modals = $( '.modals' ).find( '.modal-con' );
    let timeout = 300;
    $.each( modals, function ( i, modal ) {
      modal.classList.forEach( ( e ) => {
        if ( e !== 'modal-con' ) {
          Modal.Close( '.' + e );
          timeout = timeout + 300;
          return;
        }
      } );
    } );
    setTimeout( () => {
      $( '.modals' ).css( 'display', 'none' );
    }, timeout );
  }
} );

function viewportToPixels( value ) {
  var parts = value.match(/([0-9\.]+)(vh|vw)/)//eslint-disable-line
  var q = Number( parts[1] );
  var side =
    window[[ 'innerHeight', 'innerWidth' ][[ 'vh', 'vw' ].indexOf( parts[2] )]];
  return side * ( q / 100 );
}

function setupModal( modal, conname, info ) {
  $.each( info, function ( type, args ) {
    let elem = '';
    if ( type.includes( 'input' ) || type === 'input' ) {
      elem = '<div class="modal-main-con">';
      if ( args.label.type === 'before' ) {
        elem = elem + '<label>' + args.label.text + '</label>';
      }
      elem = elem + '<input ';
      if ( args.name !== undefined ) {
        elem = elem + 'name ="' + args.name + '"';
      }
      if ( args.value !== undefined ) {
        elem = elem + 'value ="' + args.value + '"';
      }
      if ( args.data !== undefined ) {
        $.each( args.data, function ( key, val ) {
          elem = elem + `data-${ key }="${ val }"`;
        } );
      }
      if ( args.type !== undefined ) {
        elem = elem + 'type ="' + args.type + '"';
      } else {
        elem = elem + 'type = "text"';
      }
      if ( args.style !== undefined ) {
        elem = elem + 'style="' + args.style + '"';
      }
      if ( args.class !== undefined ) {
        elem = elem + ' class="' + args.class + '"';
      }
      elem = elem + '></div>';
      $( modal )
        .find( '.' + conname )
        .append( elem );
    } else if ( type.includes( 'button' ) || type === 'button' ) {
      elem = '<div class="modal-main-con"><input type="button"';
      if ( args.text !== undefined ) {
        elem = elem + 'value="' + args.text + '" ';
      } else {
        elem = elem + 'value="Misconfigured button" ';
      }
      if ( args.name !== undefined ) {
        elem = elem + 'name="' + args.name + '" ';
      }
      if ( args.style !== undefined ) {
        elem = elem + 'style="' + args.style + '"';
      }
      if ( args.class !== undefined ) {
        elem = elem + 'class="' + args.class + '">';
      } else {
        elem = elem + '></div>';
      }
      $( modal )
        .find( '.' + conname )
        .append( elem );
    } else if ( type.includes( 'img' ) || type === 'img' ) {
      elem = '<div class="modal-main-con"><img ';
      if ( args.src !== undefined ) {
        elem = elem + ` src="${ args.src }" `;
      } else {
        return 'No src defined';
      }
      if ( args.class !== undefined ) {
        elem = elem + ` class="`;
        args.class.forEach( ( e ) => {
          elem = elem + e + ' ';
        } );
        elem = elem + '"';
      }
      if ( args.data !== undefined ) {
        $.each( args.data, function ( i, val ) {
          elem = elem + ` data-${ i }="${ val }" `;
        } );
      }
      if ( args.style !== undefined ) {
        let width = $( modal ).css( 'width' );
        elem = `${ elem } style="width:${ width };${ args.style }" `;
      } else {
        let width = $( modal ).css( 'width' );
        elem = `${ elem } style="width:${ width };"`;
      }
      elem = elem + '></div>';
      $( modal )
        .find( '.' + conname )
        .append( elem );
    } else if ( type.includes( 'select' ) || type === 'select' ) {
      if ( args.name === undefined || args.text === undefined ) {
        return 'Select needs a Name & Text for label';
      }
      elem = `<div class="modal-main-con"><label>${ args.text }</label><select `;
      if ( args.name !== undefined ) {
        elem = `${ elem }name="${ args.name }" `;
      } else {
        return 'Select needs a name';
      }
      if ( args.class !== undefined ) {
        elem = `${ elem } class=" `;
        args.class.forEach( ( e ) => {
          elem = elem + e + ' ';
        } );
        elem = elem + '"';
      }
      if ( args.data !== undefined ) {
        $.each( args.data, function ( i, val ) {
          elem = elem + ` data-${ i }="${ val }" `;
        } );
      }
      elem = `${ elem } >`;
			if ( Array.isArray( args.options ) ) { //array options
				args.options.map( ( arg, index )=>{
					if ( args.default !== undefined && index === args.default ) {
						elem = `${ elem }<option value="${ arg.toLowerCase() }" selected="selected">${ arg }</option>`;
					} else {
						elem = `${ elem }<option value="${ arg.toLowerCase() }">${ arg }</option>`;
					}
				} )
			} else { // Object options-
				$.each( args.options, ( i, arg )=>{
					if ( args.default !== undefined && IsKeyDefault( args.default, Object.entries( args.options ) ) ) {
						elem = `${ elem }<option value="${ i }" selected="selected">${ arg }</option>`;
					} else {
						elem = `${ elem }<option value="${ i }">${ arg }</option>`;
					}
				} );
			}
      elem = `${ elem }</select></div>`;
      $( modal )
        .find( '.' + conname )
        .append( elem );
    } else if ( type.includes( 'dropdown' ) || type == 'dropdown' ) {
      // root of contianer = document.querySelector(`[data-${datan}]`)

      if ( args.data === undefined && args.data[0] === undefined ) {
        throw new Error( 'Jouw keuzemenu heeft geen data(required)' );
      }
      elem = `<div class="modal-main-con" `;
      let datai;
      let datav;

      $.each( args.data, function ( i, val ) {
        elem = `${ elem } data-${ i }="${ val }"`;
        datai = i;
        datav = val;
      } );

      elem = `${ elem } ><div class="dropdown-button noselect"><div class="dropdown-label"></div><i class="fa fa-filter"></i></div><div class="dropdown-list" style="display:none;"><input type="search" placeholder="Zoeken" class="dropdown-search"/><ul></ul></div></div>`;
      $( modal )
        .find( '.' + conname )
        .append( elem );

      if ( args.options[0].name !== undefined ) {
        args.options.forEach( ( s ) => {
          s.capName = s.name.slice( 0, 1 ).toUpperCase() + s.name.slice( 1 );
          $( `[data-${ datai }="${ datav }"] ul` ).append(
            '<li>' +
              `<input name="${ s.name }" type="checkbox">` +
              `<label for="${ s.name }">${ s.capName }</label>` +
              '</li>'
          );
        } );
      } else {
        args.options.forEach( ( s ) => {
          let d = {
            name: s
          };
          d.capName = s.slice( 0, 1 ).toUpperCase() + s.slice( 1 );
          $( `[data-${ datai }="${ datav }"] ul` ).append(
            '<li>' +
              `<input name="${ d.name }" type="checkbox">` +
              `<label for="${ d.name }">${ d.capName }</label>` +
              '</li>'
          );
        } );
      }
    } else if ( type.includes( 'p' ) || type === 'p' ) {
      elem = '<div class="modal-main-con"><p';
      if ( args.text === undefined ) {
        return 'The paragraph is missing text';
      }
      if ( args.class !== undefined ) {
        elem = elem + ` class="`;
        args.class.forEach( ( e ) => {
          elem = elem + e + ' ';
        } );
        elem = elem + '"';
      }
      if ( args.data !== undefined ) {
        $.each( args.data, function ( i, val ) {
          elem = elem + ` data-${ i }="${ val }" `;
        } );
      }
      if ( args.style !== undefined ) {
        elem = `${ elem } style="${ args.style }" `;
      }
      elem = `${ elem }>${ args.text }</p></div>`;
      $( modal )
        .find( '.' + conname )
        .append( elem );
    }
  } );
}

$( document ).on( 'click', '.dropdown-button', function ( e ) {
  e.preventDefault();
  setTimeout( () => {
    if ( !inlistitem ) {
      if ( $( this ).siblings( '.dropdown-list' ).css( 'display' ) === 'flex' ) {
        $( this ).siblings( '.dropdown-list' ).css( 'display', 'block' );
      } else if (
        $( this ).siblings( '.dropdown-list' ).css( 'display' ) === 'block'
      ) {
        $( this ).siblings( '.dropdown-list' ).css( 'display', 'none' );
      } else if ( $( this ).siblings( '.dropdown-list' ).css( 'display' ) === 'none' ) {
        $( this ).siblings( '.dropdown-list' ).css( 'display', 'block' );
      }
    }
    inlistitem = false;
  }, 10 );
} );

$( document ).on( 'click', '.dropdown-label-item', function ( e ) {
  e.preventDefault();
  inlistitem = true;
} );

$( document ).on( 'click', '.dropdown-label-item i', function ( e ) {
  e.preventDefault();

  let val = $( this.parentNode.parentNode ).html().split( '<span>' )[0];
  let d = {
    name: val
  };
  d.capName = val.slice( 0, 1 ).toUpperCase() + val.slice( 1 );
	let data = Object.entries( $( this.parentNode.parentNode.parentNode.parentNode.parentNode ).data() )[0]
	$( `.modal-main-con[data-${ data[0] }="${ data[1] }"] ul` ).append(
		`<li>
      <input name="${ d.name }" type="checkbox">
      <label for="${ d.name }">${ d.capName }</label>
		</li>`
	)
  $( this.parentNode.parentNode ).remove();
} );

$( document ).on( 'change', '.dropdown-list [type="checkbox"]', function ( e ) {
  e.preventDefault();
  //remove item from list add extra div to label
  if ( this.checked ) {
    let elem = `<div class="dropdown-label-item">${ $( this ).attr(
      'name'
    ) }<span><i class="fas fa-times"></i></span></div>`;
    $(
      $( this.parentNode.parentNode.parentNode.parentNode ).find(
        '.dropdown-label'
      )
    ).append( elem );
    $( this ).parent().remove();
  }
} );

$( document ).on( 'click', '.dropdown-list li label', function ( e ) {
  e.preventDefault();
  if ( this.checked ) {
    let elem = `<div class="dropdown-label-item">${ $(
      this
    ).html() }<span><i class="fas fa-times"></i></span></div>`;
    $(
      $( this.parentNode.parentNode.parentNode.parentNode ).find(
        '.dropdown-label'
      )
    ).append( elem );
    $( this ).parent().remove();
  }
} );

$( document ).on( 'input', '.dropdown-search', function () {
  var target = $( this );
  var dropdownList = target.closest( '.dropdown-list' );
  var search = target.val().toLowerCase();

  if ( !search ) {
    dropdownList.find( 'li' ).show();
    return false;
  }

  dropdownList.find( 'li' ).each( function () {
    var text = $( this ).text().toLowerCase();
    var match = text.indexOf( search ) > -1;
    $( this ).toggle( match );
  } );
} );

$( document ).on( 'click', '.modal-close-standard', function ( e ) {
  e.preventDefault();
  this.parentNode.parentNode.classList.forEach( ( cls ) => {
    if ( cls !== 'modal-con' ) {
      Modal.Close( '.'+cls );
      setTimeout( () => {
        Modal.Delete( '.'+cls );
      }, 1000 );
      return false;
    }
  } );
} );

const IsKeyDefault = ( defentry, entries ) => {
	let retval = false
	entries.map( ( entry )=>{
		if ( entry === defentry ) retval = true;
	} )
	return retval
}