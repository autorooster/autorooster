let id = 0;
let cachedNotifications = JSON.parse(sessionStorage.getItem('cachedNotifications')) ?? [];
const Notify = {};

addLoadEvent(() => {
	cachedNotifications.forEach(noti => {
		Notify[noti.type](noti.text, noti.length, noti.waitonDismiss, true);
	});
	cachedNotifications = [];
	sessionStorage.setItem('cachedNotifications', JSON.stringify([]));
});

Notify.success = function (text, length, waitonDismiss = false, skipSave = false) {
	id++;
	if ($('body').find('.notify-wrapper').length < 1) {
		$('body').prepend('<div class="notify-wrapper"></div>');
	}
	$('.notify-wrapper').prepend(
		'<div class="notify-container notify-success" data-noti="' +
			id +
			'"><div class="notify-icon"><i class="fas fa-check-circle"></i></div><div class="notify-label">' +
			text +
			'</div></div>'
	);
	if(!skipSave){
		cachedNotifications.push({
			type: 'success',
			text,
			length,
			waitonDismiss,
			id,
		});
		sessionStorage.setItem('cachedNotifications', JSON.stringify(cachedNotifications));
	}
	a(id, length || 3000, waitonDismiss);
};
Notify.error = function (text, length, waitonDismiss = false, skipSave = false) {
	id++;
	if ($('body').find('.notify-wrapper').length < 1) {
		$('body').prepend('<div class="notify-wrapper"></div>');
	}
	$('.notify-wrapper').prepend(
		'<div class="notify-container notify-error" data-noti="' +
			id +
			'"><div class="notify-icon"><i class="fas fa-times-circle"></i></div><div class="notify-label">' +
			text +
			'</div></div>'
	);
	if(!skipSave){
		cachedNotifications.push({
			type: 'error',
			text,
			length,
			waitonDismiss,
			id,
		});
		sessionStorage.setItem('cachedNotifications', JSON.stringify(cachedNotifications));
	}
	a(id, length || 3000, waitonDismiss);
};
Notify.warning = function (text, length, waitonDismiss = false, skipSave = false) {
	id++;
	if ($('body').find('.notify-wrapper').length < 1) {
		$('body').prepend('<div class="notify-wrapper"></div>');
	}
	$('.notify-wrapper').prepend(
		'<div class="notify-container notify-warning" data-noti="' +
			id +
			'"><div class="notify-icon"><i class="fas fa-exclamation-triangle"></i></div><div class="notify-label">' +
			text +
			'</div></div>'
	);
	if(!skipSave){
		cachedNotifications.push({
			type: 'warning',
			text,
			length,
			waitonDismiss,
			id,
		});
		sessionStorage.setItem('cachedNotifications', JSON.stringify(cachedNotifications));
	}
	a(id, length || 3000, waitonDismiss);
};
Notify.info = function (text, length, waitonDismiss = false, skipSave = false) {
	id++;
	if ($('body').find('.notify-wrapper').length < 1) {
		$('body').prepend('<div class="notify-wrapper"></div>');
	}
	$('.notify-wrapper').prepend(
		'<div class="notify-container notify-info" data-noti="' +
			id +
			'"><div class="notify-icon"><i class="fas fa-info-circle"></i></div><div class="notify-label">' +
			text +
			'</div></div>'
	);
	if(!skipSave){
		cachedNotifications.push({
			type: 'info',
			text,
			length,
			waitonDismiss,
			id,
		});
		sessionStorage.setItem('cachedNotifications', JSON.stringify(cachedNotifications));
	}
	a(id, length || 3000, waitonDismiss);
};

function animateCSS(element, animationName, callback) {
	const node = document.querySelector(element);
	node.classList.add('animated', animationName);

	function handleAnimationEnd() {
		node.classList.remove('animated', animationName);
		node.removeEventListener('animationend', handleAnimationEnd);

		if (typeof callback === 'function') callback();
	}

	node.addEventListener('animationend', handleAnimationEnd);
}

function a(id, length, waitonDismiss) {
	animateCSS('[data-noti="' + id + '"]', 'zoomInRight', function () {
		$('[data-noti="' + id + '"]').removeClass('animated zoomInRight');
	});
	if (waitonDismiss) return;
	setTimeout(() => {
		animateCSS('[data-noti="' + id + '"]', 'zoomOutRight', function () {
			$('[data-noti="' + id + '"]').remove();
		});
		const index = cachedNotifications.findIndex(noti => noti.id === id);
		if (index === undefined) return;
		cachedNotifications.splice(index, 1);
		sessionStorage.setItem('cachedNotifications', JSON.stringify(cachedNotifications));
	}, length);
}

$(document).on('click', '.notify-container', e => {
	animateCSS(`[data-noti="${e.currentTarget.dataset.noti}"]`, 'zoomOutRight', function () {
		$(`[data-noti="${e.currentTarget.dataset.noti}"]`).remove();
		const index = cachedNotifications.findIndex(noti => noti.id === e.currentTarget.dataset.noti);
		if (index === undefined) return;
		cachedNotifications.splice(index, 1);
		sessionStorage.setItem('cachedNotifications', JSON.stringify(cachedNotifications));
	});
});
