const Delay = (ms = 1000) => new Promise(res => setTimeout(res, ms));

// eslint-disable-next-line no-undef
addLoadEvent(async () => {
	while (true) {
		await Ajax.get('/api/notification/retrieve')
			.then(res => {
				res.forEach(noti => {
					Notify[noti.type](noti.msg, undefined, true);
				});
			})
			.catch(res => {});
		await Delay(10 * 1000);
	}
});
