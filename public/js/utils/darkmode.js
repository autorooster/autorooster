// eslint-disable-next-line no-undef
addLoadEvent( () => {
	const savedTheme = localStorage.getItem( 'theme' ) ?? "light";
	changeTheme( savedTheme )
} );

$( document ).on( 'change', '.nav-darkmode-toggle #toggle-switch', ()=>{
	const savedTheme = $( '.nav-darkmode-toggle #toggle-switch' ).prop( 'checked' ) ? "dark" : "light" ;
	
	changeTheme( savedTheme )
} )

const changeTheme = async( theme )=> {
	switch ( theme ) {
		case "light":
			if( $( 'body' ).hasClass( 'dark-mode' ) ) $( 'body' ).removeClass( 'dark-mode' );
			$( '.nav-darkmode-toggle #toggle-switch' ).prop( 'checked', false )
			localStorage.setItem( 'theme', "light" )
			break;
		case 'dark':
			if( !( $( 'body' ).hasClass( 'dark-mode' ) ) ) $( 'body' ).addClass( 'dark-mode' );
			while ( $( '.nav-darkmode-toggle #toggle-switch' ).length < 1 ) {
				await new Promise( ( res, _rej )=>{
					setTimeout( res,100 )
				} )
			}	
			$( '.nav-darkmode-toggle #toggle-switch' ).prop( 'checked', true )
			localStorage.setItem( 'theme', "dark" );
			break;
		default:
			break;
	}
}