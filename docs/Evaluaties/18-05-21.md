# 18MEI2021

# Realisaties
- De navigatie bar is eindelijk +- gecentreerd
- Mogelijkheid om een 9de lesuur toe te voegen toegevoegd
- Pause cards verwijderd omdat ze niet echt veel nut hadden
- Begonnen aan de parser voor het combineren van de schema's
- Backend naar Typescript veranderd
- Sentry logging toegevoegd (Error tracking)
- Font aangepast naar een font met meer opties (roboto)
- Docker image + CI

# Verdediging
## Powerpoint
- Doel van eindwerkt
- Wat is er gelukt wat is er niet gelukt
- Aanpak?
- Gebruikt services en kleine uitleg hierover
- Uitleg over elke pagina
- Feitjes over project? (lijntjes code, aantal commits,...)
## Demo van website
- Elke pagina laten zien en wel iets laten zien wat er op mogelijk is