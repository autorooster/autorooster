# GIP

## [Gitkraken board](https://app.gitkraken.com/glo/board/X6qgPG3fUgARNT7Y)
## [Blog](https://nuttyshrimp.gitlab.io/autorooster/)

## [diagram](https://viewer.diagrams.net/?target=blank&layers=1&nav=1&title=gip.drawio#Uhttps%3A%2F%2Fraw.githubusercontent.com%2FNuttyShrimp%2Fgip%2Fmaster%2Fportfolio%2Fgip.drawio)

```
- Groen: Info gelinked aan de gebruiker
- Blauw: Info m.b.t. de lessenroosters

```

## Notificatie systeem

[![Image from Gyazo](https://i.gyazo.com/44c077e3e9f7e3a88eab9b694c14ad86.gif)](https://gyazo.com/44c077e3e9f7e3a88eab9b694c14ad86)

## Stappenplan van een lessenrooster

1. praktijkvakken + LO
2. vakken waarbij je met verschillende klassen gecombineerd zit
3. vakken waarvoor je een speciaal lokaal nodig hebt dat niet speciaal bedoeld is voor altijd een praktisch vak in te geven(bv. Elektriciteit)
4. vakken waar je alleen zit in het lessenrooster toegevoegd

## API calls
### 403
In ajax functie inbouwen dat een 403 standaard alle data uit sessionstorage verwijderd en doorverwijst naar hoofdpagina
### Error message
Auto message in bouwen in Ajax met lijst van vertalingen?
standaard template: ${errorcode} - ${msg|| standardmsgs[errorcode]}
Deze zou in een error notify moeten komen zolang er geen msgType gedefiniëerd is

## Gebruikte links/docs

- <https://www.freecodecamp.org/news/introduction-to-mongoose-for-mongodb-d2a7aa593c57/>
- <https://www.w3schools.com>
- <https://docs.mongodb.com/>
- <https://www.youtube.com/watch?v=2jqok-WgelI>
- <https://codepen.io/danzawadzki/details/EgqKRr>
- <https://www.w3docs.com/snippets/javascript/how-to-encode-and-decode-strings-with-base64-in-javascript.html>
- <https://codepen.io/tonkec/pen/jWmgqN>
- <https://codepen.io/satjeet_sandhu/pen/oGJQKd>
- <https://codepen.io/hologramdesign/pen/KKKPjJO>
- <https://codepen.io/stefen/pen/VNVaYW>
- File structure: <https://stackoverflow.com/questions/51126472/how-to-organise-file-structure-of-backend-and-frontend-in-mern>
- Eliott
